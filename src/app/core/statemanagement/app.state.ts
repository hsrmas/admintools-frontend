import { Proposal } from '../domain/proposal';
import { Technology } from '../domain/technology';
import { ProposalType } from '../domain/proposal-type';
import { Institute } from '../domain/institute';
import { University } from '../domain/university';
import { Application } from '../domain/application';
import { User } from '../domain/user';

export interface AppState {
  proposals: Proposal[];
  technologies: Technology[];
  proposalTypes: ProposalType[];
  institutes: Institute[];
  universities: University[];
  applications: Application[];
  users: User[];
}
