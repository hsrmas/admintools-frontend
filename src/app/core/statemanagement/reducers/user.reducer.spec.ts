import { User } from '../../domain/user';
import { userReducer } from './user.reducer';
import { UserActions } from '../actions';
import { UserMockData } from '../../../unittest/mock-data/user-mock-data';

const initialState: User[] = UserMockData.getMockUsers();

describe('user reducer', () => {
  it('returns an empty array by default', () => {
    const defaultState = userReducer(undefined, {type: 'DOES_NOT_EXIST'} as UserActions.All);
    expect(defaultState).toEqual([]);
  });

  it(`${UserActions.LOAD_USERS}`, () => {
    const payload = initialState;
    const newState = userReducer(initialState, new UserActions.LoadUsers(payload));
    expect(JSON.stringify(newState)).toEqual(JSON.stringify(payload));
  });

  it(`${UserActions.CREATE_USER}`, () => {
    const payload = UserMockData.newUser();
    const result = [...initialState, payload];
    const newState = userReducer(initialState, new UserActions.CreateUser(payload));
    expect(JSON.stringify(newState)).toEqual(JSON.stringify(result));
  });

  it(`${UserActions.UPDATE_USER}`, () => {
    const payload = UserMockData.updatedUser();
    const result = [initialState[0], payload];
    const newState = userReducer(initialState, new UserActions.UpdateUser(payload));
    expect(JSON.stringify(newState)).toEqual(JSON.stringify(result));
  });

  it(`${UserActions.CREATE_OR_UPDATE_USER}`, () => {
    const payload = UserMockData.updatedUser();
    const result = [initialState[0], payload];
    const newState = userReducer(initialState, new UserActions.CreateOrUpdateUser(payload));
    expect(JSON.stringify(newState)).toEqual(JSON.stringify(result));
  });

  it(`${UserActions.CREATE_OR_UPDATE_USER}`, () => {
    const payload = UserMockData.newUser();
    const result = [...initialState, payload];
    const newState = userReducer(initialState, new UserActions.CreateOrUpdateUser(payload));
    expect(JSON.stringify(newState)).toEqual(JSON.stringify(result));
  });
});
