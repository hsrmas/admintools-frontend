import { ApplicationActions } from '../actions';
import { Application } from '../../domain/application';

/**
 * ApplicationReducer
 *
 * @param {Application[]} state
 * @param {All} action
 * @returns {Application[]}
 */
export function applicationReducer(state: Application[] = [], action: ApplicationActions.All): Application[] {

  switch (action.type) {
    case ApplicationActions.LOAD_APPLICATIONS:
      return (<ApplicationActions.LoadApplications>action).applications;

    case ApplicationActions.CREATE_APPLICATION:
      return [...state, (<ApplicationActions.CreateApplication>action).application];

    case ApplicationActions.UPDATE_APPLICATION:
      const updatedApplication = (<ApplicationActions.UpdateApplication>action).application;

      return state.map((application: Application) => {
        return application.id === updatedApplication.id
          ? Object.assign(Object.create(Application.prototype), application, updatedApplication)
          : application;
      });

    case ApplicationActions.CREATE_OR_UPDATE_APPLICATION:
      const existingApplication = state.find((application: Application) =>
        application.id === (<ApplicationActions.CreateOrUpdateApplication>action).application.id);

      if (existingApplication) {
        return applicationReducer(state,
          new ApplicationActions.UpdateApplication((<ApplicationActions.CreateOrUpdateApplication>action).application));
      } else {
        return applicationReducer(state,
          new ApplicationActions.CreateApplication((<ApplicationActions.CreateOrUpdateApplication>action).application));
      }
    default:
      return state;

  }
}
