import { Proposal } from '../../domain/proposal';
import { ProposalActions } from '../actions';
import { proposalReducer } from './proposal.reducer';
import { ProposalMockData } from '../../../unittest/mock-data/proposal-mock-data';


const initialState: Proposal[] = ProposalMockData.getMockProposals();


describe('proposal reducer', () => {

  it('returns an empty array by default', () => {
    const defaultState = proposalReducer(undefined, {type: 'DOES_NOT_EXIST'} as ProposalActions.All);
    expect(defaultState).toEqual([]);
  });

  it(`${ProposalActions.LOAD_PROPOSALS}`, () => {
    const payload = initialState;
    const newState = proposalReducer(initialState, new ProposalActions.LoadProposals(payload));
    expect(JSON.stringify(newState)).toEqual(JSON.stringify(payload));
  });

  it(`${ProposalActions.CREATE_PROPOSAL}`, () => {
    const payload = ProposalMockData.newProposal();
    const result = [...initialState, payload];
    const newState = proposalReducer(initialState, new ProposalActions.CreateProposal(payload));
    expect(JSON.stringify(newState)).toEqual(JSON.stringify(result));
  });

  it(`${ProposalActions.UPDATE_PROPOSAL}`, () => {
    const payload = ProposalMockData.updatedProposal();
    const result = [initialState[0], payload];
    const newState = proposalReducer(initialState, new ProposalActions.UpdateProposal(payload));
    expect(JSON.stringify(newState)).toEqual(JSON.stringify(result));
  });

  it(`${ProposalActions.CREATE_OR_UPDATE_PROPOSAL}`, () => {
    const payload = ProposalMockData.updatedProposal();
    const result = [initialState[0], payload];
    const newState = proposalReducer(initialState, new ProposalActions.CreateOrUpdateProposal(payload));
    expect(JSON.stringify(newState)).toEqual(JSON.stringify(result));
  });

  it(`${ProposalActions.CREATE_OR_UPDATE_PROPOSAL}`, () => {
    const payload = ProposalMockData.newProposal();
    const result = [...initialState, payload];
    const newState = proposalReducer(initialState, new ProposalActions.CreateOrUpdateProposal(payload));
    expect(JSON.stringify(newState)).toEqual(JSON.stringify(result));
  });

  it(`${ProposalActions.DELETE_PROPOSAL}`, () => {
    const result = [initialState[0]];
    const newState = proposalReducer(initialState, new ProposalActions.DeleteProposal(ProposalMockData.updatedProposal()));
    expect(JSON.stringify(newState)).toEqual(JSON.stringify(result));
  });
});
