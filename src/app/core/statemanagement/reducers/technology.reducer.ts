import { TechnologyActions } from '../actions';
import { Technology } from '../../domain/technology';

/**
 * Technology Reducer
 * @param {Technology[]} state
 * @param {All} action
 * @returns {Technology[]}
 */
export function technologyReducer(state: Technology[] = [], action: TechnologyActions.All): Technology[] {

  switch (action.type) {
    case TechnologyActions.LOAD_TECHNOLOGIES:
      return (<TechnologyActions.LoadTechnologies>action).technologies;

    case TechnologyActions.CREATE_TECHNOLOGY:
      return [...state, (<TechnologyActions.CreateTechnology>action).technology];

    case TechnologyActions.UPDATE_TECHNOLOGY:
      const updatedTechnology = (<TechnologyActions.UpdateTechnology>action).technology;

      return state.map((techology: Technology) => {
        return techology.id === updatedTechnology.id
          ? Object.assign(Object.create(Technology.prototype), techology, updatedTechnology)
          : techology;
      });

    case TechnologyActions.CREATE_OR_UPDATE_TECHNOLOGY:
      const existingTechnology = state.find((techology: Technology) =>
        techology.id === (<TechnologyActions.CreateOrUpdateTechnology>action).technology.id);

      if (existingTechnology) {
        return technologyReducer(state,
          new TechnologyActions.UpdateTechnology((<TechnologyActions.CreateOrUpdateTechnology>action).technology));
      } else {
        return technologyReducer(state,
          new TechnologyActions.CreateTechnology((<TechnologyActions.CreateOrUpdateTechnology>action).technology));
      }
    default:
      return state;

  }
}
