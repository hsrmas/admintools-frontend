import { Proposal } from '../../domain/proposal';
import { ProposalActions } from '../actions/index';

/**
 * Proposal reducer
 * @param {Proposal[]} state
 * @param {All} action
 * @returns {Proposal[]}
 */
export function proposalReducer(state: Proposal[] = [], action: ProposalActions.All): Proposal[] {

  switch (action.type) {
    case ProposalActions.LOAD_PROPOSALS:
      return (<ProposalActions.LoadProposals>action).proposals;

    case ProposalActions.CREATE_PROPOSAL:
      return [...state, (<ProposalActions.CreateProposal>action).proposal];

    case ProposalActions.UPDATE_PROPOSAL:
      const updatedProposal = (<ProposalActions.UpdateProposal>action).proposal;
      return state.map((proposal: Proposal) => {
        return proposal.id === updatedProposal.id
          ? Object.assign(Object.create(Proposal.prototype), proposal, updatedProposal)
          : proposal;
      });

    case ProposalActions.CREATE_OR_UPDATE_PROPOSAL:
      const exisitingProposal = state.find((proposal: Proposal) =>
        proposal.id === (<ProposalActions.CreateOrUpdateProposal>action).proposal.id);

      if (exisitingProposal) {
        return proposalReducer(state,
          new ProposalActions.UpdateProposal((<ProposalActions.CreateOrUpdateProposal>action).proposal));
      } else {
        return proposalReducer(state,
          new ProposalActions.CreateProposal((<ProposalActions.CreateOrUpdateProposal>action).proposal));
      }

    case ProposalActions.DELETE_PROPOSAL:
      const deleteProposal = (<ProposalActions.DeleteProposal>action).proposal;
      return state.filter((proposal: Proposal) => proposal.id !== deleteProposal.id);
    default:
      return state;
  }

}
