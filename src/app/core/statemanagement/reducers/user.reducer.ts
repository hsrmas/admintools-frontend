import { UserActions } from '../actions';
import { User } from '../../domain/user';

/**
 * User reducer
 * @param {User[]} state
 * @param {All} action
 * @returns {User[]}
 */
export function userReducer(state: User[] = [], action: UserActions.All): User[] {

  switch (action.type) {
    case UserActions.LOAD_USERS:
      return (<UserActions.LoadUsers>action).users;

    case UserActions.CREATE_USER:
      return [...state, (<UserActions.CreateUser>action).user];

    case UserActions.UPDATE_USER:
      const updatedUser = (<UserActions.UpdateUser>action).user;

      return state.map((user: User) => {
        return user.id === updatedUser.id
          ? Object.assign(Object.create(User.prototype), user, updatedUser)
          : user;
      });

    case UserActions.CREATE_OR_UPDATE_USER:
      const existingUser = state.find((user: User) =>
        user.id === (<UserActions.CreateOrUpdateUser>action).user.id);

      if (existingUser) {
        return userReducer(state,
          new UserActions.UpdateUser((<UserActions.CreateOrUpdateUser>action).user));
      } else {
        return userReducer(state,
          new UserActions.CreateUser((<UserActions.CreateOrUpdateUser>action).user));
      }
    default:
      return state;

  }
}
