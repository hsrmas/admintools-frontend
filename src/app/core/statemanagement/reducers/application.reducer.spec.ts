import { Application } from '../../domain/application';
import { applicationReducer } from './application.reducer';
import { ApplicationActions } from '../actions/index';
import { ApplicationMockData } from '../../../unittest/mock-data/application-mock-data';

describe('application reducer', () => {

  const initialState: Application[] = ApplicationMockData.getMockApplications();

  it('returns an empty array by default', () => {
    const defaultState = applicationReducer(undefined, {type: 'DOES_NOT_EXIST'} as ApplicationActions.All);
    expect(defaultState).toEqual([]);
  });

  it(`${ApplicationActions.LOAD_APPLICATIONS}`, () => {
    const payload = initialState;
    const newState = applicationReducer(initialState, new ApplicationActions.LoadApplications(payload));
    expect(JSON.stringify(newState)).toEqual(JSON.stringify(payload));
  });

  it(`${ApplicationActions.CREATE_APPLICATION}`, () => {
    const payload = ApplicationMockData.newApplication();
    const result = [...initialState, payload];
    const newState = applicationReducer(initialState, new ApplicationActions.CreateApplication(payload));
    expect(JSON.stringify(newState)).toEqual(JSON.stringify(result));
  });

  it(`${ApplicationActions.UPDATE_APPLICATION}`, () => {
    const payload = ApplicationMockData.updatedApplication();
    const result = [initialState[0], payload];
    const newState = applicationReducer(initialState, new ApplicationActions.UpdateApplication(payload));
    expect(JSON.stringify(newState)).toEqual(JSON.stringify(result));
  });

  it(`${ApplicationActions.CREATE_OR_UPDATE_APPLICATION}`, () => {
    const payload = ApplicationMockData.updatedApplication();
    const result = [initialState[0], payload];
    const newState = applicationReducer(initialState, new ApplicationActions.CreateOrUpdateApplication(payload));
    expect(JSON.stringify(newState)).toEqual(JSON.stringify(result));
  });

  it(`${ApplicationActions.CREATE_OR_UPDATE_APPLICATION}`, () => {
    const payload = ApplicationMockData.newApplication();
    const result = [...initialState, payload];
    const newState = applicationReducer(initialState, new ApplicationActions.CreateOrUpdateApplication(payload));
    expect(JSON.stringify(newState)).toEqual(JSON.stringify(result));
  });
});
