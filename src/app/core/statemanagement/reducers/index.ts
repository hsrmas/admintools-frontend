import { AppState } from '../app.state';
import {
  Action,
  ActionReducerMap
} from '@ngrx/store';
import { proposalReducer } from './proposal.reducer';
import { technologyReducer } from './technology.reducer';
import { proposalTypeReducer } from './proposal-type.reducer';
import { universityReducer } from './university.reducer';
import { instituteReducer } from './institute.reducer';
import { applicationReducer } from './application.reducer';
import { userReducer } from './user.reducer';


export const appReducers: ActionReducerMap<AppState, Action> = {
  proposals: proposalReducer,
  technologies: technologyReducer,
  proposalTypes: proposalTypeReducer,
  universities: universityReducer,
  institutes: instituteReducer,
  applications: applicationReducer,
  users: userReducer
};
