import { Institute } from '../../domain/institute';
import { instituteReducer } from './institute.reducer';
import { InstituteActions } from '../actions/index';
import { InstituteMockData } from '../../../unittest/mock-data/institute-mock-data';

describe('institute reducer', () => {

  const initialState: Institute[] = InstituteMockData.getMockInstitutes();

  it('should return an empty array', () => {
    const defaultState = instituteReducer(undefined, {type: 'DOES NOT EXIST'} as InstituteActions.All);
  });

  it(`${InstituteActions.LOAD_INSTITUTES}`, () => {
    const payload = initialState;
    const newState = instituteReducer(initialState, new InstituteActions.LoadInstitutes(payload));
    expect(JSON.stringify(initialState)).toEqual(JSON.stringify(newState));
  });

  it(`${InstituteActions.CREATE_INSTITUTE}`, () => {
    const payload = InstituteMockData.newInstitute();
    const result = [...initialState, payload];
    const newState = instituteReducer(initialState, new InstituteActions.CreateInstitute(payload));
    expect(JSON.stringify(newState)).toEqual(JSON.stringify(result));
  });

  it(`${InstituteActions.UPDATE_INSTITUTE}`, () => {
    const payload = InstituteMockData.updatedInstitute();
    const result = [initialState[0], payload];
    const newState = instituteReducer(initialState, new InstituteActions.UpdateInstitute(payload));
    expect(JSON.stringify(newState)).toEqual(JSON.stringify(result));
  });

  it(`${InstituteActions.CREATE_OR_UPDATE_INSTITUTE}`, () => {
    const payload = InstituteMockData.updatedInstitute();
    const result = [initialState[0], payload];
    const newState = instituteReducer(initialState, new InstituteActions.CreateOrUpdateInstitute(payload));
    expect(JSON.stringify(newState)).toEqual(JSON.stringify(result));
  });

  it(`${InstituteActions.CREATE_OR_UPDATE_INSTITUTE}`, () => {
    const payload = InstituteMockData.newInstitute();
    const result = [...initialState, payload];
    const newState = instituteReducer(initialState, new InstituteActions.CreateOrUpdateInstitute(payload));
    expect(JSON.stringify(newState)).toEqual(JSON.stringify(result));
  });
});
