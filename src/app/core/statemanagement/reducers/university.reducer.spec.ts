import { University } from '../../domain/university';
import { universityReducer } from './university.reducer';
import { UniversityActions } from '../actions';
import { UniversityMockData } from '../../../unittest/mock-data/university-mock-data';

const initialState: University[] = UniversityMockData.getMockUniversities();

describe('university reducer', () => {
  it('returns an empty array by default', () => {
    const defaultState = universityReducer(undefined, {type: 'DOES_NOT_EXIST'} as UniversityActions.All);
    expect(defaultState).toEqual([]);
  });

  it(`${UniversityActions.LOAD_UNIVERSITIES}`, () => {
    const payload = initialState;
    const newState = universityReducer(initialState, new UniversityActions.LoadUniversities(payload));
    expect(JSON.stringify(newState)).toEqual(JSON.stringify(payload));
  });

  it(`${UniversityActions.CREATE_UNIVERSITY}`, () => {
    const payload = UniversityMockData.newUniversity();
    const result = [...initialState, payload];
    const newState = universityReducer(initialState, new UniversityActions.CreateUniversity(payload));
    expect(JSON.stringify(newState)).toEqual(JSON.stringify(result));
  });

  it(`${UniversityActions.UPDATE_UNIVERSITY}`, () => {
    const payload = UniversityMockData.updatedUniversity();
    const result = [initialState[0], payload];
    const newState = universityReducer(initialState, new UniversityActions.UpdateUniversity(payload));
    expect(JSON.stringify(newState)).toEqual(JSON.stringify(result));
  });

  it(`${UniversityActions.CREATE_OR_UPDATE_UNIVERSITY}`, () => {
    const payload = UniversityMockData.updatedUniversity();
    const result = [initialState[0], payload];
    const newState = universityReducer(initialState, new UniversityActions.CreateOrUpdateUniversity(payload));
    expect(JSON.stringify(newState)).toEqual(JSON.stringify(result));
  });

  it(`${UniversityActions.CREATE_OR_UPDATE_UNIVERSITY}`, () => {
    const payload = UniversityMockData.newUniversity();
    const result = [...initialState, payload];
    const newState = universityReducer(initialState, new UniversityActions.CreateOrUpdateUniversity(payload));
    expect(JSON.stringify(newState)).toEqual(JSON.stringify(result));
  });
});
