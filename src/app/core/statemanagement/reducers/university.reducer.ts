import { UniversityActions } from '../actions';
import { University } from '../../domain/university';

/**
 * University Reducer
 * @param {University[]} state
 * @param {All} action
 * @returns {University[]}
 */
export function universityReducer(state: University[] = [], action: UniversityActions.All): University[] {

  switch (action.type) {
    case UniversityActions.LOAD_UNIVERSITIES:
      return (<UniversityActions.LoadUniversities>action).universities;

    case UniversityActions.CREATE_UNIVERSITY:
      return [...state, (<UniversityActions.CreateUniversity>action).university];

    case UniversityActions.UPDATE_UNIVERSITY:
      const updatedUniversity = (<UniversityActions.UpdateUniversity>action).university;

      return state.map((university: University) => {
        return university.id === updatedUniversity.id
          ? Object.assign(Object.create(University.prototype), university, updatedUniversity)
          : university;
      });

    case UniversityActions.CREATE_OR_UPDATE_UNIVERSITY:
      const existingUniversity = state.find((university: University) =>
        university.id === (<UniversityActions.CreateOrUpdateUniversity>action).university.id);

      if (existingUniversity) {
        return universityReducer(state,
          new UniversityActions.UpdateUniversity((<UniversityActions.CreateOrUpdateUniversity>action).university));
      } else {
        return universityReducer(state,
          new UniversityActions.CreateUniversity((<UniversityActions.CreateOrUpdateUniversity>action).university));
      }
    default:
      return state;

  }
}
