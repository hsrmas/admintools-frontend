import { ProposalType } from '../../domain/proposal-type';
import { ProposalTypeActions } from '../actions';
import { proposalTypeReducer } from './proposal-type.reducer';
import { ProposalTypeMockData } from '../../../unittest/mock-data/proposal-type-mock-data';


const initialState: ProposalType[] = ProposalTypeMockData.getMockProposalTypes();

describe('proposal-type reducer', () => {

  it('returns an empty array by default', () => {
    const defaultState = proposalTypeReducer(undefined, {type: 'DOES_NOT_EXIST'} as ProposalTypeActions.All);
    expect(defaultState).toEqual([]);
  });

  it(`${ProposalTypeActions.LOAD_PROPOSALTYPES}`, () => {
    const payload = initialState;
    const newState = proposalTypeReducer(initialState, new ProposalTypeActions.LoadProposalTypes(payload));
    expect(JSON.stringify(newState)).toEqual(JSON.stringify(payload));
  });

  it(`${ProposalTypeActions.CREATE_PROPOSALTYPE}`, () => {
    const payload = ProposalTypeMockData.newProposalType();
    const result = [...initialState, payload];
    const newState = proposalTypeReducer(initialState, new ProposalTypeActions.CreateProposalType(payload));
    expect(JSON.stringify(newState)).toEqual(JSON.stringify(result));
  });

  it(`${ProposalTypeActions.UPDATE_PROPOSALTYPE}`, () => {
    const payload = ProposalTypeMockData.updatedProposalType();
    const result = [initialState[0], payload];
    const newState = proposalTypeReducer(initialState, new ProposalTypeActions.UpdateProposalType(payload));
    expect(JSON.stringify(newState)).toEqual(JSON.stringify(result));
  });

  it(`${ProposalTypeActions.CREATE_OR_UPDATE_PROPOSALTYPE}`, () => {
    const payload = ProposalTypeMockData.updatedProposalType();
    const result = [initialState[0], payload];
    const newState = proposalTypeReducer(initialState, new ProposalTypeActions.CreateOrUpdateProposalType(payload));
    expect(JSON.stringify(newState)).toEqual(JSON.stringify(result));
  });

  it(`${ProposalTypeActions.CREATE_OR_UPDATE_PROPOSALTYPE}`, () => {
    const payload = ProposalTypeMockData.newProposalType();
    const result = [...initialState, payload];
    const newState = proposalTypeReducer(initialState, new ProposalTypeActions.CreateOrUpdateProposalType(payload));
    expect(JSON.stringify(newState)).toEqual(JSON.stringify(result));
  });
});
