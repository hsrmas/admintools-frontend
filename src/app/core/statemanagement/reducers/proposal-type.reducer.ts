import { ProposalTypeActions } from '../actions';
import { ProposalType } from '../../domain/proposal-type';

/**
 * Proposal-Type reducer
 * @param {ProposalType[]} state
 * @param {All} action
 * @returns {ProposalType[]}
 */
export function proposalTypeReducer(state: ProposalType[] = [], action: ProposalTypeActions.All): ProposalType[] {

  switch (action.type) {
    case ProposalTypeActions.LOAD_PROPOSALTYPES:
      return (<ProposalTypeActions.LoadProposalTypes>action).proposalTypes;

    case ProposalTypeActions.CREATE_PROPOSALTYPE:
      return [...state, (<ProposalTypeActions.CreateProposalType>action).proposalType];

    case ProposalTypeActions.UPDATE_PROPOSALTYPE:
      const updatedTechnology = (<ProposalTypeActions.UpdateProposalType>action).proposalType;

      return state.map((proposalType: ProposalType) => {
        return proposalType.id === updatedTechnology.id
          ? Object.assign(Object.create(ProposalType.prototype), proposalType, updatedTechnology)
          : proposalType;
      });

    case ProposalTypeActions.CREATE_OR_UPDATE_PROPOSALTYPE:
      const existingProposalType = state.find((proposalType: ProposalType) =>
        proposalType.id === (<ProposalTypeActions.CreateOrUpdateProposalType>action).proposalType.id);

      if (existingProposalType) {
        return proposalTypeReducer(state,
          new ProposalTypeActions.UpdateProposalType((<ProposalTypeActions.CreateOrUpdateProposalType>action).proposalType));
      } else {
        return proposalTypeReducer(state,
          new ProposalTypeActions.CreateProposalType((<ProposalTypeActions.CreateOrUpdateProposalType>action).proposalType));
      }
    default:
      return state;

  }
}
