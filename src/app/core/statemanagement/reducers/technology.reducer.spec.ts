import { Technology } from '../../domain/technology';
import { TechnologyActions } from '../actions';
import { technologyReducer } from './technology.reducer';
import { TechnologyMockData } from '../../../unittest/mock-data/technology-mock-data';

const initialState: Technology[] = TechnologyMockData.getMockTechnologies();

describe('technology reducer', () => {

  it('returns an empty array by default', () => {
    const defaultState = technologyReducer(undefined, {type: 'DOES_NOT_EXIST'} as TechnologyActions.All);
    expect(defaultState).toEqual([]);
  });

  it(`${TechnologyActions.LOAD_TECHNOLOGIES}`, () => {
    const payload = initialState;
    const newState = technologyReducer(initialState, new TechnologyActions.LoadTechnologies(payload));
    expect(JSON.stringify(newState)).toEqual(JSON.stringify(payload));
  });

  it(`${TechnologyActions.CREATE_TECHNOLOGY}`, () => {
    const payload = TechnologyMockData.newTechnology();
    const result = [...initialState, payload];
    const newState = technologyReducer(initialState, new TechnologyActions.CreateTechnology(payload));
    expect(JSON.stringify(newState)).toEqual(JSON.stringify(result));
  });

  it(`${TechnologyActions.UPDATE_TECHNOLOGY}`, () => {
    const payload = TechnologyMockData.updatedTechnology();
    const result = [initialState[0], payload];
    const newState = technologyReducer(initialState, new TechnologyActions.UpdateTechnology(payload));
    expect(JSON.stringify(newState)).toEqual(JSON.stringify(result));
  });

  it(`${TechnologyActions.CREATE_OR_UPDATE_TECHNOLOGY}`, () => {
    const payload = TechnologyMockData.updatedTechnology();
    const result = [initialState[0], payload];
    const newState = technologyReducer(initialState, new TechnologyActions.CreateOrUpdateTechnology(payload));
    expect(JSON.stringify(newState)).toEqual(JSON.stringify(result));
  });

  it(`${TechnologyActions.CREATE_OR_UPDATE_TECHNOLOGY}`, () => {
    const payload = TechnologyMockData.newTechnology();
    const result = [...initialState, payload];
    const newState = technologyReducer(initialState, new TechnologyActions.CreateOrUpdateTechnology(payload));
    expect(JSON.stringify(newState)).toEqual(JSON.stringify(result));
  });
});
