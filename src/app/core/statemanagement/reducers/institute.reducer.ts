import { InstituteActions } from '../actions';
import { Institute } from '../../domain/institute';

/**
 * Institute Reducer
 * @param {Institute[]} state
 * @param {All} action
 * @returns {Institute[]}
 */
export function instituteReducer(state: Institute[] = [], action: InstituteActions.All): Institute[] {

  switch (action.type) {
    case InstituteActions.LOAD_INSTITUTES:
      return (<InstituteActions.LoadInstitutes>action).institutes;

    case InstituteActions.CREATE_INSTITUTE:
      return [...state, (<InstituteActions.CreateInstitute>action).institute];

    case InstituteActions.UPDATE_INSTITUTE:
      const updatedInstitute = (<InstituteActions.UpdateInstitute>action).institute;

      return state.map((institute: Institute) => {
        return institute.id === updatedInstitute.id
          ? Object.assign(Object.create(Institute.prototype), institute, updatedInstitute)
          : institute;
      });

    case InstituteActions.CREATE_OR_UPDATE_INSTITUTE:
      const existingInstitute = state.find((institute: Institute) =>
        institute.id === (<InstituteActions.CreateOrUpdateInstitute>action).institute.id);

      if (existingInstitute) {
        return instituteReducer(state,
          new InstituteActions.UpdateInstitute((<InstituteActions.CreateOrUpdateInstitute>action).institute));
      } else {
        return instituteReducer(state,
          new InstituteActions.CreateInstitute((<InstituteActions.CreateOrUpdateInstitute>action).institute));
      }
    default:
      return state;

  }
}
