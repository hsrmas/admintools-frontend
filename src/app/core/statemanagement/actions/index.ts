import * as ProposalActions from './proposal.actions';
import * as TechnologyActions from './technology.action';
import * as ProposalTypeActions from './proposal-types.action';
import * as UniversityActions from './university.actions';
import * as InstituteActions from './institute.actions';
import * as ApplicationActions from './application.action';
import * as UserActions from './user.actions';

export {
  ProposalActions,
  TechnologyActions,
  ProposalTypeActions,
  UniversityActions,
  InstituteActions,
  ApplicationActions,
  UserActions
};
