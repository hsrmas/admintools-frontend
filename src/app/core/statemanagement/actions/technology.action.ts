import { Action } from '@ngrx/store';
import { Technology } from '../../domain/technology';

const ACTION_PREFIX = '[Technologies]';

export const LOAD_TECHNOLOGIES = `${ACTION_PREFIX} Load TECHNOLOGIES`;
export const CREATE_TECHNOLOGY = `${ACTION_PREFIX} Create TECHNOLOGY`;
export const UPDATE_TECHNOLOGY = `${ACTION_PREFIX} Update TECHNOLOGY`;
export const CREATE_OR_UPDATE_TECHNOLOGY = `${ACTION_PREFIX} Create or Update TECHNOLOGIES`;


export class LoadTechnologies implements Action {
  readonly type = LOAD_TECHNOLOGIES;

  constructor(public technologies: Technology[]) {
  }
}

export class CreateTechnology implements Action {
  readonly type = CREATE_TECHNOLOGY;

  constructor(public technology: Technology) {
  }
}

export class UpdateTechnology implements Action {
  readonly type = UPDATE_TECHNOLOGY;

  constructor(public technology: Technology) {
  }
}

export class CreateOrUpdateTechnology implements Action {
  readonly type = CREATE_OR_UPDATE_TECHNOLOGY;

  constructor(public technology: Technology) {
  }
}


export type All
  = LoadTechnologies
  | CreateOrUpdateTechnology;
