import { Action } from '@ngrx/store';
import { Proposal } from '../../domain/proposal';


const ACTION_PREFIX = '[Proposals]';

export const LOAD_PROPOSALS = `${ACTION_PREFIX} Load PROPOSALS`;
export const CREATE_PROPOSAL = `${ACTION_PREFIX} Create PROPOSAL`;
export const UPDATE_PROPOSAL = `${ACTION_PREFIX} Update PROPOSAL`;
export const CREATE_OR_UPDATE_PROPOSAL = `${ACTION_PREFIX} Create or Update PROPOSAL`;
export const DELETE_PROPOSAL = `${ACTION_PREFIX} Delete PROPOSAL`;


export class LoadProposals implements Action {
  readonly type = LOAD_PROPOSALS;

  constructor(public proposals: Proposal[]) {
  }
}

export class CreateProposal implements Action {
  readonly type = CREATE_PROPOSAL;

  constructor(public proposal: Proposal) {
  }
}

export class UpdateProposal implements Action {
  readonly type = UPDATE_PROPOSAL;

  constructor(public proposal: Proposal) {
  }
}

export class CreateOrUpdateProposal implements Action {
  readonly type = CREATE_OR_UPDATE_PROPOSAL;

  constructor(public proposal: Proposal) {
  }
}

export class DeleteProposal implements Action {
  readonly type = DELETE_PROPOSAL;

  constructor(public proposal: Proposal) {
  }
}

export type All
  = LoadProposals
  | CreateProposal
  | UpdateProposal
  | CreateOrUpdateProposal
  | DeleteProposal;
