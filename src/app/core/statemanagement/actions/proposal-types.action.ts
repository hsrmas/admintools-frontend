import { Action } from '@ngrx/store';
import { ProposalType } from '../../domain/proposal-type';

const ACTION_PREFIX = '[ProposalTypes]';

export const LOAD_PROPOSALTYPES = `${ACTION_PREFIX} Load ProposalTypes`;
export const CREATE_PROPOSALTYPE = `${ACTION_PREFIX} Create ProposalType`;
export const UPDATE_PROPOSALTYPE = `${ACTION_PREFIX} Update ProposalType`;
export const CREATE_OR_UPDATE_PROPOSALTYPE = `${ACTION_PREFIX} Create or Update ProposalType`;


export class LoadProposalTypes implements Action {
  readonly type = LOAD_PROPOSALTYPES;

  constructor(public proposalTypes: ProposalType[]) {
  }
}

export class CreateProposalType implements Action {
  readonly type = CREATE_PROPOSALTYPE;

  constructor(public proposalType: ProposalType) {
  }
}

export class UpdateProposalType implements Action {
  readonly type = UPDATE_PROPOSALTYPE;

  constructor(public proposalType: ProposalType) {
  }
}

export class CreateOrUpdateProposalType implements Action {
  readonly type = CREATE_OR_UPDATE_PROPOSALTYPE;

  constructor(public proposalType: ProposalType) {
  }
}


export type All
  = LoadProposalTypes
  | CreateOrUpdateProposalType;
