import { Action } from '@ngrx/store';
import { Application } from '../../domain/application';

const ACTION_PREFIX = '[Application]';

export const LOAD_APPLICATIONS = `${ACTION_PREFIX} Load APPLICATIONS`;
export const CREATE_APPLICATION = `${ACTION_PREFIX} Create APPLICATION`;
export const UPDATE_APPLICATION = `${ACTION_PREFIX} Update APPLICATION`;
export const CREATE_OR_UPDATE_APPLICATION = `${ACTION_PREFIX} Create or Update APPLICATIONS`;


export class LoadApplications implements Action {
  readonly type = LOAD_APPLICATIONS;

  constructor(public applications: Application[]) {
  }
}

export class CreateApplication implements Action {
  readonly type = CREATE_APPLICATION;

  constructor(public application: Application) {
  }
}

export class UpdateApplication implements Action {
  readonly type = UPDATE_APPLICATION;

  constructor(public application: Application) {
  }
}

export class CreateOrUpdateApplication implements Action {
  readonly type = CREATE_OR_UPDATE_APPLICATION;

  constructor(public application: Application) {
  }
}

export type All
  = LoadApplications
  | CreateOrUpdateApplication;
