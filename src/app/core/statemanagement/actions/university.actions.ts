import { Action } from '@ngrx/store';
import { University } from '../../domain/university';

const ACTION_PREFIX = '[University]';

export const LOAD_UNIVERSITIES = `${ACTION_PREFIX} Load UNIVERSITIES`;
export const CREATE_UNIVERSITY = `${ACTION_PREFIX} Create UNIVERSITY`;
export const UPDATE_UNIVERSITY = `${ACTION_PREFIX} Update UNIVERSITY`;
export const CREATE_OR_UPDATE_UNIVERSITY = `${ACTION_PREFIX} Create or Update UNIVERSITIES`;


export class LoadUniversities implements Action {
  readonly type = LOAD_UNIVERSITIES;

  constructor(public universities: University[]) {
  }
}

export class CreateUniversity implements Action {
  readonly type = CREATE_UNIVERSITY;

  constructor(public university: University) {
  }
}

export class UpdateUniversity implements Action {
  readonly type = UPDATE_UNIVERSITY;

  constructor(public university: University) {
  }
}

export class CreateOrUpdateUniversity implements Action {
  readonly type = CREATE_OR_UPDATE_UNIVERSITY;

  constructor(public university: University) {
  }
}

export type All
  = LoadUniversities
  | CreateOrUpdateUniversity;
