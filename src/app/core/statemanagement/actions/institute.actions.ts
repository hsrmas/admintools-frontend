import { Action } from '@ngrx/store';
import { Institute } from '../../domain/institute';

const ACTION_PREFIX = '[Institute]';

export const LOAD_INSTITUTES = `${ACTION_PREFIX} Load INSTITUTES`;
export const CREATE_INSTITUTE = `${ACTION_PREFIX} Create INSTITUTE`;
export const UPDATE_INSTITUTE = `${ACTION_PREFIX} Update INSTITUTE`;
export const CREATE_OR_UPDATE_INSTITUTE = `${ACTION_PREFIX} Create or Update INSTITUTES`;


export class LoadInstitutes implements Action {
  readonly type = LOAD_INSTITUTES;

  constructor(public institutes: Institute[]) {
  }
}

export class CreateInstitute implements Action {
  readonly type = CREATE_INSTITUTE;

  constructor(public institute: Institute) {
  }
}

export class UpdateInstitute implements Action {
  readonly type = UPDATE_INSTITUTE;

  constructor(public institute: Institute) {
  }
}

export class CreateOrUpdateInstitute implements Action {
  readonly type = CREATE_OR_UPDATE_INSTITUTE;

  constructor(public institute: Institute) {
  }
}


export type All
  = LoadInstitutes
  | CreateOrUpdateInstitute;
