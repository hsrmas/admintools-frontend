import { Action } from '@ngrx/store';
import { User } from '../../domain/user';

const ACTION_PREFIX = '[User]';

export const LOAD_USERS = `${ACTION_PREFIX} Load USERS`;
export const CREATE_USER = `${ACTION_PREFIX} Create USER`;
export const UPDATE_USER = `${ACTION_PREFIX} Update USER`;
export const CREATE_OR_UPDATE_USER = `${ACTION_PREFIX} Create or Update USERS`;


export class LoadUsers implements Action {
  readonly type = LOAD_USERS;

  constructor(public users: User[]) {
  }
}

export class CreateUser implements Action {
  readonly type = CREATE_USER;

  constructor(public user: User) {
  }
}

export class UpdateUser implements Action {
  readonly type = UPDATE_USER;

  constructor(public user: User) {
  }
}

export class CreateOrUpdateUser implements Action {
  readonly type = CREATE_OR_UPDATE_USER;

  constructor(public user: User) {
  }
}

export type All
  = LoadUsers
  | CreateOrUpdateUser;
