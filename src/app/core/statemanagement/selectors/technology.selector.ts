import { AppState } from '../app.state';
import { createSelector } from '@ngrx/store';
import { Technology } from '../../domain/technology';

export const allTechnologies = (state: AppState) => state.technologies;

export const findTechnologyIdByKey = (technologykey: string) => createSelector(allTechnologies,
  (technologies: Technology[]): Technology => {
    if (technologies) {
      return technologies.find((technology: Technology) => technology.key === technologykey);
    }
  });

export const findTechnologyById = (technologyId: number) => createSelector(allTechnologies,
  (technologies: Technology[]): Technology => {
    if (technologies) {
      return technologies.find((technology: Technology) => technology.id === technologyId);
    }
  });
