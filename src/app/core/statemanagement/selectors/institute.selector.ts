import { AppState } from '../app.state';
import { createSelector } from '@ngrx/store';
import { Institute } from '../../domain/institute';

export const allInstitutes = (state: AppState) => state.institutes;

export const findInstitutesByUniversity = (searchString: string) => createSelector(allInstitutes,
  (institutes: Institute[]): Institute[] => {
    if (institutes) {
      return institutes.filter((institute: Institute) => institute.university === searchString);
    }
  });


export const findUniversityByURL = (universityURL: string) => createSelector(allInstitutes,
  (institutes: Institute[]): Institute => {
    if (institutes) {
      return institutes.find((institute: Institute) => institute.url === universityURL);
    }
  });
