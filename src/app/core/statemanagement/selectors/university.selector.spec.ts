import { University } from '../../domain/university';
import { AppState } from '../app.state';
import {
  allUniversities,
  findUniversityById,
  findUniversityByURL
} from './university.selector';
import { UniversityMockData } from '../../../unittest/mock-data/university-mock-data';

const universities = <University[]>[UniversityMockData.university1, UniversityMockData.university2];

const appState = {
  universities: universities
} as AppState;

describe('university.selector', () => {

  it('should return all universities', () => {
    expect(allUniversities(appState)).toEqual(universities);
  });

  it('should return university1 with findUniversityById(1)', () => {
    expect(findUniversityById(1).projector(universities)).toEqual(UniversityMockData.university1);
  });

  it('should return undefined with findUniversityById(99)', () => {
    expect(findUniversityById(99).projector(universities)).toEqual(undefined);
  });

  it('should return university2 with findUniversityByURL(url2)', () => {
    expect(findUniversityByURL('url2').projector(universities)).toEqual(UniversityMockData.university2);
  });

  it('should return undefined with findUniversityByURL(non-url)', () => {
    expect(findUniversityByURL('non-url').projector(universities)).toEqual(undefined);
  });

});
