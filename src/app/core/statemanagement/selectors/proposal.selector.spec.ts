import { Proposal } from '../../domain/proposal';
import { AppState } from '../app.state';
import {
  allProposals,
  findProposalByApplicationId,
  findProposalById,
  findProposalByName
} from './proposal.selector';
import { ProposalMockData } from '../../../unittest/mock-data/proposal-mock-data';

const proposals = <Proposal[]>[ProposalMockData.proposal1, ProposalMockData.proposal2, ProposalMockData.proposal3];

const appState = {
  proposals: proposals
} as AppState;

describe('proposal.selector', () => {

  it('should return all proposals', () => {
    expect(allProposals(appState)).toEqual(proposals);
  });

  it('should return proposal1 with findProposalById(1)', () => {
    expect(findProposalById(1).projector(proposals)).toEqual(ProposalMockData.proposal1);
  });

  it('should return undefined with findProposalById(99)', () => {
    expect(findProposalById(99).projector(proposals)).toEqual(undefined);
  });

  it('should return proposal1 with findProposalByName(proposal2)', () => {
    expect(findProposalByName('proposal2').projector(proposals)).toEqual(ProposalMockData.proposal2);
  });

  it('should return undefined with findProposalByName(non-proposal)', () => {
    expect(findProposalByName('non-proposal').projector(proposals)).toEqual(undefined);
  });

  it('should return proposal1 with findProposalByApplicationId(1)', () => {
    expect(findProposalByApplicationId(1).projector(proposals)).toEqual(ProposalMockData.proposal1);
  });

  it('should return undefined with findProposalByApplicationId(99)', () => {
    expect(findProposalByApplicationId(99).projector(proposals)).toEqual(undefined);
  });

});
