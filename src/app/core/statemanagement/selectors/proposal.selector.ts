import { AppState } from '../app.state';
import { createSelector } from '@ngrx/store';
import { Proposal } from '../../domain/proposal';

export const allProposals = (state: AppState) => state.proposals;


export const findProposalById = (searchProposalId: number) => createSelector(allProposals,
  (proposals: Proposal[]): Proposal => {
    if (proposals) {
      return proposals.find((proposal: Proposal) => proposal.id === searchProposalId);
    }
  });

export const findProposalByName = (searchProposalName: string) => createSelector(allProposals,
  (proposals: Proposal[]): Proposal => {
    if (proposals) {
      return proposals.find((proposal: Proposal) => proposal.name === searchProposalName);
    }
  });
export const findProposalByApplicationId = (applicationId: number) => createSelector(allProposals,
  (proposals: Proposal[]): Proposal => {
    if (proposals) {
      return proposals.find((proposal: Proposal) => proposal.application_id === applicationId);
    }
  });
