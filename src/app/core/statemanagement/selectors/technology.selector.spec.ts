import { Technology } from '../../domain/technology';
import { AppState } from '../app.state';
import {
  allTechnologies,
  findTechnologyById,
  findTechnologyIdByKey
} from './technology.selector';
import { TechnologyMockData } from '../../../unittest/mock-data/technology-mock-data';

const technologies = <Technology[]>[TechnologyMockData.technology1, TechnologyMockData.technology2, TechnologyMockData.technology3];

const appState = {
  technologies: technologies
} as AppState;

describe('technology.selector', () => {

  it('should return all technologies', () => {
    expect(allTechnologies(appState)).toEqual(technologies);
  });


  it('should return technology1 with findTechnologyIdByKey(key1)', () => {
    expect(findTechnologyIdByKey('key1').projector(technologies)).toEqual(TechnologyMockData.technology1);
  });

  it('should return undefined with findTechnologyIdByKey(key99)', () => {
    expect(findTechnologyIdByKey('key99').projector(technologies)).toEqual(undefined);
  });


  it('should return technology2 with findTechnologyById(2)', () => {
    expect(findTechnologyById(2).projector(technologies)).toEqual(TechnologyMockData.technology2);
  });

  it('should return undefined with findTechnologyById(99)', () => {
    expect(findTechnologyById(99).projector(technologies)).toEqual(undefined);
  });

});
