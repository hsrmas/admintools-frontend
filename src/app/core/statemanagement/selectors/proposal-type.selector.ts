import { AppState } from '../app.state';
import { createSelector } from '@ngrx/store';
import { ProposalType } from '../../domain/proposal-type';


export const allProposalTypes = (state: AppState) => state.proposalTypes;

export const findProposalTypeIdByKey = (proposalTypeKey: string) => createSelector(allProposalTypes,
  (proposalTypes: ProposalType[]): ProposalType => {
    if (proposalTypes) {
      return proposalTypes.find((proposalType: ProposalType) => proposalType.key === proposalTypeKey);
    }
  });

export const findProposaTypeById = (proposalTypeId: number) => createSelector(allProposalTypes,
  (proposalTypes: ProposalType[]): ProposalType => {
    if (proposalTypes) {
      return proposalTypes.find((proposalType: ProposalType) => proposalType.id === proposalTypeId);
    }
  });

