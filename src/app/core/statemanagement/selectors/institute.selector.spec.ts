import { Institute } from '../../domain/institute';
import { AppState } from '../app.state';
import {
  allInstitutes,
  findInstitutesByUniversity,
  findUniversityByURL,
} from './institute.selector';
import { InstituteMockData } from '../../../unittest/mock-data/institute-mock-data';

const institutes = <Institute[]>[InstituteMockData.institute1, InstituteMockData.institute2, InstituteMockData.institute3];

const appState = {
  institutes: institutes
} as AppState;

describe('institute.selector', () => {

  it('should return all institutes', () => {
    expect(allInstitutes(appState)).toEqual(institutes);
  });

  it('should find institute1 with findUniversityByURL(url1)', () => {
    expect(findUniversityByURL('url1').projector(institutes)).toEqual(InstituteMockData.institute1);
  });

  it('should return undefined with findUniversityByURL(failurl)', () => {
    expect(findUniversityByURL('failurl').projector(institutes)).toEqual(undefined);
  });

  it('should find institute2 with findInstitutesByUniversity(uni2)', () => {
    const result = findInstitutesByUniversity('uni2').projector(institutes);
    expect(result).toEqual([InstituteMockData.institute2, InstituteMockData.institute3]);
  });

  it('should return undefined with findInstitutesByUniversity(fail-uni)', () => {
    expect(findInstitutesByUniversity('fail-uni').projector(institutes)).toEqual([]);
  });

});
