import { AppState } from '../app.state';
import { createSelector } from '@ngrx/store';
import { Application } from '../../domain/application';

export const allApplications = (state: AppState) => state.applications;


export const findApplicationById = (searchApplicationId: number) => createSelector(allApplications,
  (applications: Application[]): Application => {
    if (applications) {
      return applications.find((application: Application) => application.id === searchApplicationId);
    }
  });

export const findApplicationByName = (searchApplicationName: string) => createSelector(allApplications,
  (applications: Application[]): Application => {
    if (applications) {
      return applications.find((application: Application) => application.name === searchApplicationName);
    }
  });
