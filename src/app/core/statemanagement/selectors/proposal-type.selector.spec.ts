import { AppState } from '../app.state';
import { ProposalType } from '../../domain/proposal-type';
import {
  allProposalTypes,
  findProposalTypeIdByKey,
  findProposaTypeById,
} from './proposal-type.selector';
import { ProposalTypeMockData } from '../../../unittest/mock-data/proposal-type-mock-data';

const proposalTypes = <ProposalType[]>[
  ProposalTypeMockData.proposalType1,
  ProposalTypeMockData.proposalType2,
  ProposalTypeMockData.proposalType3
];

const appState = {
  proposalTypes: proposalTypes
} as AppState;

describe('proposal-type.selector', () => {

  it('should return all proposal-types', () => {
    expect(allProposalTypes(appState)).toEqual(proposalTypes);
  });

  it('should return proposalType1 with findProposalTypeIdByKey(key1)', () => {
    expect(findProposalTypeIdByKey('key1').projector(proposalTypes)).toEqual(ProposalTypeMockData.proposalType1);
  });

  it('should return undefined with findProposalTypeIdByKey(no-key)', () => {
    expect(findProposalTypeIdByKey('no-key').projector(proposalTypes)).toEqual(undefined);
  });


  it('should return proposalType2 with findProposalById(2)', () => {
    expect(findProposaTypeById(2).projector(proposalTypes)).toEqual(ProposalTypeMockData.proposalType2);
  });

  it('should return undefined with findProposaTypeById(99)', () => {
    expect(findProposaTypeById(99).projector(proposalTypes)).toEqual(undefined);
  });

});
