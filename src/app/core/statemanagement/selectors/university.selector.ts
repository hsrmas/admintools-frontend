import { AppState } from '../app.state';
import { createSelector } from '@ngrx/store';
import { University } from '../../domain/university';

export const allUniversities = (state: AppState) => state.universities;

export const findUniversityById = (id: number) => createSelector(allUniversities, (universities: University[]): University => {
  if (universities) {
    return universities.find((university: University) => university.id === id);
  }
});


export const findUniversityByURL = (universityURL: string) => createSelector(allUniversities,
  (universities: University[]): University => {
    if (universities) {
      return universities.find((university: University) => university.url === universityURL);
    }
  });
