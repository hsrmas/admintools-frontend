import { Application } from '../../domain/application';
import { AppState } from '../app.state';
import {
  allApplications,
  findApplicationById,
  findApplicationByName
} from './application.selector';
import { ApplicationMockData } from '../../../unittest/mock-data/application-mock-data';

const applications = <Application[]>[
  ApplicationMockData.firstApplication,
  ApplicationMockData.secondApplication,
  ApplicationMockData.thirdApplication
];
const applicationsEmpty = <Application[]>[];

const appState = {
  applications: applications
} as AppState;

const appStateEmpty = {
  applications: applicationsEmpty
} as AppState;


describe('application.selector', () => {

  it('should return all applications', () => {
    expect(allApplications(appState)).toEqual(applications);
  });

  it('should return empty array', () => {
    expect(allApplications(appStateEmpty)).toEqual(applicationsEmpty);
  });

  it('should return correct firstApplication with findApplicationByName(name1)', () => {
    expect(findApplicationByName('name1').projector(applications)).toEqual(ApplicationMockData.firstApplication);
  });

  it('should return undefined with finApplicationByName(fail)', () => {
    expect(findApplicationByName('fail').projector(applications)).toEqual(undefined);
  });

  it('should return secondApplication with findApplicationById(1)', () => {
    expect(findApplicationById(2).projector(applications)).toEqual(ApplicationMockData.secondApplication);
  });

  it('should return undefined with findApplicationById(99)', () => {
    expect(findApplicationById(99).projector(applications)).toEqual(undefined);
  });

});
