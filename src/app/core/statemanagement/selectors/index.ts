import * as ProposalSelector from './proposal.selector';
import * as InstituteSelector from './institute.selector';
import * as UniversitySelector from './university.selector';
import * as ProposalTypeSelector from './proposal-type.selector';
import * as TechnologySelector from './technology.selector';
import * as ApplicationSelector from './application.selector';

export {
  ProposalSelector,
  InstituteSelector,
  UniversitySelector,
  ProposalTypeSelector,
  TechnologySelector,
  ApplicationSelector
};
