import { TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { FormControl } from '@angular/forms';
import { ApplicationNameValidator } from './application-name.validator';
import { Application } from '../domain/application';
import { ApplicationService } from '../services/adminTools/application.service';
import { ApplicationMockData } from '../../unittest/mock-data/application-mock-data';


describe('application name validator', () => {

  let applicationNameValidator: ApplicationNameValidator;
  let applicationServiceMock: ApplicationServiceMock;
  let mockStore: MockStore;

  beforeEach(() => {

    TestBed.configureTestingModule({
      declarations: [],
      imports: [],
      providers: [
        ApplicationNameValidator,
        {provide: ApplicationService, useClass: ApplicationServiceMock},
        {provide: Store, useClass: MockStore}
      ]
    });
    applicationNameValidator = TestBed.get(ApplicationNameValidator);
    applicationServiceMock = TestBed.get(ApplicationService);
    mockStore = TestBed.get(Store);
  });


  describe('should validate', () => {

    it('validate for existing name', () => {
      applicationNameValidator.validate(new FormControl('test'))
        .subscribe((res: boolean) => {
          expect(res).toEqual(true);
        });
    });

    it('validate for none existing name', () => {
      applicationNameValidator.validate(new FormControl('lalala'))
        .subscribe((res: boolean) => {
          expect(res).toEqual(false);
        });
    });
  });
});


class ApplicationServiceMock {
  getApplicationFromStoreByName(name: string): Application {
    if (name === 'test') {
      return ApplicationMockData.newApplication();
    } else {
      return null;
    }
  }
}

class MockStore {
}
