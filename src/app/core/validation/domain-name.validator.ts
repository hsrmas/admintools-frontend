import { FormControl } from '@angular/forms';

/**
 * Custom Validator
 *
 * Validate the 'verzeichnis name' for a new proposal
 *
 * Allowed:
 * - lowercase letters
 * - numbers
 * - special characters: '-', '_'
 *
 * @param {FormControl} control
 * @returns {{[p: string]: boolean}}
 */
export function domainNameValidator(control: FormControl): { [value: string]: boolean } {
  const REGEX = RegExp('^[a-z0-9_-]+$', 'g');
  if (!REGEX.test(control.value)) {
    return {invalidDomainName: true};
  }
}

