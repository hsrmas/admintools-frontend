import { ProposalNameValidator } from './proposal-name.validator';
import { TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { FormControl } from '@angular/forms';
import { Proposal } from '../domain/proposal';
import { ProposalService } from '../services/adminTools/proposal.service';
import { ProposalMockData } from '../../unittest/mock-data/proposal-mock-data';


describe('proposal name validator', () => {

  let proposalNameValidator: ProposalNameValidator;
  let mockStore: MockStore;
  let mockService: ProposalServiceMock;


  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      providers: [
        ProposalNameValidator,
        {provide: ProposalService, useClass: ProposalServiceMock},
        {provide: Store, useClass: MockStore}
      ]
    });
    proposalNameValidator = TestBed.get(ProposalNameValidator);
    mockStore = TestBed.get(Store);
    mockService = TestBed.get(ProposalService);
  });


  describe('should validate', () => {

    it('validate for existing name', () => {
      proposalNameValidator.validate(new FormControl('test'))
        .subscribe((res: boolean) => {
          expect(res).toEqual(true);
        });
    });

    it('validate for not existing name', () => {
      proposalNameValidator.validate(new FormControl('lalala'))
        .subscribe((res: boolean) => {
          expect(res).toEqual(false);
        });
    });
  });
});


class ProposalServiceMock {
  getProposalFromStoreByName(name: string): Proposal {
    if (name === 'test') {
      return ProposalMockData.newProposal();
    } else {
      return null;
    }
  }
}

class MockStore {
}
