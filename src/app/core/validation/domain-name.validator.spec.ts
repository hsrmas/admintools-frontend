import { domainNameValidator } from './domain-name.validator';
import { FormControl } from '@angular/forms';

describe('domain name validator', () => {

  it('should validate undefined (ok) for testdomainname', () => {
    expect(domainNameValidator(new FormControl('testdomainname'))).toEqual(undefined);
  });

  it('should validate undefined (ok) for test-domain', () => {
    expect(domainNameValidator(new FormControl('test-domain'))).toEqual(undefined);
  });

  it('should validate undefined (ok) for test_domain', () => {
    expect(domainNameValidator(new FormControl('test_domain'))).toEqual(undefined);
  });

  it('should validate {invalidDomainName: true} for test67&%name', () => {
    expect(domainNameValidator(new FormControl('test67&%name'))).toEqual({invalidDomainName: true});
  });

  it('should validate {invalidDomainName: true} for testName', () => {
    expect(domainNameValidator(new FormControl('testName'))).toEqual({invalidDomainName: true});
  });
});
