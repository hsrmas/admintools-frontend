import { AbstractControl } from '@angular/forms';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ApplicationService } from '../services/adminTools/application.service';

/**
 * Custom Validator
 *
 * Checks if a given application/proposal name is already in use/exists.
 */
@Injectable()
export class ApplicationNameValidator {

  constructor(private applicationService: ApplicationService) {
  }

  public validate(control: AbstractControl): Observable<boolean> {
    const inputValue = control.value;
    const applicationFound = this.applicationService.getApplicationFromStoreByName(inputValue);
    if (applicationFound) {
      return Observable.of(true);
    }
    return Observable.of(false);
  }
}
