import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { ProposalService } from '../services/adminTools/proposal.service';

/**
 * Custom Validator
 *
 * Checks if a given Proposal name is already in use/exists.
 */
@Injectable()
export class ProposalNameValidator {

  constructor(private proposalService: ProposalService) {
  }

  public validate(control: AbstractControl): Observable<boolean> {
    const inputValue = control.value;
    const proposalFound = this.proposalService.getProposalFromStoreByName(inputValue);
    if (proposalFound) {
      return Observable.of(true);
    }
    return Observable.of(false);
  }
}
