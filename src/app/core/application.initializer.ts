import { LanguageService } from './services/language/language.service';
import { UniversityService } from './services/adminTools/university.service';
import { InstituteService } from './services/adminTools/institute.service';
import { ProposalTypeService } from './services/adminTools/proposal-type.service';
import { ProposalService } from './services/adminTools/proposal.service';
import { ApplicationService } from './services/adminTools/application.service';
import { University } from './domain/university';
import { Institute } from './domain/institute';
import { ProposalType } from './domain/proposal-type';
import { Proposal } from './domain/proposal';
import { Application } from './domain/application';
import { Observable } from 'rxjs/Observable';
import { UserService } from './services/adminTools/user.service';
import { User } from './domain/user';

/**
 * APP Initializer to perform pre-start initializations
 *
 * @param {LanguageService} languageService
 * @returns {() => Observable<any>}
 */
export function adminToolsFrontendInitializer(universityService: UniversityService,
                                              insitituteService: InstituteService,
                                              proposalTypeService: ProposalTypeService,
                                              proposalService: ProposalService,
                                              applicationService: ApplicationService,
                                              userService: UserService,
                                              languageService: LanguageService): () => {} |
  Promise<[University[], Institute[], ProposalType[], Proposal[], Application[], User[]]> {

  return () => languageService.init()
    .switchMap(() => Observable.forkJoin(
      universityService.init(),
      insitituteService.init(),
      proposalTypeService.init(),
      proposalService.init(),
      applicationService.init(),
      userService.init(),
    ))
    .catch(() => null)
    .toPromise();
}
