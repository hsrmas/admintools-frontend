import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import {
  HttpClient,
  HttpHeaders,
  HttpParams
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { EnvironmentService } from '../environment/environment.service';

@Injectable()
export class HttpServiceImpl extends HttpService {

  constructor(public httpClient: HttpClient,
              public environmentService: EnvironmentService) {
    super();
  }

  get<T>(url: string, options?: {
    headers?: HttpHeaders | { [p: string]: string | string[] };
    observe?: string; params?: HttpParams | { [p: string]: string | string[] };
    reportProgress?: boolean; responseType?: string; withCredentials?: boolean
  }, useLoadingSpinner?: boolean, spinnerId?: string): Observable<T> {
    return this.httpClient.get<T>(this.buildRequestURI(url), {headers: this.buildHttpHeaders()});
  }

  post<T>(url: string, body: string, options?: {
    headers?: HttpHeaders | { [p: string]: string | string[] };
    observe?: string; params?: HttpParams | { [p: string]: string | string[] };
    reportProgress?: boolean;
    responseType?: string;
    withCredentials?: boolean
  }, useLoadingSpinner?: boolean, spinnerId?: string): Observable<T> {
    return this.httpClient.post<T>(this.buildRequestURI(url), body, {headers: this.buildHttpHeaders()});
  }

  put<T>(url: string, body: string, options?: {
    headers?: HttpHeaders | { [p: string]: string | string[] };
    observe?: string; params?: HttpParams | { [p: string]: string | string[] };
    reportProgress?: boolean; responseType?: string; withCredentials?: boolean
  }, useLoadingSpinner?: boolean, spinnerId?: string): Observable<T> {
    return undefined;
  }

  delete<T>(url: string, options?: {
    headers?: HttpHeaders | { [p: string]: string | string[] };
    observe?: string; params?: HttpParams | { [p: string]: string | string[] };
    reportProgress?: boolean; responseType?: string; withCredentials?: boolean
  }, useLoadingSpinner?: boolean, spinnerId?: string): Observable<T> {
    return undefined;
  }


  /**
   * Concat the target endpoint with the 'apiServerRoot'
   * from the {environment} settings
   *
   * @param {string} uri target endpoint
   * @returns {string} complete URI
   */
  private buildRequestURI(url: string): string {
    // if url's absolute path is given then just return that url
    if (url && url[0].indexOf('/') === -1) {
      return url;
    }
    return `${this.environmentService.environment.apiServerRoot}${url}`;
  }

  /**
   * Add Http-Headers to requests
   * - Content-Type
   *
   * @returns {HttpHeaders}
   */
  private buildHttpHeaders(): HttpHeaders {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return headers;
  }
}
