import {
  HttpHeaders,
  HttpParams
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

export abstract class HttpService {

  /**
   * Performs a request with `get` http method.
   * @param {string} url - A string containing the URL to which the request is sent.
   * @param {options} options - Options to construct a RequestOptions, based on
   *                                       [RequestInit](https://fetch.spec.whatwg.org/#requestinit) from the Fetch spec.
   * @param {boolean} useLoadingSpinner - True if loading spinner is shown during http request
   * @param {string} spinnerId - The id of the loading spinner html tag if there are more than one spinner in the application
   */
  abstract get<T>(url: string, options?: {
    headers?: HttpHeaders | { [header: string]: string | string[]; };
    observe?: string; params?: HttpParams | { [param: string]: string | string[]; };
    reportProgress?: boolean; responseType?: string;
    withCredentials?: boolean;
  }, useLoadingSpinner?: boolean, spinnerId?: string): Observable<T>;

  /**
   * Performs a request with `post` http method.
   * @param {string} url - A string containing the URL to which the request is sent.
   * @param {string} body - The actual HTTP request data
   * @param {options} options - Options to construct a RequestOptions, based on
   *                                       [RequestInit](https://fetch.spec.whatwg.org/#requestinit) from the Fetch spec.
   * @param {boolean} useLoadingSpinner - True if loading spinner is shown during http request
   * @param {string} spinnerId - The id of the loading spinner html tag if there are more than one spinner in the application
   */
  abstract post<T>(url: string, body: string, options?: {
    headers?: HttpHeaders | { [header: string]: string | string[]; };
    observe?: string; params?: HttpParams | { [param: string]: string | string[]; };
    reportProgress?: boolean;
    responseType?: string;
    withCredentials?: boolean;
  }, useLoadingSpinner?: boolean, spinnerId?: string): Observable<T>;

  /**
   * Performs a request with `put` http method.
   * @param {string} url - A string containing the URL to which the request is sent.
   * @param {string} body - The actual HTTP request data
   * @param {options} options - Options to construct a RequestOptions, based on
   *                                       [RequestInit](https://fetch.spec.whatwg.org/#requestinit) from the Fetch spec.
   * @param {boolean} useLoadingSpinner - True if loading spinner is shown during http request
   * @param {string} spinnerId - The id of the loading spinner html tag if there are more than one spinner in the application
   */
  abstract put<T>(url: string, body: string, options?: {
    headers?: HttpHeaders | { [header: string]: string | string[]; };
    observe?: string; params?: HttpParams | { [param: string]: string | string[]; };
    reportProgress?: boolean; responseType?: string;
    withCredentials?: boolean;
  }, useLoadingSpinner?: boolean, spinnerId?: string): Observable<T>;

  /**
   * Performs a request with `delete` http method.
   * @param {string} url - A string containing the URL to which the request is sent.
   * @param {options} options - Options to construct a RequestOptions, based on
   *                                       [RequestInit](https://fetch.spec.whatwg.org/#requestinit) from the Fetch spec.
   * @param {boolean} useLoadingSpinner - True if loading spinner is shown during http request
   * @param {string} spinnerId - The id of the loading spinner html tag if there are more than one spinner in the application
   */
  abstract delete<T>(url: string, options?: {
    headers?: HttpHeaders | { [header: string]: string | string[]; };
    observe?: string;
    params?: HttpParams | { [param: string]: string | string[]; };
    reportProgress?: boolean;
    responseType?: string;
    withCredentials?: boolean;
  }, useLoadingSpinner?: boolean, spinnerId?: string): Observable<T>;
}
