import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class EventService {

  private _eventSource = new BehaviorSubject<boolean>(false);
  eventItems = this._eventSource.asObservable();

  proposalEvent(value) {
    this._eventSource.next(value);
  }

  getEventItems(): Observable<any> {
    return this.eventItems;
  }
}
