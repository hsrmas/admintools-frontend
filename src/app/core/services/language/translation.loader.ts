import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

/**
 * Returns a new Http Translationloader for @ngx-translate service
 * with configured translation resources in assets directory.
 *
 * @param {HttpClient} httpClient
 * @returns {TranslateHttpLoader}
 */
export function createTranslationLoader(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient, './assets/i18n/admintools-frontend_', '.json');
}
