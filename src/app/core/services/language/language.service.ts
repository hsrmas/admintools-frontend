import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { CONSTANTS } from '../../constants';
import { Observable } from 'rxjs/Observable';

/**
 * Language Service
 */
@Injectable()
export class LanguageService {

  supportedLanguages: string[];

  constructor(private translationService: TranslateService) {
    this.supportedLanguages = [CONSTANTS.LANGUAGE_ENGLISH, CONSTANTS.LANGUAGE_GERMAN];
  }

  /**
   * Initialize the language service with defaults
   * and set available languages
   *
   * @returns {Observable<any>}
   */
  init(): Observable<any> {
    this.translationService.addLangs(this.supportedLanguages);
    this.translationService.setDefaultLang(CONSTANTS.LANGUAGE_GERMAN);
    return this.translationService.use(CONSTANTS.LANGUAGE_GERMAN);
  }

  /**
   * Return the default language
   * @returns {string}
   */
  getDefaultLanguage(): string {
    return this.translationService.getDefaultLang();
  }

  /**
   * Switch to language
   * @param {string} lang
   */
  switchLanguage(lang: string) {
    this.translationService.use(lang);
  }
}
