/*
xdescribe('language.service', () => {

  let languageService: LanguageService;
  let store: StoreMock;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        LanguageService,
        {provide: Store, useClass: StoreMock},
        provideMockActions(() => languageActions)
      ]
    });

    store = TestBed.get(Store);
    languageService = TestBed.get(LanguageService);
    languageActions = new ReplaySubject(1);
  });

  it('should send an action to load languages & wait for their resolution^^', () => {
    // prepare
    spyOn(store, 'dispatch');
    languageActions.next(new LanguageActions.TranslateServiceUsed({
      acceptLanguageHeader: 'de,fr;q=0.9,it;q=0.8,en-US;q=0.7,en;q=0.6',
      browserLanguage: 'de',
      cislLocale: 'de_CH',
      supportedLocales: ['de_CH', 'it_CH', 'fr_CH']
    }));

    // trigger & verify
    languageService.init()
      .subscribe((serviceUsed: null) => {
        expect(store.dispatch).toHaveBeenCalledWith(new LanguageActions.LoadLanguage());
        expect(serviceUsed).toBe(null);
      });
  });
});
**/
