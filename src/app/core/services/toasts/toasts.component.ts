import {
  Component,
  Inject
} from '@angular/core';
import { MAT_SNACK_BAR_DATA } from '@angular/material';
import { ToastData } from './toast-data';

@Component({
  selector: 'app-toasts',
  templateUrl: './toasts.component.html',
  styleUrls: ['./toasts.component.scss']
})
export class ToastsComponent {
  constructor(@Inject(MAT_SNACK_BAR_DATA) public data: ToastData) {
  }

  closeToast(event: any) {
  }
}
