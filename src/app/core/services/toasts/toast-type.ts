export enum ToastType {
  info = 'info',
  success = 'success',
  warning = 'warning',
  error = 'error'
}
