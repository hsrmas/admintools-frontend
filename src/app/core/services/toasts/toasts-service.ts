import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { ToastsComponent } from './toasts.component';
import { ToastData } from './toast-data';
import { ToastType } from './toast-type';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class ToastsService {

  constructor(private matSnackBar: MatSnackBar,
              private translateService: TranslateService) {
  }

  /**
   * Helper function to create a new toast according to ToastData submitted
   * @param {ToastData} toastData
   */
  makeToast(toastData: ToastData): void {
    let extraClassesConfig;
    switch (toastData.type) {
      case ToastType.success: {
        extraClassesConfig = ['toast-background-green'];
        break;
      }
      case ToastType.error: {
        extraClassesConfig = ['toast-background-red'];
        break;
      }
      case ToastType.info: {
        extraClassesConfig = ['toast-background-blue'];
        break;
      }
      case ToastType.warning: {
        extraClassesConfig = ['toast-background-orange'];
        break;
      }
    }
    this.matSnackBar.openFromComponent(ToastsComponent, {
      extraClasses: extraClassesConfig,
      data: toastData,
      duration: 5000,
      horizontalPosition: 'right',
      verticalPosition: 'top',
    });
  }

  /**
   * Create a std. error toast
   */
  genericErrorToast() {
    const toastData = new ToastData(this.translateService.instant('TOAST_MESSAGES.ERROR.GENERIC.TITLE'),
      this.translateService.instant('TOAST_MESSAGES.ERROR.GENERIC.MESSAGE'), ToastType.error);

    this.makeToast(toastData);
  }

}
