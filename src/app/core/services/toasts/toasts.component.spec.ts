import {
  async,
  ComponentFixture,
  TestBed
} from '@angular/core/testing';

import { ToastsComponent } from './toasts.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import {
  FormsModule,
  ReactiveFormsModule
} from '@angular/forms';
import { EnvironmentService } from '../environment/environment.service';
import { HttpService } from '../http/http.service';
import { MaterialModule } from '../../../catalogue/material.module';
import {
  HttpClient,
  HttpHandler
} from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { LanguageService } from '../language/language.service';
import { MAT_SNACK_BAR_DATA } from '@angular/material';

describe('ToastsComponent', () => {
  let component: ToastsComponent;
  let fixture: ComponentFixture<ToastsComponent>;

  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        declarations: [ToastsComponent],
        imports: [
          RouterModule,
          MaterialModule,
          BrowserAnimationsModule,
          FormsModule,
          ReactiveFormsModule,
          RouterTestingModule,

        ],
        providers: [
          HttpService, EnvironmentService, HttpClient, HttpHandler, LanguageService,
          {provide: MAT_SNACK_BAR_DATA, useValue: {}}
        ],
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
      })
      .compileComponents();

  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToastsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should createNewProposal', () => {
    expect(component).toBeTruthy();
  });
});
