import { NgModule } from '@angular/core';
import { ToastsComponent } from './toasts.component';

@NgModule({
  imports: [],
  exports: [
    ToastsComponent,

  ],
  declarations: [
    ToastsComponent,
  ]
})
export class ToastsModule {
}
