import { ToastType } from './toast-type';

export class ToastData {

  constructor(title: string, message: string, type: ToastType) {
    this.title = title;
    this.message = message;
    this.type = type;
  }

  title: string;
  message: string;
  type: ToastType;
}
