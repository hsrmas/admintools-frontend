import { Injectable } from '@angular/core';
import { HttpService } from '../http/http.service';
import { EnvironmentService } from '../environment/environment.service';
import { AppState } from '../../statemanagement/app.state';
import { Store } from '@ngrx/store';
import { ProposalTypeActions } from '../../statemanagement/actions/index';
import { ProposalType } from '../../domain/proposal-type';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ProposalTypeService {


  constructor(private http: HttpService,
              private env: EnvironmentService,
              private store: Store<AppState>) {
  }

  /**
   * Initialization of ProposalType.  Should only be used by the {@link APP_INITIALIZER} !
   * @returns {Observable<ProposalType[]>}
   */
  init(): Observable<ProposalType[]> {
    return this.http.get<ProposalType[]>(this.env.environment.apiServerRoot + '/proposaltypes/')
      .do((response: ProposalType[]) => this.store.dispatch(new ProposalTypeActions.LoadProposalTypes(response)));
  }

  /**
   * Get all proposal-types from store or backend
   * @returns {Observable<ProposalType[]>}
   */
  getAllProposalTypes(): Observable<ProposalType[]> {
    const proposalTypes = this.getAllProposalTypesFromStore();
    if (proposalTypes.length > 0) {
      return Observable.of(proposalTypes);
    }
    // load from backend
    return this.http.get<ProposalType[]>(this.env.environment.apiServerRoot + '/proposaltypes/')
      .do((response: ProposalType[]) => this.dispatchProposalTypeHttpCallToStore(response));
  }

  /**
   * Get all the ProposalTypes in Store
   * @returns {ProposalType[]}
   */
  getAllProposalTypesFromStore(): ProposalType[] {
    let proposalTypesFromStore: ProposalType[] = [];
    this.store
      .take(1)
      .map((appState: AppState) => appState.proposalTypes)
      .subscribe((proposalTypes: ProposalType[]) => proposalTypesFromStore = proposalTypes);
    proposalTypesFromStore.map((proposalType: ProposalType) => Object.assign(Object.create(ProposalType.prototype), proposalType));
    return proposalTypesFromStore;
  }

  /**
   * Dispatch the ProposalTypes to the store after Http-Call.
   * @param {ProposalType[]} proposalTypes
   */
  private dispatchProposalTypeHttpCallToStore(proposalTypes: ProposalType[]): void {
    proposalTypes.forEach((proposalType: ProposalType) => this.store.dispatch(
      new ProposalTypeActions.CreateOrUpdateProposalType(proposalType)));
  }
}
