import { Injectable } from '@angular/core';
import { HttpService } from '../http/http.service';
import { EnvironmentService } from '../environment/environment.service';
import { AppState } from '../../statemanagement/app.state';
import { Store } from '@ngrx/store';
import { TechnologyActions } from '../../statemanagement/actions/index';
import { Technology } from '../../domain/technology';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class TechnologyService {


  constructor(private http: HttpService,
              private env: EnvironmentService,
              private store: Store<AppState>) {
  }

  /**
   * Initialization of Technology.  Should only be used by the {@link APP_INITIALIZER} !
   * @returns {Observable<Technology[]>}
   */
  init(): Observable<Technology[]> {
    return this.http.get<Technology[]>(this.env.environment.apiServerRoot + '/technologies/')
      .do((response: Technology[]) => this.store.dispatch(new TechnologyActions.LoadTechnologies(response)));
  }

  /**
   * Get all technologies from store or backend
   * @returns {Observable<Technology[]>}
   */
  getAllTechnologies(): Observable<Technology[]> {
    const technologies = this.getAllTechnologiesFromStore();
    if (technologies.length > 0) {
      return Observable.of(technologies);
    }
    // load from backend
    return this.http.get<Technology[]>(this.env.environment.apiServerRoot + '/technologies/')
      .do((response: Technology[]) => this.dispatchTechnologyHttpCallToStore(response));
  }


  /**
   * Get all the Technologies in Store
   * @returns {DomainReference[]}
   */
  getAllTechnologiesFromStore(): Technology[] {
    let technologiesFromStore: Technology[] = [];
    this.store
      .take(1)
      .map((appState: AppState) => appState.technologies)
      .subscribe((technologies: Technology[]) => technologiesFromStore = technologies);
    technologiesFromStore.map((technology: Technology) => Object.assign(Object.create(Technology.prototype), technology));
    return technologiesFromStore;
  }

  /**
   * Dispatch the Technology[] to store
   * @param {Technology[]} technologies
   */
  private dispatchTechnologyHttpCallToStore(technologies: Technology[]): void {
    technologies.forEach((technology: Technology) => this.store.dispatch(new TechnologyActions.CreateOrUpdateTechnology(technology)));
  }
}
