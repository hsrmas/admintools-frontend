import { InstituteService } from './institute.service';
import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { HttpClientModule } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { Institute } from '../../domain/institute';
import { HttpServiceImpl } from '../http/http.service.impl';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HttpService } from '../http/http.service';
import { EnvironmentService } from '../environment/environment.service';
import { EnvironmentServiceImpl } from '../environment/environment-impl.service';
import { Observable } from 'rxjs/Observable';
import { InstituteMockData } from '../../../unittest/mock-data/institute-mock-data';

describe('institute service', () => {

  let instituteService: InstituteService;
  let httpTestingController: HttpTestingController;
  let store: Store<{ institutes: Institute[] }>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        HttpClientTestingModule],
      providers: [
        InstituteService,
        {provide: Store, useClass: MockStore},
        {provide: EnvironmentService, useClass: EnvironmentServiceImpl},
        {provide: HttpService, useClass: HttpServiceImpl},
        {provide: InstituteService, useClass: InstituteService},
      ]
    });
    instituteService = TestBed.get(InstituteService);
    httpTestingController = TestBed.get(HttpTestingController);
    store = TestBed.get(Store);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should return all the institutes from the store', () => {
    const institute: Institute[] = instituteService.getAllInstitutesFromStore();
    expect(institute).toEqual(InstituteMockData.getMockInstitutes());
  });

  it('should return all the institutes from the store', () => {
    let result: Institute[];
    instituteService.getAllInstitutes()
      .subscribe((institutes: Institute[]) => {
        result = institutes;
      });
    expect(result).toEqual(InstituteMockData.getMockInstitutes());
  });


});

describe('institue with empty store', () => {

  let instituteService: InstituteService;
  let httpTestingController: HttpTestingController;
  let store: Store<{ institutes: Institute[] }>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        HttpClientTestingModule],
      providers: [
        InstituteService,
        {provide: Store, useClass: MockStoreEmpty},
        {provide: EnvironmentService, useClass: EnvironmentServiceImpl},
        {provide: HttpService, useClass: MockHttpClientService},
        {provide: InstituteService, useClass: InstituteService},
      ]
    });
    instituteService = TestBed.get(InstituteService);
    httpTestingController = TestBed.get(HttpTestingController);
    store = TestBed.get(Store);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should intialize the store from the backend', () => {
    let result: Institute[];
    instituteService.init()
      .subscribe((institutes: Institute[]) => {
        result = institutes;
      });
    expect(result).toEqual(InstituteMockData.getMockInstitutes());
  });

  it('should return all the institutes from the backend', () => {
    let result: Institute[];
    instituteService.getAllInstitutes()
      .subscribe((institutes: Institute[]) => {
        result = institutes;
      });
    expect(result).toEqual(InstituteMockData.getMockInstitutes());
  });
});

class MockStore extends BehaviorSubject<{ institutes: Institute[] }> {
  constructor() {
    super({institutes: InstituteMockData.getMockInstitutes()});
  }
}

class MockStoreEmpty extends BehaviorSubject<{ institutes: Institute[] }> {
  constructor() {
    super({institutes: InstituteMockData.getMockInstitutesEmpty()});
  }

  dispatch() {
  }
}

class MockHttpClientService {

  get(): Observable<any> {
    return Observable.of(InstituteMockData.getMockInstitutes());
  }
}



