import { HttpService } from '../http/http.service';
import { EnvironmentService } from '../environment/environment.service';
import { Observable } from 'rxjs/Observable';
import { ProposalActions } from '../../statemanagement/actions/index';
import { AppState } from '../../statemanagement/app.state';
import { Store } from '@ngrx/store';
import { Injectable } from '@angular/core';
import { Proposal } from '../../domain/proposal';
import { ProposalCreate } from '../../domain/proposal-create';
import { RejectReason } from '../../domain/reject-reason';

@Injectable()
export class ProposalService {


  constructor(private http: HttpService,
              private env: EnvironmentService,
              private store: Store<AppState>) {
  }

  /**
   * Initialization of Proposals.  Should only be used by the {@link APP_INITIALIZER} !
   * @returns {Observable<Proposal[]>}
   */
  init(): Observable<Proposal[]> {
    return this.http.get<Proposal[]>(this.env.environment.apiServerRoot + '/proposals/')
      .do((proposals: Proposal[]) => this.store.dispatch(new ProposalActions.LoadProposals(proposals)));
  }


  /**
   * Returns all Proposals which require approval corresponding to the logged-in user in the admintools-backend
   * @returns {Observable<Proposal[]>}
   */
  getAllMyTodos(): Observable<Proposal[]> {
    return this.http.get<Proposal[]>(this.env.environment.apiServerRoot + '/mytodos/')
      .do((proposals: Proposal[]) => this.dispatchProposalsHttpCallToStore(proposals));
  }

  /**
   * Returns all Proposals which are linked to the corresponding logged-in user in the admintools-backend
   * @returns {Observable<Proposal[]>}
   */
  getAllMyProposals(): Observable<Proposal[]> {
    return this.http.get<Proposal[]>(this.env.environment.apiServerRoot + '/myproposals/')
      .do((proposals: Proposal[]) => this.dispatchProposalsHttpCallToStore(proposals));
  }

  /**
   * Returns all Proposals from store. If none are available it loads them from the backend.
   * @returns {Observable<Proposal[]>}
   */
  getAllProposals(): Observable<Proposal[]> {
    const proposals = this.getAllProposalsFromStore();
    if (proposals.length > 0) {
      return Observable.of(proposals);
    }
    return this.http.get<Proposal[]>(this.env.environment.apiServerRoot + '/proposals/')
      .do((response: Proposal[]) => this.dispatchProposalsHttpCallToStore(response));
  }

  /**
   * Create a new Proposal and store the result in the store
   * @param {ProposalCreate} proposal
   * @returns {Observable<Proposal>}
   */
  createNewProposal(proposal: ProposalCreate): Observable<Proposal> {
    return this.http.post<Proposal>(this.env.environment.apiServerRoot + '/proposals/',
      JSON.stringify(proposal), {}, false, '')
      .do((response: Proposal) => this.dispatchProposalToStore(response));
  }


  /**
   * Approve a Proposal by ProposalID.
   * @param {number} proposalId
   * @returns {Observable<Proposal>}
   */
  approveProposal(proposalId: number): Observable<Proposal> {
    return this.http.get<Proposal>(this.env.environment.apiServerRoot + '/proposals/' + proposalId + '/approve/')
      .do((response: Proposal) => this.store.dispatch(new ProposalActions.UpdateProposal(response)));
  }

  /**
   * Reject a Proposal by ProposalID with reject reason.
   * @param {RejectReason} rejectReason
   * @param {number} proposalId
   * @returns {Observable<Proposal>}
   */
  rejectProposal(rejectReason: RejectReason, proposalId: number): Observable<Proposal> {
    return this.http.post<Proposal>(this.env.environment.apiServerRoot + '/proposals/' + proposalId + '/reject/',
      JSON.stringify(rejectReason), {}, false, '')
      .do((response: Proposal) => this.dispatchProposalToStore(response));
  }

  /**
   * Load all Proposals from store
   * @returns {Proposal[]}
   */
  public getAllProposalsFromStore(): Proposal[] {
    let proposalsFromStore: Proposal[] = [];
    this.store
      .take(1)
      .map((appState: AppState) => appState.proposals)
      .subscribe((proposals: Proposal[]) => proposalsFromStore = proposals);
    proposalsFromStore.map((proposal: Proposal) => Object.assign(Object.create(Proposal.prototype), proposal));
    return proposalsFromStore;
  }

  /**
   * Get Proposal from store by id
   * @param {number} proposalId
   * @returns {Proposal}
   */
  public getProposalFromStoreById(proposalId: number): Proposal {
    let proposalFromStore = null;
    this.store
      .take(1)
      .map((appState: AppState) => appState.proposals)
      .map(((proposals: Proposal[]) => proposals.find((proposal: Proposal) => proposal.id === proposalId)))
      .filter((proposal: Proposal) => !!proposal)
      .subscribe((proposal: Proposal) => proposalFromStore = Object.assign(Object.create(Proposal.prototype), proposal));
    return proposalFromStore;
  }

  /**
   * Get proposal from store by name
   * @param {string} proposalName
   * @returns {Proposal}
   */
  public getProposalFromStoreByName(proposalName: string): Proposal {
    let proposalFromStore = null;
    this.store
      .take(1)
      .map((appState: AppState) => appState.proposals)
      .map(((proposals: Proposal[]) => proposals.find((proposal: Proposal) => proposal.name === proposalName)))
      .filter((proposal: Proposal) => !!proposal)
      .subscribe((proposal: Proposal) => proposalFromStore = Object.assign(Object.create(Proposal.prototype), proposal));
    return proposalFromStore;
  }

  /**
   * Get proposal from store by applicationId
   * @param {number} applicationId
   * @returns {Proposal}
   */
  public getProposalFromStoreByApplicationId(applicationId: number): Proposal {
    let proposalFromStore = null;
    this.store
      .take(1)
      .map((appState: AppState) => appState.proposals)
      .map(((proposals: Proposal[]) => proposals.find((proposal: Proposal) => proposal.application_id === applicationId)))
      .filter((proposal: Proposal) => !!proposal)
      .subscribe((proposal: Proposal) => proposalFromStore = Object.assign(Object.create(Proposal.prototype), proposal));
    return proposalFromStore;
  }


  private dispatchProposalsHttpCallToStore(newProposals: Proposal[]): void {
    newProposals.forEach((proposal: Proposal) => this.dispatchProposalToStore(proposal));
  }

  private dispatchProposalToStore(newProposal: Proposal): void {
    this.store.dispatch(new ProposalActions.CreateOrUpdateProposal(newProposal));
  }
}
