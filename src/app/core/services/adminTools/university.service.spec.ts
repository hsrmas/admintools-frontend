import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { HttpClientModule } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { HttpServiceImpl } from '../http/http.service.impl';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HttpService } from '../http/http.service';
import { EnvironmentService } from '../environment/environment.service';
import { EnvironmentServiceImpl } from '../environment/environment-impl.service';
import { Observable } from 'rxjs/Observable';
import { UniversityService } from './university.service';
import { University } from '../../domain/university';
import { UniversityMockData } from '../../../unittest/mock-data/university-mock-data';

describe('university service with store', () => {

  let universityService: UniversityService;
  let httpTestingController: HttpTestingController;
  let store: Store<{ universities: University[] }>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        HttpClientTestingModule],
      providers: [
        UniversityService,
        {provide: Store, useClass: MockStore},
        {provide: EnvironmentService, useClass: EnvironmentServiceImpl},
        {provide: HttpService, useClass: HttpServiceImpl},
        {provide: UniversityService, useClass: UniversityService},
      ]
    });
    universityService = TestBed.get(UniversityService);
    httpTestingController = TestBed.get(HttpTestingController);
    store = TestBed.get(Store);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should return all the universities from the store', () => {
    const universities: University[] = universityService.getAllUniversitiesFromStore();
    expect(universities).toEqual(UniversityMockData.getMockUniversities());
  });

  it('should return all the universities from the store as observables', () => {
    let result: University[];
    universityService.getAllUniversities()
      .subscribe((universities: University[]) => {
        result = universities;
      });
    expect(result).toEqual(UniversityMockData.getMockUniversities());
  });
});

describe('university service with backend call', () => {

  let universityService: UniversityService;
  let httpTestingController: HttpTestingController;
  let store: Store<{ universities: University[] }>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        HttpClientTestingModule],
      providers: [
        UniversityService,
        {provide: Store, useClass: MockStoreEmpty},
        {provide: EnvironmentService, useClass: EnvironmentServiceImpl},
        {provide: HttpService, useClass: MockHttpClientService},
        {provide: UniversityService, useClass: UniversityService},
      ]
    });
    universityService = TestBed.get(UniversityService);
    httpTestingController = TestBed.get(HttpTestingController);
    store = TestBed.get(Store);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should initialize the store from the backend', () => {
    let result: University[];
    universityService.init()
      .subscribe((universities: University[]) => {
        result = universities;
      });
    expect(result).toEqual(UniversityMockData.getMockUniversities());
  });

  it('should return all the technologies from the backend', () => {
    let result: University[];
    universityService.getAllUniversities()
      .subscribe((universities: University[]) => {
        result = universities;
      });
    expect(result).toEqual(UniversityMockData.getMockUniversities());
  });
});

class MockStore extends BehaviorSubject<{ universities: University[] }> {
  constructor() {
    super({universities: UniversityMockData.getMockUniversities()});
  }
}

class MockStoreEmpty extends BehaviorSubject<{ universities: University[] }> {
  constructor() {
    super({universities: UniversityMockData.getMockUnivseritiesEmpty()});
  }
  dispatch() {
  }
}

class MockHttpClientService {
  get(): Observable<any> {
    return Observable.of(UniversityMockData.getMockUniversities());
  }
}

