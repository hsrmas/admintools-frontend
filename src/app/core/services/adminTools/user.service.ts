import { Injectable } from '@angular/core';
import { HttpService } from '../http/http.service';
import { Observable } from 'rxjs/Observable';
import { EnvironmentService } from '../environment/environment.service';
import { AppState } from '../../statemanagement/app.state';
import { Store } from '@ngrx/store';
import { UserActions } from '../../statemanagement/actions';
import { User } from '../../domain/user';
import { CONSTANTS } from '../../constants';

@Injectable()
export class UserService {

  constructor(private http: HttpService,
              private env: EnvironmentService,
              private store: Store<AppState>) {
  }


  /**
   * Initialization of User.  Should only be used by the {@link APP_INITIALIZER} !
   * @returns {Observable<User[]>}
   */
  init(): Observable<User[]> {
    return this.http.get<User[]>(this.env.environment.apiServerRoot + '/myuserdata/')
      .do((response: User[]) => this.store.dispatch(new UserActions.LoadUsers(response)));
  }

  /**
   * Determine if a given user has permissions to approve a request
   * @param {string} userName
   */
  isApprover(): boolean {
    return CONSTANTS.APPROVERS.indexOf(this.getUser().username) > -1 ? true : false;
  }

  /**
   * Determine if the user has an admin status
   * @returns {boolean}
   */
  isAdmin(): boolean {
    return this.getUser().is_superuser;
  }

  /**
   * We can only ever return one user
   * @returns {User}
   */
  getUser(): User {
    return this.getAllUsersFromStore()[0];
  }

  /**
   * Get all users from store or backend
   * @returns {Observable<User[]>}
   */
  getAllUsers(): Observable<User[]> {
    const users = this.getAllUsersFromStore();
    if (users.length > 0) {
      return Observable.of(users);
    }
    // do http call
    return this.http.get<User[]>(this.env.environment.apiServerRoot + '/myuserdata/')
      .do((response: User[]) => this.dispatchUnserHttpCallToStore(response));
  }

  /**
   * Get all users from store
   * @returns {User[]}
   */
  getAllUsersFromStore(): User[] {
    let usersFromStore: User[];
    // let userRetVal: User;
    this.store
      .take(1)
      .map((appState: AppState) => appState.users)
      .subscribe((users: User[]) => usersFromStore = users);
    usersFromStore.map((user: User) => Object.assign(Object.create(User.prototype), user));
    return usersFromStore;
  }

  /**
   * Dispatch the User[] to the store after Http-Call.
   * @param {User[]} users
   */
  private dispatchUnserHttpCallToStore(users: User[]): void {
    users.forEach((user: User) => this.store.dispatch(new UserActions.CreateOrUpdateUser(user)));
  }
}
