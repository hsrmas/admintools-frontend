import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { HttpClientModule } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { HttpServiceImpl } from '../http/http.service.impl';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HttpService } from '../http/http.service';
import { EnvironmentService } from '../environment/environment.service';
import { EnvironmentServiceImpl } from '../environment/environment-impl.service';
import { Observable } from 'rxjs/Observable';
import { Application } from '../../domain/application';
import { ApplicationService } from './application.service';
import { ApplicationMockData } from '../../../unittest/mock-data/application-mock-data';

describe('application service', () => {

  let applicationService: ApplicationService;
  let httpTestingController: HttpTestingController;
  let store: Store<{ applications: Application[] }>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        HttpClientTestingModule
      ],
      providers: [
        ApplicationService,
        {provide: Store, useClass: MockStore},
        {provide: EnvironmentService, useClass: EnvironmentServiceImpl},
        {provide: HttpService, useClass: HttpServiceImpl},
        {provide: ApplicationService, useClass: ApplicationService},
      ]
    });
    applicationService = TestBed.get(ApplicationService);
    httpTestingController = TestBed.get(HttpTestingController);
    store = TestBed.get(Store);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should return all the applications from the store', () => {
    const application: Application[] = applicationService.getAllApplicationsFromStore();
    expect(application).toEqual(ApplicationMockData.getMockApplications());
  });

  it('should return all the applications from the store', () => {
    let result: Application[];
    applicationService.getAllApplications()
      .subscribe((applications: Application[]) => {
        result = applications;
      });
    expect(result).toEqual(ApplicationMockData.getMockApplications());
  });

  it('should return application1 from the store', () => {
    const application1 = applicationService.getApplicationFromStoreByName('name1');
    expect(application1).toEqual(ApplicationMockData.getMockApplications()[0]);
  });

  it('should return all the applications from the store', () => {
    let result: Application[];
    applicationService.getAllApplications()
      .subscribe((applications: Application[]) => {
        result = applications;
      });
    expect(result).toEqual(ApplicationMockData.getMockApplications());
  });


});

describe('application service with empty store', () => {

  let applicationService: ApplicationService;
  let httpTestingController: HttpTestingController;
  let store: Store<{ applications: Application[] }>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        HttpClientTestingModule],
      providers: [
        ApplicationService,
        {provide: Store, useClass: MockStoreEmpty},
        {provide: EnvironmentService, useClass: EnvironmentServiceImpl},
        {provide: HttpService, useClass: MockHttpClientService},
        {provide: ApplicationService, useClass: ApplicationService},
      ]
    });
    applicationService = TestBed.get(ApplicationService);
    httpTestingController = TestBed.get(HttpTestingController);
    store = TestBed.get(Store);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should intialize the store from the backend', () => {
    let result;
    applicationService.init()
      .subscribe((applications: Application[]) => {
        result = applications;
      });
    expect(result).toEqual(ApplicationMockData.getMockApplications());
  });

  it('should return all the applications from the backend', () => {
    let result: Application[];
    applicationService.getAllApplications()
      .subscribe((applications: Application[]) => {
        result = applications;
      });
    expect(result).toEqual(ApplicationMockData.getMockApplications());
  });
});

class MockStore extends BehaviorSubject<{ applications: Application[] }> {
  constructor() {
    super({applications: ApplicationMockData.getMockApplications()});
  }
}

class MockStoreEmpty extends BehaviorSubject<{ applications: Application[] }> {
  constructor() {
    super({applications: ApplicationMockData.getMockApplicationsEmpty()});
  }
  dispatch() {
  }
}

class MockHttpClientService {
  get(): Observable<any> {
    return Observable.of(ApplicationMockData.getMockApplications());
  }
}
