import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { HttpClientModule } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { HttpServiceImpl } from '../http/http.service.impl';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HttpService } from '../http/http.service';
import { EnvironmentService } from '../environment/environment.service';
import { EnvironmentServiceImpl } from '../environment/environment-impl.service';
import { Observable } from 'rxjs/Observable';
import { ProposalService } from './proposal.service';
import { Proposal } from '../../domain/proposal';
import { ToastsService } from '../toasts/toasts-service';
import { TranslateService } from '@ngx-translate/core';
import { ProposalMockData } from '../../../unittest/mock-data/proposal-mock-data';

describe('proposal service with store', () => {

  let proposalService: ProposalService;
  let httpTestingController: HttpTestingController;
  let store: Store<{ proposals: Proposal[] }>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        HttpClientTestingModule],
      providers: [
        ProposalService,
        {provide: Store, useClass: MockStore},
        {provide: EnvironmentService, useClass: EnvironmentServiceImpl},
        {provide: HttpService, useClass: HttpServiceImpl},
        {provide: ToastsService, useClass: ToastsService},
        {provide: ProposalService, useClass: ProposalService},
        {provide: TranslateService, useClass: TranslateServiceMock}
      ]
    });
    proposalService = TestBed.get(ProposalService);
    httpTestingController = TestBed.get(HttpTestingController);
    store = TestBed.get(Store);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should return all the proposals from the store', () => {
    const proposal: Proposal[] = proposalService.getAllProposalsFromStore();
    expect(proposal).toEqual(ProposalMockData.getMockProposals());
  });

  it('should return all the proposals from the store', () => {
    let result: Proposal[];
    proposalService.getAllProposals()
      .subscribe((proposals: Proposal[]) => {
        result = proposals;
      });
    expect(result).toEqual(ProposalMockData.getMockProposals());
  });

  it('should return proposal1 from the store with name', () => {
    const proposal1 = proposalService.getProposalFromStoreByName('proposal1');
    expect(proposal1).toEqual(ProposalMockData.getMockProposals()[0]);
  });

  it('should return proposal1 from the store with id', () => {
    const proposal1 = proposalService.getProposalFromStoreById(1);
    expect(proposal1).toEqual(ProposalMockData.getMockProposals()[0]);
  });

  it('should return all the proposals from the store', () => {
    let result: Proposal[];
    proposalService.getAllProposals()
      .subscribe((proposals: Proposal[]) => {
        result = proposals;
      });
    expect(result).toEqual(ProposalMockData.getMockProposals());
  });

  it('should return proposal1 from the store by application_id', () => {
    const proposal1 = proposalService.getProposalFromStoreByApplicationId(1);
    expect(proposal1).toEqual(ProposalMockData.getMockProposals()[0]);
  });


});

describe('proposal service with empty store (backend)', () => {

  let proposalService: ProposalService;
  let httpTestingController: HttpTestingController;
  let store: Store<{ proposals: Proposal[] }>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        HttpClientTestingModule],
      providers: [
        ProposalService,
        {provide: Store, useClass: MockStoreEmpty},
        {provide: EnvironmentService, useClass: EnvironmentServiceImpl},
        {provide: HttpService, useClass: MockHttpClientService},
        {provide: ProposalService, useClass: ProposalService},
      ]
    });
    proposalService = TestBed.get(ProposalService);
    httpTestingController = TestBed.get(HttpTestingController);
    store = TestBed.get(Store);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should initialize the store from the backend', () => {
    let result: Proposal[];
    proposalService.init()
      .subscribe((proposals: Proposal[]) => {
        result = proposals;
      });
    expect(result).toEqual(ProposalMockData.getMockProposals());
  });

  it('should return all the proposals from the backend', () => {
    let result: Proposal[];
    proposalService.getAllProposals()
      .subscribe((proposals: Proposal[]) => {
        result = proposals;
      });
    expect(result).toEqual(ProposalMockData.getMockProposals());
  });

  it('should create (post) a new proposal', () => {
    let result: Proposal;
    proposalService.createNewProposal(ProposalMockData.newProposalCreate())
      .subscribe((proposal: Proposal) => {
        result = proposal;
      });
    expect(result).toEqual(ProposalMockData.newProposalResponse());
  });

});

class MockStore extends BehaviorSubject<{ proposals: Proposal[] }> {
  constructor() {
    super({proposals: ProposalMockData.getMockProposals()});
  }
}

class MockStoreEmpty extends BehaviorSubject<{ proposals: Proposal[] }> {
  constructor() {
    super({proposals: ProposalMockData.getMockProposalsEmpty()});
  }

  dispatch() {
  }
}

class MockHttpClientService {

  get(): Observable<any> {
    return Observable.of(ProposalMockData.getMockProposals()
    );
  }

  post(): Observable<any> {
    return Observable.of(ProposalMockData.newProposalResponse());
  }
}

class TranslateServiceMock {

}
