import { Injectable } from '@angular/core';
import { EnvironmentService } from '../environment/environment.service';
import { AppState } from '../../statemanagement/app.state';
import { Store } from '@ngrx/store';
import { HttpService } from '../http/http.service';
import { Observable } from 'rxjs/Observable';
import { Application } from '../../domain/application';
import { ApplicationActions } from '../../statemanagement/actions';
import { Proposal } from '../../domain/proposal';


@Injectable()
export class ApplicationService {

  constructor(private http: HttpService,
              private env: EnvironmentService,
              private store: Store<AppState>) {
  }

  /**
   * Initialization of Application.  Should only be used by the {@link APP_INITIALIZER} !
   * @returns {Observable<Application[]>}
   */
  init(): Observable<Application[]> {
    return this.http.get<Application[]>(this.env.environment.apiServerRoot + '/applications/')
      .do((response: Application[]) => this.store.dispatch(new ApplicationActions.LoadApplications(response)));
  }

  /**
   * Get all applications from either store or backend
   * @returns {Observable<Application[]>}
   */
  getAllApplications(): Observable<Application[]> {
    const applications = this.getAllApplicationsFromStore();
    if (applications.length > 0) {
      return Observable.of(applications);
    }
    return this.http.get<Application[]>(this.env.environment.apiServerRoot + '/applications/')
      .do((response: Application[]) => this.dispatchApplicationsHttpCallToStore(response));
  }

  /**
   * Load all Applications from store
   * @returns {Proposal[]}
   */
  getAllApplicationsFromStore(): Application[] {
    let applicationsFromStore: Application[] = [];
    this.store
      .take(1)
      .map((appState: AppState) => appState.applications)
      .subscribe((proposals: Application[]) => applicationsFromStore = proposals);
    applicationsFromStore.map((proposal: Application) => Object.assign(Object.create(Application.prototype), proposal));
    return applicationsFromStore;
  }

  /**
   * Get a specific application from the store by name
   * @param {string} applicationName
   * @returns {Application}
   */
  getApplicationFromStoreByName(applicationName: string): Application {
    let applicationFromStore = null;
    this.store
      .take(1)
      .map((appState: AppState) => appState.applications)
      .map(((applications: Application[]) => applications.find((application: Application) => application.name === applicationName)))
      .filter((application: Application) => !!application)
      .subscribe((application: Application) => applicationFromStore = Object.assign(Object.create(Application.prototype), application));
    return applicationFromStore;
  }

  /**
   * Dispatch the ProposalTypes to the store after Http-Call.
   * @param {ProposalType[]} proposalTypes
   */
  private dispatchApplicationsHttpCallToStore(applications: Application[]): void {
    applications.forEach((application: Application) => this.store.dispatch(
      new ApplicationActions.CreateOrUpdateApplication(application)));
  }
}
