import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { HttpClientModule } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { HttpServiceImpl } from '../http/http.service.impl';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HttpService } from '../http/http.service';
import { EnvironmentService } from '../environment/environment.service';
import { EnvironmentServiceImpl } from '../environment/environment-impl.service';
import { Observable } from 'rxjs/Observable';
import { TechnologyService } from './technology.service';
import { Technology } from '../../domain/technology';
import { TechnologyMockData } from '../../../unittest/mock-data/technology-mock-data';

describe('technology service with store', () => {

  let technologyService: TechnologyService;
  let httpTestingController: HttpTestingController;
  let store: Store<{ technologies: Technology[] }>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        HttpClientTestingModule],
      providers: [
        TechnologyService,
        {provide: Store, useClass: MockStore},
        {provide: EnvironmentService, useClass: EnvironmentServiceImpl},
        {provide: HttpService, useClass: HttpServiceImpl},
        {provide: TechnologyService, useClass: TechnologyService},
      ]
    });
    technologyService = TestBed.get(TechnologyService);
    httpTestingController = TestBed.get(HttpTestingController);
    store = TestBed.get(Store);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should return all the technologies from the store', () => {
    const technologies: Technology[] = technologyService.getAllTechnologiesFromStore();
    expect(technologies).toEqual(TechnologyMockData.getMockTechnologies());
  });

  it('should return all the technologies from the store as observables', () => {
    let result: Technology[];
    technologyService.getAllTechnologies()
      .subscribe((technologies: Technology[]) => {
        result = technologies;
      });
    expect(result).toEqual(TechnologyMockData.getMockTechnologies());
  });


});

describe('technology service with backend call', () => {

  let technologyService: TechnologyService;
  let httpTestingController: HttpTestingController;
  let store: Store<{ proposalTypes: Technology[] }>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        HttpClientTestingModule],
      providers: [
        TechnologyService,
        {provide: Store, useClass: MockStoreEmpty},
        {provide: EnvironmentService, useClass: EnvironmentServiceImpl},
        {provide: HttpService, useClass: MockHttpClientService},
        {provide: TechnologyService, useClass: TechnologyService},
      ]
    });
    technologyService = TestBed.get(TechnologyService);
    httpTestingController = TestBed.get(HttpTestingController);
    store = TestBed.get(Store);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should initialize the store from the backend', () => {
    let result: Technology[];
    technologyService.init()
      .subscribe((technologies: Technology[]) => {
        result = technologies;
      });
    expect(result).toEqual(TechnologyMockData.getMockTechnologies());
  });

  it('should return all the technologies from the backend', () => {
    let result: Technology[];
    technologyService.getAllTechnologies()
      .subscribe((technologies: Technology[]) => {
        result = technologies;
      });
    expect(result).toEqual(TechnologyMockData.getMockTechnologies());
  });
});

class MockStore extends BehaviorSubject<{ technologies: Technology[] }> {
  constructor() {
    super({technologies: TechnologyMockData.getMockTechnologies()});
  }
}

class MockStoreEmpty extends BehaviorSubject<{ technologies: Technology[] }> {
  constructor() {
    super({technologies: TechnologyMockData.getMockTechnologiesEmpty()});
  }

  dispatch() {
  }
}

class MockHttpClientService {
  get(): Observable<any> {
    return Observable.of(TechnologyMockData.getMockTechnologies());
  }
}

