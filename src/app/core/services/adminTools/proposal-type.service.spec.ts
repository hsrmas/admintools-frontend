import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { HttpClientModule } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { HttpServiceImpl } from '../http/http.service.impl';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HttpService } from '../http/http.service';
import { EnvironmentService } from '../environment/environment.service';
import { EnvironmentServiceImpl } from '../environment/environment-impl.service';
import { Observable } from 'rxjs/Observable';
import { ProposalTypeService } from './proposal-type.service';
import { ProposalType } from '../../domain/proposal-type';
import { ProposalTypeMockData } from '../../../unittest/mock-data/proposal-type-mock-data';

describe('proposal type service with store', () => {

  let proposalTypeService: ProposalTypeService;
  let httpTestingController: HttpTestingController;
  let store: Store<{ proposalTypes: ProposalType[] }>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        HttpClientTestingModule],
      providers: [
        ProposalTypeService,
        {provide: Store, useClass: MockStore},
        {provide: EnvironmentService, useClass: EnvironmentServiceImpl},
        {provide: HttpService, useClass: HttpServiceImpl},
        {provide: ProposalTypeService, useClass: ProposalTypeService},
      ]
    });
    proposalTypeService = TestBed.get(ProposalTypeService);
    httpTestingController = TestBed.get(HttpTestingController);
    store = TestBed.get(Store);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should return all the porposal-types from the store', () => {
    const proposalTypes: ProposalType[] = proposalTypeService.getAllProposalTypesFromStore();
    expect(proposalTypes).toEqual(ProposalTypeMockData.getMockProposalTypes());
  });

  it('should return all the proposal-types from the store', () => {
    let result: ProposalType[];
    proposalTypeService.getAllProposalTypes()
      .subscribe((proposalTypes: ProposalType[]) => {
        result = proposalTypes;
      });
    expect(result).toEqual(ProposalTypeMockData.getMockProposalTypes());
  });


});

describe('proposal-type service with backend call', () => {

  let proposalTypeService: ProposalTypeService;
  let httpTestingController: HttpTestingController;
  let store: Store<{ proposalTypes: ProposalType[] }>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        HttpClientTestingModule],
      providers: [
        ProposalTypeService,
        {provide: Store, useClass: MockStoreEmpty},
        {provide: EnvironmentService, useClass: EnvironmentServiceImpl},
        {provide: HttpService, useClass: MockHttpClientService},
        {provide: ProposalTypeService, useClass: ProposalTypeService},
      ]
    });
    proposalTypeService = TestBed.get(ProposalTypeService);
    httpTestingController = TestBed.get(HttpTestingController);
    store = TestBed.get(Store);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should initialize the store from the backend', () => {
    let result: ProposalType[];
    proposalTypeService.init()
      .subscribe((proposalTypes: ProposalType[]) => {
        result = proposalTypes;
      });
    expect(result).toEqual(ProposalTypeMockData.getMockProposalTypes());
  });
  it('should return all the institutes from the backend', () => {
    let result: ProposalType[];
    proposalTypeService.getAllProposalTypes()
      .subscribe((proposalTypes: ProposalType[]) => {
        result = proposalTypes;
      });
    expect(result).toEqual(ProposalTypeMockData.getMockProposalTypes());
  });
});

class MockStore extends BehaviorSubject<{ proposalTypes: ProposalType[] }> {
  constructor() {
    super({proposalTypes: ProposalTypeMockData.getMockProposalTypes()});
  }
}

class MockStoreEmpty extends BehaviorSubject<{ proposalTypes: ProposalType[] }> {
  constructor() {
    super({proposalTypes: ProposalTypeMockData.getMockProposalTypesEmpty()});
  }
  dispatch() {
  }
}

class MockHttpClientService {
  get(): Observable<any> {
    return Observable.of(ProposalTypeMockData.getMockProposalTypes());
  }
}

