import { Injectable } from '@angular/core';
import { EnvironmentService } from '../environment/environment.service';
import { AppState } from '../../statemanagement/app.state';
import { HttpService } from '../http/http.service';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { University } from '../../domain/university';
import { UniversityActions } from '../../statemanagement/actions';


@Injectable()
export class UniversityService {


  constructor(private http: HttpService,
              private env: EnvironmentService,
              private store: Store<AppState>) {
  }

  /**
   * Initialization of University.  Should only be used by the {@link APP_INITIALIZER} !
   * @returns {Observable<University[]>}
   */
  init(): Observable<University[]> {
    return this.http.get<University[]>(this.env.environment.apiServerRoot + '/universities/')
      .do((response: University[]) => this.store.dispatch(new UniversityActions.LoadUniversities(response)));
  }

  /**
   * Get all universities from store or backend
   * @returns {Observable<University[]>}
   */
  getAllUniversities(): Observable<University[]> {
    const universities = this.getAllUniversitiesFromStore();
    if (universities.length > 0) {
      return Observable.of(universities);
    }
    // do http call
    return this.http.get<University[]>(this.env.environment.apiServerRoot + '/universities/')
      .do((response: University[]) => this.dispatchUniversityHttpCallToStore(response));
  }

  /**
   * Get all universities from store
   * @returns {University[]}
   */
  getAllUniversitiesFromStore(): University[] {
    let universitiesFromStore: University[];
    this.store
      .take(1)
      .map((appState: AppState) => appState.universities)
      .subscribe((universities: University[]) => universitiesFromStore = universities);
    universitiesFromStore.map((university: University) => Object.assign(Object.create(University.prototype), university));
    return universitiesFromStore;
  }

  /**
   * Dispatch the University[] to the store after Http-Call.
   * @param {University[]} universities
   */
  private dispatchUniversityHttpCallToStore(universities: University[]): void {
    universities.forEach((university: University) => this.store.dispatch(new UniversityActions.CreateOrUpdateUniversity(university)));
  }

}
