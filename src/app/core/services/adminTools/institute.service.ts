import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Institute } from '../../domain/institute';
import { EnvironmentService } from '../environment/environment.service';
import { AppState } from '../../statemanagement/app.state';
import { HttpService } from '../http/http.service';
import { Store } from '@ngrx/store';
import { InstituteActions } from '../../statemanagement/actions';


@Injectable()
export class InstituteService {

  constructor(private http: HttpService,
              private env: EnvironmentService,
              private store: Store<AppState>) {
  }

  /**
   * Initialization of Institute.  Should only be used by the {@link APP_INITIALIZER} !
   * @returns {Observable<Institute[]>}
   */
  init(): Observable<Institute[]> {
    return this.http.get<Institute[]>(this.env.environment.apiServerRoot + '/institutes/')
      .do((result: Institute[]) => this.store.dispatch(new InstituteActions.LoadInstitutes(result)));
  }

  /**
   * Get all Institutes from store or backend
   * @returns {Observable<Institute[]>}
   */
  getAllInstitutes(): Observable<Institute[]> {
    const institutes = this.getAllInstitutesFromStore();
    if (institutes.length > 0) {
      return Observable.of(institutes);
    }
    // do http call
    return this.http.get<Institute[]>(this.env.environment.apiServerRoot + '/institutes/')
      .do((result: Institute[]) => this.dispatchUniversityHttpCallToStore(result));
  }

  /**
   * Get all the institutes from the store
   * @returns {Institute[]}
   */
  getAllInstitutesFromStore(): Institute[] {
    let institutesFromStore: Institute[];
    this.store.take(1)
      .map((appState: AppState) => appState.institutes)
      .subscribe((institutes: Institute[]) => institutesFromStore = institutes);
    institutesFromStore.map((institute: Institute) => Object.assign(Object.create(Institute.prototype), institute));
    return institutesFromStore;
  }

  /**
   * Dispatch the Institutes to the store after Http-Call.
   * @param {Institute[]} institutes
   */
  private dispatchUniversityHttpCallToStore(institutes: Institute[]): void {
    institutes.forEach((institute: Institute) => this.store.dispatch(new InstituteActions.CreateOrUpdateInstitute(institute)));
  }

}
