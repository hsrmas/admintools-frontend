/**
 * Environment Service
 *
 * Used to handle environment specific functions (Which API Endpoint, Databases, etc.)
 */
export abstract class EnvironmentService {

  /**
   * Return the environment defined in '/src/app/enviroments'
   * @returns {any}
   */
  get environment(): any {
    return this.getEnvironment();
  }

  abstract getEnvironment(): any;

}
