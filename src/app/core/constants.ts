/**
 * Project Constants *
 */
export const CONSTANTS = {
  LANGUAGE_GERMAN: 'de',
  LANGUAGE_ENGLISH: 'en',
  EXAMPLE_PROPOSAL_NAME: 'neuewebseite',
  PROPOSAL_TYPE_NEW: 'ProposalType_NewApplication',
  PROPOSAL_TYPE_DELETE: 'ProposalType_DecomposeApplication',
  DEFAULT_PROPOSAL_NAME: 'neuewebseite',
  APPLICATION_STATE_NEW: 1,
  DEBOUNCE_TIME_CHANGE_DETECTION: 400,
  ORIGIN_MYTODOS: 'MYTODOS',
  ORIGIN_MYPROPOSALS: 'MYPROPOSALS',
  ORIGIN_ADMIN: 'ADMIN',
  APPROVERS: [
    'ApproverWebadministrator',
    'ApproverIT',
    'ApproverUniversity',
    'ApproverCommunication',
    'admin'
  ]
};
