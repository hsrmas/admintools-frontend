/**
 * Proposal Object
 *
 * Used to createNewProposal a new proposal.
 * Except for the proposal-attribtues all other members are references by id
 */
export class ProposalCreate {

  public id: string;

  // proposal attributes
  public purpose: string;
  public admin_email: string;
  public name: string;

  // antragsteller (user)
  public owner_id: number;
  // New(1), Approved University(2) etc..
  public status_id: number;
  // cms, webspace
  public technology_id: number;
  // new application, decompose application
  public proposal_type_id: number;
  // lehrgang
  public institute_id: number;
  // fhnw, services....
  public university_id: number;
}
