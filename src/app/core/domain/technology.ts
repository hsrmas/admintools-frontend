export class Technology {
  public url: string;
  public id: number;
  public name: string;
  public key: string;
}
