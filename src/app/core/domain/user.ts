export class User {
  public url: string;
  public id: number;
  public username: string;
  public email: string;
  public first_name: string;
  public last_name: string;
  public telephone_number: string;
  public is_staff: boolean;
  public is_superuser: boolean;
  public university: string[];
  public groups: string[];
}
