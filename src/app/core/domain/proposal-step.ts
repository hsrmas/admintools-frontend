export class ProposalStep {
  public url: string;
  public proposal_type: string;
  public name: string;
  public order: number;
  public key: string;
}
