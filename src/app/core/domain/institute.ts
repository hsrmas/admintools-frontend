export class Institute {
  public url: string;
  public id: number;
  public name: string;
  public token: string;
  public university: string;
  public key: string;
}
