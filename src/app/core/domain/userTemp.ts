/**
 *
 */
export class UserTemp {
  constructor(public isAdmin: boolean,
              public shibbolethUserId: string,
              public firstName: string,
              public lastName: string,
              public emailAddress: string,
              public phoneNumber: string) {
  }
}
