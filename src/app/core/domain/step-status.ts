export class StepStatus {
  public id: number;
  public is_completed: boolean;
  public name: string;
}
