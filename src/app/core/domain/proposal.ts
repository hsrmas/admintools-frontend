import { StepStatus } from './step-status';
import { ProposalStep } from './proposal-step';
import { User } from './user';
import { Technology } from './technology';
import { ProposalType } from './proposal-type';
import { Institute } from './institute';

export class Proposal {
  public id: number;
  public name: string;
  public purpose: string;
  public admin_email: string;
  public owner_id: number;
  public status_id: number;
  public technology_id: number;
  public proposal_type_id: number;
  public institute_id: number;

  public owner: User;
  public status: ProposalStep;
  public technology: Technology;
  public proposal_type: ProposalType;
  public institute: Institute;

  public all_states: StepStatus[];
  public application_status: string;
  public is_rejected: boolean;
  public rejection_reason: string;
  public application_id: number;
}
