export class ProposalType {
  public url: string;
  public id: number;
  public name: string;
  public key: string;
}
