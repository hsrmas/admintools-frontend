export class Application {
  public id: number;
  public name: string;
  public path: string;
  public container_ip: string;
}
