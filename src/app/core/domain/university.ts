import { Institute } from './institute';

export class University {
  public url: string;
  public id: number;
  public name: string;
  public token: string;
  public institute: Institute[];
}
