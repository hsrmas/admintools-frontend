import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { EnvironmentService } from '../services/environment/environment.service';


export class DefaultInterceptor implements HttpInterceptor {

  /**
   * Holds default Header definitions like Language, Content type etc.
   */
  protected defaultHeaders: any = {};


  constructor(private envService: EnvironmentService) {
    this.initializeDefaultHeaders();
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(this.setupRequestOptions(req));
  }

  /**
   * Initialize default headers
   */
  private initializeDefaultHeaders(): void {
    // this.defaultHeaders['Content-Type'] = 'application/json';
    // this.defaultHeaders['Cache-Control'] = 'no-cache';
    // this.defaultHeaders['Pragma'] = 'no-cache';
    // this.defaultHeaders['Expires'] = '0';
  }

  /**
   * Merge default options with existing options into header options.
   * @param {HttpRequest<any>} request - Could already have some headers set and we need to make sure, that we do not loose them
   * @returns {HttpRequest<any>} Extended by the default headers
   */
  private setupRequestOptions(request: HttpRequest<any>): HttpRequest<any> {
    let headers = request.headers;
    for (const property in this.defaultHeaders) {
      if (this.defaultHeaders.hasOwnProperty(property) && !request.headers.hasOwnProperty(property)) {
        headers = headers.append(property, this.defaultHeaders[property]);
      }
    }
    return request.clone({headers: headers});
  }
}
