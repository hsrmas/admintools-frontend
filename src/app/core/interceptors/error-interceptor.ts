import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Injector } from '@angular/core';
import { Router } from '@angular/router';

export class ErrorInterceptor implements HttpInterceptor {

  constructor(private injector: Injector) {
  }

  /**
   * Intercept Http errors and redirect to the error-page
   * @param {HttpRequest<any>} request
   * @param {HttpHandler} next
   * @returns {Observable<HttpEvent<any>>}
   */
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).do((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
        }
      },
      (error: any) => {
        if (error instanceof HttpErrorResponse) {
          this.injector.get(Router).navigate(['/errorpage']);
        }
      });
  }
}
