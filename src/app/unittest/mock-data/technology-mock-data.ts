import { Technology } from '../../core/domain/technology';

export class TechnologyMockData {

  static technology1 = {
    url: 'url1',
    id: 1,
    name: 'name1',
    key: 'key1',
  };

  static technology2 = {
    url: 'url2',
    id: 2,
    name: 'name2',
    key: 'key2',
  };

  static technology3 = {
    url: 'url3',
    id: 3,
    name: 'name3',
    key: 'key3',
  };

  static getMockTechnologies(): Technology[] {
    return [
      Object.assign(Object.create(Technology.prototype), {
        url: 'url1',
        id: 1,
        name: 'name1',
        key: 'key1',
      }),
      Object.assign(Object.create(Technology.prototype), {
        url: 'url2',
        id: 2,
        name: 'name2',
        key: 'key2',
      })
    ];
  }

  static getMockTechnologiesEmpty(): Technology[] {
    return [];
  }

  static newTechnology(): Technology {
    return Object.assign(Object.create(Technology.prototype), {
      url: 'url3',
      id: 3,
      name: 'name3',
      key: 'key3',
    });
  }

  static updatedTechnology(): Technology {
    return Object.assign(Object.create(Technology.prototype), {
      url: 'url2',
      id: 2,
      name: 'updated-name',
      key: 'key2',
    });
  }

}
