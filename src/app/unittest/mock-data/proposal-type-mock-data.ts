import { ProposalType } from '../../core/domain/proposal-type';

export class ProposalTypeMockData {

  static proposalType1 = {
    url: 'url1',
    id: 1,
    name: 'name1',
    key: 'key1'
  };

  static proposalType2 = {
    url: 'url2',
    id: 2,
    name: 'name2',
    key: 'key2'
  };

  static proposalType3 = {
    url: 'url3',
    id: 3,
    name: 'name3',
    key: 'key3'
  };

  static getMockProposalTypes(): ProposalType[] {
    return [
      Object.assign(Object.create(ProposalType.prototype), {
        url: 'url1',
        id: 1,
        name: 'name',
        key: 'key',
      }),
      Object.assign(Object.create(ProposalType.prototype), {
        url: 'url2',
        id: 2,
        name: 'name2',
        key: 'key2',
      })
    ];
  }

  static getMockProposalTypesEmpty(): ProposalType[] {
    return [];
  }

  static updatedProposalType(): ProposalType {
    return Object.assign(Object.create(ProposalType.prototype), {
      url: 'url2',
      id: 2,
      name: 'name-updated',
      key: 'key2',
    });
  }

  static newProposalType(): ProposalType {
    return Object.assign(Object.create(ProposalType.prototype), {
      url: 'url3',
      id: 3,
      name: 'name3',
      key: 'key3',
    });
  }
}
