import { Application } from '../../core/domain/application';

export class ApplicationMockData {

  static firstApplication = {
    id: 1,
    name: 'name1',
    path: 'path1',
    container_ip: '1.1.1.1'
  };

  static secondApplication = {
    id: 2,
    name: 'name2',
    path: 'path2',
    container_ip: '2.2.2.2'
  };

  static thirdApplication = {
    id: 3,
    name: 'name3',
    path: 'path3',
    container_ip: '3.3.3.3'
  };

  static getMockApplications(): Application[] {
    return [
      Object.assign(Object.create(Application.prototype), {
        id: 1,
        name: 'name1',
        path: 'path1',
        container_ip: '1.1.1.1'
      }),
      Object.assign(Object.create(Application.prototype), {
        id: 2,
        name: 'name2',
        path: 'path2',
        container_ip: '1.1.1.1'
      }),
    ];
  }

  static getMockApplicationsEmpty(): Application[] {
    return [];
  }

  static newApplication(): Application {
    return Object.assign(Object.create(Application.prototype), {
      id: 3,
      name: 'name-new',
      path: 'pathnew',
      container_ip: '1.1.1.1'
    });
  }

  static updatedApplication(): Application {
    return Object.assign(Object.create(Application.prototype), {
      id: 2,
      name: 'name-updated',
      path: 'path2',
      container_ip: '1.1.1.1'
    });
  }
}
