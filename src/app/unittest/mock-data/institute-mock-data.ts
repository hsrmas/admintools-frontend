import { Institute } from '../../core/domain/institute';

export class InstituteMockData {

  static institute1 = {
    url: 'url1',
    id: 1,
    name: 'name1',
    token: 'token1',
    university: 'uni1',
    key: 'key1',
  };

  static institute2 = {
    url: 'url2',
    id: 2,
    name: 'name2',
    token: 'token2',
    university: 'uni2',
    key: 'key2',
  };

  static institute3 = {
    url: 'url3',
    id: 3,
    name: 'name3',
    token: 'token3',
    university: 'uni2',
    key: 'key3',
  };


  static getMockInstitutes(): Institute[] {
    return [
      Object.assign(Object.create(Institute.prototype), {
        url: 'url1',
        id: 1,
        name: 'name',
        token: 'token',
        university: 'uni',
        key: 'key',
      }),
      Object.assign(Object.create(Institute.prototype), {
        url: 'url2',
        id: 2,
        name: 'name2',
        token: 'token2',
        university: 'uni2',
        key: 'key2',
      })
    ];
  }

  static getMockInstitutesEmpty(): Institute[] {
    return [];
  }

  static newInstitute(): Institute {
    return Object.assign(Object.create(Institute.prototype), {
      url: 'url3',
      id: 3,
      name: 'name3',
      token: 'token3',
      university: 'uni3',
      key: 'key3',
    });
  }

  static updatedInstitute(): Institute {
    return Object.assign(Object.create(Institute.prototype), {
      url: 'url2',
      id: 2,
      name: 'new-name',
      token: 'token2',
      university: 'uni2',
      key: 'key2',
    });
  }

}
