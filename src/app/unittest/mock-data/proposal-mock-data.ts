import { Proposal } from '../../core/domain/proposal';
import { ProposalCreate } from '../../core/domain/proposal-create';

export class ProposalMockData {

  static proposal1 = {
    id: 1,
    name: 'proposal1',
    purpose: 'purpose1',
    admin_email: 'admin-email1',
    owner_id: 1,
    status_id: 1,
    technology_id: 1,
    proposal_type_id: 1,
    institute_id: 1,
    owner: {
      url: 'url1',
      id: 1,
      username: 'usernam1',
      email: 'useremail1',
      groups: []
    },
    status: {
      url: 'url1',
      proposal_type: 'typ1',
      name: 'name1',
      order: 1,
      key: 'key1'
    },
    technology: {
      url: 'url1',
      id: 1,
      name: 'name1',
      key: 'key1',
    },
    proposal_type: {
      url: 'url1',
      id: 1,
      name: 'name',
      key: 'key',
    },
    institute: {
      url: 'url1',
      id: 1,
      name: 'name',
      token: 'token',
      university: 'uni',
      key: 'key',
    },
    all_states: [
      {
        id: 1,
        is_completed: true,
        name: 'status1'
      },
      {
        id: 2,
        is_completed: false,
        name: 'status2'
      }
    ],
    application_status: 'status',
    is_rejected: false,
    rejection_reason: '',
    application_id: 1,
  };

  static proposal2 = {
    id: 2,
    name: 'proposal2',
    purpose: 'purpose2',
    admin_email: 'admin-email1',
    owner_id: 1,
    status_id: 1,
    technology_id: 1,
    proposal_type_id: 1,
    institute_id: 1,
    owner: {
      url: 'url1',
      id: 1,
      username: 'usernam1',
      email: 'useremail1',
      groups: []
    },
    status: {
      url: 'url1',
      proposal_type: 'typ1',
      name: 'name1',
      order: 1,
      key: 'key1'
    },
    technology: {
      url: 'url1',
      id: 1,
      name: 'name1',
      key: 'key1',
    },
    proposal_type: {
      url: 'url1',
      id: 1,
      name: 'name',
      key: 'key',
    },
    institute: {
      url: 'url1',
      id: 1,
      name: 'name',
      token: 'token',
      university: 'uni',
      key: 'key',
    },
    all_states: [
      {
        id: 1,
        is_completed: true,
        name: 'status1'
      },
      {
        id: 2,
        is_completed: false,
        name: 'status2'
      }
    ],
    application_status: 'status',
    is_rejected: false,
    rejection_reason: '',
    application_id: 1,
  };

  static proposal3 = {
    id: 3,
    name: 'proposal3',
    purpose: 'purpose3',
    admin_email: 'admin-email1',
    owner_id: 1,
    status_id: 1,
    technology_id: 1,
    proposal_type_id: 1,
    institute_id: 1,
    owner: {
      url: 'url1',
      id: 1,
      username: 'usernam1',
      email: 'useremail1',
      groups: []
    },
    status: {
      url: 'url1',
      proposal_type: 'typ1',
      name: 'name1',
      order: 1,
      key: 'key1'
    },
    technology: {
      url: 'url1',
      id: 1,
      name: 'name1',
      key: 'key1',
    },
    proposal_type: {
      url: 'url1',
      id: 1,
      name: 'name',
      key: 'key',
    },
    institute: {
      url: 'url1',
      id: 1,
      name: 'name',
      token: 'token',
      university: 'uni',
      key: 'key',
    },
    all_states: [
      {
        id: 1,
        is_completed: true,
        name: 'status1'
      },
      {
        id: 2,
        is_completed: false,
        name: 'status2'
      }
    ],
    application_status: 'status',
    is_rejected: false,
    rejection_reason: '',
    application_id: 1,
  };

  static getMockProposalsEmpty(): Proposal[] {
    return [];
  }

  static getMockProposals(): Proposal[] {
    return [
      Object.assign(Object.create(Proposal.prototype), {
        id: 1,
        name: 'proposal1',
        purpose: 'purpose1',
        admin_email: 'admin-email1',
        owner_id: 1,
        status_id: 1,
        technology_id: 1,
        proposal_type_id: 1,
        institute_id: 1,
        owner: {
          url: 'url1',
          id: 1,
          username: 'usernam1',
          email: 'useremail1',
          groups: []
        },
        status: {
          url: 'url1',
          proposal_type: 'typ1',
          name: 'name1',
          order: 1,
          key: 'key1'
        },
        technology: {
          url: 'url1',
          id: 1,
          name: 'name1',
          key: 'key1',
        },
        proposal_type: {
          url: 'url1',
          id: 1,
          name: 'name',
          key: 'key',
        },
        institute: {
          url: 'url1',
          id: 1,
          name: 'name',
          token: 'token',
          university: 'uni',
          key: 'key',
        },
        all_states: [
          {
            id: 1,
            is_completed: true,
            name: 'status1'
          },
          {
            id: 2,
            is_completed: false,
            name: 'status2'
          }
        ],
        application_status: 'status',
        is_rejected: false,
        rejection_reason: '',
        application_id: 1,
      }),
      Object.assign(Object.create(Proposal.prototype), {
        id: 2,
        name: 'proposal2',
        purpose: 'purpose2',
        admin_email: 'admin-email1',
        owner_id: 1,
        status_id: 1,
        technology_id: 1,
        proposal_type_id: 1,
        institute_id: 1,
        owner: {
          url: 'url1',
          id: 1,
          username: 'usernam1',
          email: 'useremail1',
          groups: []
        },
        status: {
          url: 'url1',
          proposal_type: 'typ1',
          name: 'name1',
          order: 1,
          key: 'key1'
        },
        technology: {
          url: 'url1',
          id: 1,
          name: 'name1',
          key: 'key1',
        },
        proposal_type: {
          url: 'url1',
          id: 1,
          name: 'name',
          key: 'key',
        },
        institute: {
          url: 'url1',
          id: 1,
          name: 'name',
          token: 'token',
          university: 'uni',
          key: 'key',
        },
        all_states: [
          {
            id: 1,
            is_completed: true,
            name: 'status1'
          },
          {
            id: 2,
            is_completed: false,
            name: 'status2'
          }
        ],
        application_status: 'status',
        is_rejected: false,
        rejection_reason: '',
        application_id: 1,
      })
    ];
  }

  static newProposal(): Proposal {
    return Object.assign(Object.create(Proposal.prototype), {
      id: 3,
      name: 'proposal3',
      purpose: 'purpose3',
      admin_email: 'admin-email1',
      owner_id: 1,
      status_id: 1,
      technology_id: 1,
      proposal_type_id: 1,
      institute_id: 1,
      owner: {
        url: 'url1',
        id: 1,
        username: 'usernam1',
        email: 'useremail1',
        groups: []
      },
      status: {
        url: 'url1',
        proposal_type: 'typ1',
        name: 'name1',
        order: 1,
        key: 'key1'
      },
      technology: {
        url: 'url1',
        id: 1,
        name: 'name1',
        key: 'key1',
      },
      proposal_type: {
        url: 'url1',
        id: 1,
        name: 'name',
        key: 'key',
      },
      institute: {
        url: 'url1',
        id: 1,
        name: 'name',
        token: 'token',
        university: 'uni',
        key: 'key',
      },
      all_states: [
        {
          id: 1,
          is_completed: true,
          name: 'status1'
        },
        {
          id: 2,
          is_completed: false,
          name: 'status2'
        }
      ],
      application_status: 'status',
      is_rejected: false,
      rejection_reason: '',
      application_id: 1,
    });
  }

  static updatedProposal(): Proposal {
    return Object.assign(Object.create(Proposal.prototype), {
      id: 2,
      name: 'proposal2',
      purpose: 'purpose-new',
      admin_email: 'admin-email1',
      owner_id: 1,
      status_id: 1,
      technology_id: 1,
      proposal_type_id: 1,
      institute_id: 1,
      owner: {
        url: 'url1',
        id: 1,
        username: 'usernam1',
        email: 'useremail1',
        groups: []
      },
      status: {
        url: 'url1',
        proposal_type: 'typ1',
        name: 'name1',
        order: 1,
        key: 'key1'
      },
      technology: {
        url: 'url1',
        id: 1,
        name: 'name1',
        key: 'key1',
      },
      proposal_type: {
        url: 'url1',
        id: 1,
        name: 'name',
        key: 'key',
      },
      institute: {
        url: 'url1',
        id: 1,
        name: 'name',
        token: 'token',
        university: 'uni',
        key: 'key',
      },
      all_states: [
        {
          id: 1,
          is_completed: true,
          name: 'status1'
        },
        {
          id: 2,
          is_completed: false,
          name: 'status2'
        }
      ],
      application_status: 'status',
      is_rejected: false,
      rejection_reason: '',
      application_id: 1,
    });
  }

  static newProposalCreate(): ProposalCreate {
    return Object.assign(Object.create(ProposalCreate.prototype), {
      purpose: 'purpose',
      admin_email: 'adminmail',
      name: 'name',
      owner_id: 1,
      status_id: 1,
      technology_id: 1,
      proposal_type_id: 1,
      institute_id: 1,
      university_id: 1,
    });
  }

  static newProposalResponse(): Proposal {
    return Object.assign(Object.create(Proposal.prototype), {
      id: 3,
      name: 'proposal3',
      purpose: 'purpose3',
      admin_email: 'admin-email1',
      owner_id: 1,
      status_id: 1,
      technology_id: 1,
      proposal_type_id: 1,
      institute_id: 1,
      owner: {
        url: 'url1',
        id: 1,
        username: 'usernam1',
        email: 'useremail1',
        groups: []
      },
      status: {
        url: 'url1',
        proposal_type: 'typ1',
        name: 'name1',
        order: 1,
        key: 'key1'
      },
      technology: {
        url: 'url1',
        id: 1,
        name: 'name1',
        key: 'key1',
      },
      proposal_type: {
        url: 'url1',
        id: 1,
        name: 'name',
        key: 'key',
      },
      institute: {
        url: 'url1',
        id: 1,
        name: 'name',
        token: 'token',
        university: 'uni',
        key: 'key',
      },
      all_states: [
        {
          id: 1,
          is_completed: true,
          name: 'status1'
        },
        {
          id: 2,
          is_completed: false,
          name: 'status2'
        }
      ],
      application_status: 'status',
      is_rejected: false,
      rejection_reason: '',
      application_id: 1,
    });
  }
}



