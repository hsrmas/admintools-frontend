import { User } from '../../core/domain/user';

export class UserMockData {

  static user1 = {
    url: 'url1',
    id: 1,
    username: 'name',
    email: 'arjan.vandoesburg@fhnw.ch',
    first_name: 'Arjan',
    last_name: 'van Doesburg',
    telephone_number: '+41 56 200 00 00',
    is_staff: true,
    is_superuser: false,
    university: [
      'uni1'
    ],
    groups: [
      'group1'
    ]
  };
  static user2 = {
    url: 'url2',
    id: 2,
    username: 'name',
    email: 'token',
    first_name: 'firstname',
    last_name: 'lastname',
    telephone_number: '+41 56 200 00 00',
    is_staff: true,
    is_superuser: false,
    university: [
      'uni1'
    ],
    groups: [
      'group1'
    ]
  };

  static getMockUsers(): User[] {
    return [
      Object.assign(Object.create(User.prototype), {
        url: 'url1',
        id: 1,
        username: 'name',
        email: 'token',
        first_name: 'firstname',
        last_name: 'lastname',
        telephone_number: '+41 56 200 00 00',
        is_staff: true,
        is_superuser: false,
        university: [
          'uni1'
        ],
        groups: [
          'group1'
        ]
      }),
      Object.assign(Object.create(User.prototype), {
        url: 'url2',
        id: 2,
        username: 'name',
        email: 'token',
        first_name: 'firstname',
        last_name: 'lastname',
        telephone_number: '+41 56 200 00 00',
        is_staff: true,
        is_superuser: false,
        university: [
          'uni1'
        ],
        groups: [
          'group1'
        ]
      })
    ];
  }

  static getMockUsersEmpty(): User[] {
    return [];
  }

  static newUser(): User {
    return Object.assign(Object.create(User.prototype), {
      url: 'newUrl',
      id: 4,
      username: 'newname',
      email: 'newemail',
      first_name: 'newfirstname',
      last_name: 'newlastname',
      telephone_number: '+41 56 200 00 00',
      is_staff: true,
      is_superuser: false,
      university: [
        'uni1'
      ],
      groups: [
        'group1'
      ]
    });
  }

  static updatedUser(): User {
    return Object.assign(Object.create(User.prototype), {
      url: 'url2',
      id: 2,
      username: 'updated-name',
      email: 'token',
      first_name: 'firstname',
      last_name: 'lastname',
      telephone_number: '+41 56 200 00 00',
      is_staff: true,
      is_superuser: false,
      university: [
        'uni1'
      ],
      groups: [
        'group1'
      ]
    });
  }
}
