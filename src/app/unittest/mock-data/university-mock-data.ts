import { University } from '../../core/domain/university';

export class UniversityMockData {

  static university1 = {
    url: 'url1',
    id: 1,
    name: 'name',
    token: 'token',
    institute: [
      {
        url: 'url1',
        id: 1,
        name: 'name',
        token: 'token',
        university: 'uni',
        key: 'key',
      },
      {
        url: 'url2',
        id: 2,
        name: 'name',
        token: 'token',
        university: 'uni',
        key: 'key',
      },
    ]
  };

  static university2 = {
    url: 'url2',
    id: 2,
    name: 'name',
    token: 'token',
    institute: [
      {
        url: 'url1',
        id: 1,
        name: 'name',
        token: 'token',
        university: 'uni',
        key: 'key',
      },
      {
        url: 'url2',
        id: 2,
        name: 'name',
        token: 'token',
        university: 'uni',
        key: 'key',
      },
    ]
  };

  static getMockUniversities(): University[] {
    return [
      Object.assign(Object.create(University.prototype), {
        url: 'url1',
        id: 1,
        name: 'name',
        token: 'token',
        institute: [
          {
            url: 'url1',
            id: 1,
            name: 'name',
            token: 'token',
            university: 'uni',
            key: 'key',
          },
          {
            url: 'url2',
            id: 2,
            name: 'name',
            token: 'token',
            university: 'uni',
            key: 'key',
          },
        ]
      }),
      Object.assign(Object.create(University.prototype), {
        url: 'url2',
        id: 2,
        name: 'updated-name',
        token: 'token2',
        institute: [
          {
            url: 'url1',
            id: 1,
            name: 'name',
            token: 'token',
            university: 'uni',
            key: 'key',
          },
          {
            url: 'url2',
            id: 2,
            name: 'name',
            token: 'token',
            university: 'uni',
            key: 'key',
          },
        ]
      })
    ];
  }

  static getMockUnivseritiesEmpty(): University[] {
    return [];
  }

  static newUniversity(): University {
    return Object.assign(Object.create(University.prototype), {
      url: 'url3',
      id: 3,
      name: 'name',
      token: 'token',
      institute: [
        {
          url: 'url1',
          id: 1,
          name: 'name',
          token: 'token',
          university: 'uni',
          key: 'key',
        },
        {
          url: 'url2',
          id: 2,
          name: 'name',
          token: 'token',
          university: 'uni',
          key: 'key',
        },
      ]
    });
  }

  static updatedUniversity(): University {
    return Object.assign(Object.create(University.prototype), {
      url: 'url2',
      id: 2,
      name: 'updated-name',
      token: 'token',
      institute: [
        {
          url: 'url1',
          id: 1,
          name: 'name',
          token: 'token',
          university: 'uni',
          key: 'key',
        },
        {
          url: 'url2',
          id: 2,
          name: 'name',
          token: 'token',
          university: 'uni',
          key: 'key',
        },
      ]
    });
  }
}
