import { Observable } from 'rxjs/Observable';

export class EventServiceMock {

  proposalEvent(value) {
  }

  getEventItems(): Observable<any> {
    return Observable.of(null);
  }
}
