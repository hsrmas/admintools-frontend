import { Proposal } from '../../core/domain/proposal';
import { Observable } from 'rxjs/Observable';
import { ProposalMockData } from '../mock-data/proposal-mock-data';

export class ProposalServiceMock {

  getAllMyProposals(): Observable<Proposal[]> {
    return Observable.of(ProposalMockData.getMockProposals());
  }

  getAllMyTodos(): Observable<Proposal[]> {
    return Observable.of(ProposalMockData.getMockProposals());
  }

  getAllProposals(): Observable<Proposal[]> {
    return Observable.of(ProposalMockData.getMockProposals());
  }
}
