import { Observable } from 'rxjs/Observable';
import { ProposalType } from '../../core/domain/proposal-type';
import { ProposalTypeMockData } from '../mock-data/proposal-type-mock-data';

export class ProposalTypeServiceMock {
  getAllProposalTypes(): Observable<ProposalType[]> {
    return Observable.of(ProposalTypeMockData.getMockProposalTypes());
  }
}
