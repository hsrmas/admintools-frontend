import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

export class StoreMock extends BehaviorSubject<any> {
  constructor() {
    super({});
  }

  select(): Observable<any> {
    return Observable.of({});
  }

  dispatch() {
  }
}
