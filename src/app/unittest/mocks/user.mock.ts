import { User } from '../../core/domain/user';
import { UserMockData } from '../mock-data/user-mock-data';

export class UserMock {
  static getMockUser(): User {
    return UserMockData.user1;
  }
}
