import { Institute } from '../../core/domain/institute';
import { Observable } from 'rxjs/Observable';
import { InstituteMockData } from '../mock-data/institute-mock-data';

export class InstituteServiceMock {
  getAllInstitutes(): Observable<Institute[]> {
    return Observable.of(InstituteMockData.getMockInstitutes());
  }
}
