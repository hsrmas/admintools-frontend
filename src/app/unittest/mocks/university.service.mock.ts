import { University } from '../../core/domain/university';
import { Observable } from 'rxjs/Observable';
import { UniversityMockData } from '../mock-data/university-mock-data';

export class UniversityServiceMock {
  getAllUniversities(): Observable<University[]> {
    return Observable.of(UniversityMockData.getMockUniversities());
  }
}
