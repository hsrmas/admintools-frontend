import { Technology } from '../../core/domain/technology';
import { Observable } from 'rxjs/Observable';
import { TechnologyMockData } from '../mock-data/technology-mock-data';

export class TechnologyServiceMock {
  getAllTechnologies(): Observable<Technology[]> {
    return Observable.of(TechnologyMockData.getMockTechnologies());
  }
}
