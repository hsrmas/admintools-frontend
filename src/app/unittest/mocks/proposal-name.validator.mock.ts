import { AbstractControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

export class ProposalNameValidatorMock {
  validate(control: AbstractControl): Observable<boolean> {
    return Observable.of(null);
  }
}
