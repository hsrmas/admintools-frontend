import {
  Component,
  OnDestroy,
  OnInit,
  ViewChild
} from '@angular/core';
import { AppState } from './core/statemanagement/app.state';
import { Store } from '@ngrx/store';
import { EnvironmentService } from './core/services/environment/environment.service';
import { TranslateService } from '@ngx-translate/core';
import { MatSidenav } from '@angular/material';
import { ObservableMedia } from '@angular/flex-layout';
import { ProposalService } from './core/services/adminTools/proposal.service';
import { EventService } from './core/services/events/event-service';
import { Subscription } from 'rxjs/Subscription';
import { Proposal } from './core/domain/proposal';
import { UserService } from './core/services/adminTools/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  @ViewChild('sideNav', {read: MatSidenav}) sideNav: MatSidenav;
  public navMode = 'side';
  public numberOfTodos: number;
  public proposalEventSubscription: Subscription;
  public envName: string = null;
  public isAdmin: boolean;
  public loggedInUser: string;

  constructor(private store: Store<AppState>,
              private envService: EnvironmentService,
              private translateService: TranslateService,
              private proposalService: ProposalService,
              private eventService: EventService,
              private userService: UserService,
              private observableMedia: ObservableMedia) {
    this.numberOfTodos = 0;
  }

  ngOnInit(): void {

    // get the current environment
    this.envName = this.envService.getEnvironment().stage;

    // set the todo counter
    this.proposalEventSubscription = this.eventService.getEventItems()
      .subscribe(() => {
        this.proposalService.getAllMyTodos()
          .map((proposals: Proposal[]) => proposals.filter((proposal: Proposal) => proposal.is_rejected === false))
          .map((proposals: Proposal[]) => this.numberOfTodos = proposals.length)
          .subscribe();
      });

    this.isAdmin = this.userService.isAdmin();
    this.loggedInUser = this.userService.getUser().email;

    // watch for media changes to handle the side-menu
    if (this.observableMedia.isActive('xs') || this.observableMedia.isActive('sm')) {
      this.navMode = 'over';
    }
    this.observableMedia.asObservable()
      .subscribe(change => {
        switch (change.mqAlias) {
          case 'xs':
          case 'sm':
            this.navMode = 'over';
            this.sideNav.close();
            break;
          default:
            this.navMode = 'side';
            this.sideNav.open();
            break;
        }
      });
  }

  /*
  @HostListener('window:beforeunload', ['$event'])
  beforeUnloadHander(event) {
    const confirmationMessage = this.translateService.instant('NAVIGATION.VALIDATION.CLOSE_WINDOW');
    event.returnValue = confirmationMessage;
    return confirmationMessage;
  }
  */
  ngOnDestroy(): void {
    this.proposalEventSubscription.unsubscribe();
  }
}
