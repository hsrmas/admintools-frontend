import {
  async,
  TestBed
} from '@angular/core/testing';

import { ProposalHomeComponent } from './proposal-home.component';
import {
  TranslateLoader,
  TranslateModule
} from '@ngx-translate/core';
import {
  FormsModule,
  ReactiveFormsModule
} from '@angular/forms';
import { RouterModule } from '@angular/router';
import {
  HttpClient,
  HttpHandler
} from '@angular/common/http';
import { MaterialModule } from '../../catalogue/material.module';
import { RouterTestingModule } from '@angular/router/testing';
import { createTranslationLoader } from '../../core/services/language/translation.loader';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EnvironmentService } from '../../core/services/environment/environment.service';
import { LanguageService } from '../../core/services/language/language.service';
import { HttpService } from '../../core/services/http/http.service';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ToastsService } from '../../core/services/toasts/toasts-service';

describe('ProposalHomeComponent', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProposalHomeComponent],
      imports: [
        RouterModule,
        MaterialModule,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: (createTranslationLoader),
            deps: [HttpClient]
          }
        })
      ],
      providers: [
        HttpService,
        EnvironmentService,
        HttpClient,
        HttpHandler,
        LanguageService,
        ToastsService,
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
      .compileComponents();
  }));

  it('should create ProposalHomeComponent', () => {
    const fixture = TestBed.createComponent(ProposalHomeComponent);
    const component = fixture.componentInstance;
    expect(component).toBeDefined();
  });
});
