import {
  async,
  ComponentFixture,
  TestBed
} from '@angular/core/testing';

import { ProposalCreateComponent } from './proposal-create.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import {
  FormsModule,
  ReactiveFormsModule
} from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { EnvironmentService } from '../../core/services/environment/environment.service';
import { MaterialModule } from '../../catalogue/material.module';
import {
  Component,
  CUSTOM_ELEMENTS_SCHEMA,
  OnDestroy,
  OnInit
} from '@angular/core';
import {
  HttpClient,
  HttpClientModule
} from '@angular/common/http';
import {
  TranslateLoader,
  TranslateModule,
  TranslateService
} from '@ngx-translate/core';
import { LanguageService } from '../../core/services/language/language.service';
import { createTranslationLoader } from '../../core/services/language/translation.loader';
import { ProposalCreateService } from './proposal-create.service';
import { ToastsService } from '../../core/services/toasts/toasts-service';
import { UserService } from '../../core/services/adminTools/user.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ProposalService } from '../../core/services/adminTools/proposal.service';
import { HttpService } from '../../core/services/http/http.service';
import { HttpServiceImpl } from '../../core/services/http/http.service.impl';
import { Store } from '@ngrx/store';
import { EnvironmentServiceImpl } from '../../core/services/environment/environment-impl.service';
import { UniversityService } from '../../core/services/adminTools/university.service';
import { ProposalNameValidator } from '../../core/validation/proposal-name.validator';
import { InstituteService } from '../../core/services/adminTools/institute.service';
import { ApplicationNameValidator } from '../../core/validation/application-name.validator';
import { TechnologyService } from '../../core/services/adminTools/technology.service';
import { ApplicationService } from '../../core/services/adminTools/application.service';
import { ApplicationNameValidatorMock } from '../../unittest/mocks/application-name.validator.mock';
import { TechnologyServiceMock } from '../../unittest/mocks/technology.service.mock';
import { InstituteServiceMock } from '../../unittest/mocks/institute.service.mock';
import { UniversityServiceMock } from '../../unittest/mocks/university.service.mock';
import { ProposalNameValidatorMock } from '../../unittest/mocks/proposal-name.validator.mock';
import { ProposalTypeServiceMock } from '../../unittest/mocks/proposal-type.service.mock';
import { ProposalTypeService } from '../../core/services/adminTools/proposal-type.service';
import { StoreMock } from '../../unittest/mocks/store.mock';
import { ToastServiceMock } from '../../unittest/mocks/toast.service.mock';
import { TranslateServiceMock } from '../../unittest/mocks/translate.service.mock';
import { UserMockData } from '../../unittest/mock-data/user-mock-data';
import { User } from '../../core/domain/user';


describe('ProposalCreateComponent', () => {

  let component: ProposalCreateComponent;
  let fixture: ComponentFixture<ProposalCreateComponent>;
  let fixtureMock: ComponentFixture<ProposalCreateTestComponent>;

  const compSelectorAppPersonalDetails = `app-personal-details`;
  const compSelectorAppProposalDetails = `app-proposal-details`;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ProposalCreateComponent,
        ProposalCreateTestComponent,
      ],
      imports: [
        RouterModule,
        MaterialModule,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule,
        HttpClientModule,
        HttpClientTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: (createTranslationLoader),
            deps: [HttpClient]
          }
        })
      ],
      providers: [
        EnvironmentService,
        LanguageService,
        ProposalCreateTestComponent,
        ToastsService,
        {provide: Store, useClass: StoreMock},
        {provide: HttpService, useClass: HttpServiceImpl},
        {provide: UserService, useClass: UserServiceMock},
        {provide: ToastsService, useClass: ToastServiceMock},
        {provide: TranslateService, useClass: TranslateServiceMock},
        {provide: ProposalService, useClass: ProposalService},
        {provide: ProposalCreateService, useClass: ProposalCreateService},
        {provide: ProposalTypeService, useClass: ProposalTypeServiceMock},
        {provide: TechnologyService, useClass: TechnologyServiceMock},
        {provide: InstituteService, useClass: InstituteServiceMock},
        {provide: UniversityService, useClass: UniversityServiceMock},
        {provide: ApplicationNameValidator, useClass: ApplicationNameValidatorMock},
        {provide: ProposalNameValidator, useClass: ProposalNameValidatorMock},
        {provide: ApplicationService, useClass: ApplicationService},
        {provide: EnvironmentService, useClass: EnvironmentServiceImpl},
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
      .compileComponents();
  }));

  it('should create ProposalCreateComponent', () => {
    fixture = TestBed.createComponent(ProposalCreateComponent);
    component = fixture.componentInstance;
    component.ngOnInit();
    expect(component).toBeDefined();
    expect(component).toBeTruthy();
  });

  it('should create a ProposalCreateMock component with app-personal-details and app-proposal-details components', () => {

    // const element: HTMLElement = fixture.nativeElement;

    // const appPersonalDetails = element.querySelector(compSelectorAppPersonalDetails);
    // const appProposalDetails = element.querySelector(compSelectorAppProposalDetails);

    // expect(appPersonalDetails).toBeDefined();
    // expect(appProposalDetails).toBeDefined();
    // */
  });
  it('should create a ProposalCreate component with app-personal-details and app-proposal-details components', () => {
    fixture = TestBed.createComponent(ProposalCreateComponent);
    const element: HTMLElement = fixture.nativeElement;

    const appPersonalDetails = element.querySelector(compSelectorAppPersonalDetails);
    const appProposalDetails = element.querySelector(compSelectorAppProposalDetails);

    expect(appPersonalDetails).toBeDefined();
    expect(appProposalDetails).toBeDefined();

  });


  it('should call the onInit() method and instantiate the forms ', () => {
    let proposalCreateService: ProposalCreateService;
    proposalCreateService = TestBed.get(ProposalCreateService);
    proposalCreateService.newEmptyDeleteProposalDetailsForm();
    fixtureMock = TestBed.createComponent(ProposalCreateTestComponent);
    // fixtureMock.detectChanges();
    const componentMock = fixtureMock.componentInstance;
    spyOn(componentMock, 'ngOnInit');
    componentMock.ngOnInit();

    spyOn(componentMock, 'onStepComplete');
    componentMock.onStepComplete(true);
    expect(componentMock.onStepComplete).toHaveBeenCalled();
  });

});

// ----test components----- //
@Component({
  template: `
    <app-proposal-create></app-proposal-create>
  `
})
class ProposalCreateTestComponent implements OnInit, OnDestroy {

  ngOnInit() {}

  onStepComplete(value: boolean) {}

  onPreviousStep(value: boolean) {}

  onSubmitCreateProposal() {}

  onSubmitDeleteProposal() {}

  ngOnDestroy() {}
}

class UserServiceMock {
  getUser(): User {
    return UserMockData.user1;
  }
}


