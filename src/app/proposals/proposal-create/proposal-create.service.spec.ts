import { TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { Store } from '@ngrx/store';
import { TechnologyService } from '../../core/services/adminTools/technology.service';
import { UniversityService } from '../../core/services/adminTools/university.service';
import { ProposalTypeService } from '../../core/services/adminTools/proposal-type.service';
import { ProposalNameValidator } from '../../core/validation/proposal-name.validator';
import { ApplicationService } from '../../core/services/adminTools/application.service';
import { ApplicationNameValidator } from '../../core/validation/application-name.validator';
import { ProposalService } from '../../core/services/adminTools/proposal.service';
import { InstituteService } from '../../core/services/adminTools/institute.service';
import { ProposalCreateService } from './proposal-create.service';
import { HttpService } from '../../core/services/http/http.service';
import { HttpServiceImpl } from '../../core/services/http/http.service.impl';
import { HttpClientModule } from '@angular/common/http';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { EnvironmentService } from '../../core/services/environment/environment.service';
import { EnvironmentServiceImpl } from '../../core/services/environment/environment-impl.service';
import { UserService } from '../../core/services/adminTools/user.service';
import { SelectItem } from '../../core/domain/select.item';
import { ToastsService } from '../../core/services/toasts/toasts-service';
import {
  Overlay,
  ScrollDispatcher,
  ScrollStrategyOptions
} from '@angular/cdk/overlay';
import {
  MatSnackBar,
  MatSnackBarModule
} from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { ApplicationNameValidatorMock } from '../../unittest/mocks/application-name.validator.mock';
import { ProposalNameValidatorMock } from '../../unittest/mocks/proposal-name.validator.mock';
import { ProposalTypeServiceMock } from '../../unittest/mocks/proposal-type.service.mock';
import { InstituteServiceMock } from '../../unittest/mocks/institute.service.mock';
import { TechnologyServiceMock } from '../../unittest/mocks/technology.service.mock';
import { UniversityServiceMock } from '../../unittest/mocks/university.service.mock';
import { TranslateServiceMock } from '../../unittest/mocks/translate.service.mock';
import { StoreMock } from '../../unittest/mocks/store.mock';
import { InstituteMockData } from '../../unittest/mock-data/institute-mock-data';
import { User } from '../../core/domain/user';
import { UserMockData } from '../../unittest/mock-data/user-mock-data';

describe('proposal-create.service', () => {

  let store: StoreMock;
  let proposalCreateService: ProposalCreateService;
  let proposalService: ProposalService;
  let proposalTypeService: ProposalTypeService;
  let technologyService: TechnologyService;
  let instituteService: InstituteService;
  let universityService: UniversityService;
  let applicationNameValidator: ApplicationNameValidator;
  let proposalNameValidator: ProposalNameValidator;
  let applicationService: ApplicationService;
  let environmenService: EnvironmentService;
  let userService: UserService;

  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        MatSnackBarModule,
        HttpClientTestingModule
      ],
      providers: [
        FormBuilder,
        ProposalCreateService,
        {provide: Store, useClass: StoreMock},
        {provide: ToastsService, useClass: ToastsService},
        {provide: MatSnackBar, useClass: MatSnackBar},
        {provide: Overlay, useClass: Overlay},
        {provide: ScrollStrategyOptions, useClass: ScrollStrategyOptions},
        {provide: ScrollDispatcher, useClass: ScrollDispatcher},
        {provide: HttpService, useClass: HttpServiceImpl},
        {provide: ProposalService, useClass: ProposalService},
        {provide: ProposalTypeService, useClass: ProposalTypeServiceMock},
        {provide: TechnologyService, useClass: TechnologyServiceMock},
        {provide: InstituteService, useClass: InstituteServiceMock},
        {provide: UniversityService, useClass: UniversityServiceMock},
        {provide: ApplicationNameValidator, useClass: ApplicationNameValidatorMock},
        {provide: ProposalNameValidator, useClass: ProposalNameValidatorMock},
        {provide: ApplicationService, useClass: ApplicationService},
        {provide: EnvironmentService, useClass: EnvironmentServiceImpl},
        {provide: ProposalCreateService, useClass: ProposalCreateService},
        {provide: UserService, useClass: UserService},
        {provide: TranslateService, useClass: TranslateServiceMock},

      ]
    });
    proposalCreateService = TestBed.get(ProposalCreateService);
    proposalService = TestBed.get(ProposalService);
    proposalTypeService = TestBed.get(ProposalTypeService);
    technologyService = TestBed.get(TechnologyService);
    instituteService = TestBed.get(InstituteService);
    universityService = TestBed.get(UniversityService);
    applicationNameValidator = TestBed.get(ApplicationNameValidator);
    proposalNameValidator = TestBed.get(ProposalNameValidator);
    applicationService = TestBed.get(ApplicationService);
    environmenService = TestBed.get(EnvironmentService);
    userService = TestBed.get(UserService);

    httpTestingController = TestBed.get(HttpTestingController);
    store = TestBed.get(Store);

  });


  it('should create new empty proposal details form', () => {
    const form = proposalCreateService.newEmptyNewProposalDetailsForm();
    expect(form.get('purpose').dirty).toBe(false, 'field purpose should not be dirty after initialization');
    expect(form.get('purpose').validator).not.toBeNull();
    expect(form.get('proposalName').dirty).toBe(false, 'field proposalName should not be dirty after initialization');
    expect(form.get('proposalName').validator).not.toBeNull();
    expect(form.get('admin_email').dirty).toBe(false, 'field admin_email should not be dirty after initialization');
    expect(form.get('admin_email').validator).not.toBeNull();
    expect(form.get('proposalType').dirty).toBe(false, 'field proposalType should not be dirty after initialization');
    expect(form.get('proposalType').validator).not.toBeNull();
    expect(form.get('technology').dirty).toBe(false, 'field technology should not be dirty after initialization');
    expect(form.get('technology').validator).not.toBeNull();
  });

  it('should create new empty delete proposal form', () => {
    const form = proposalCreateService.newEmptyDeleteProposalDetailsForm();
    expect(form.get('purpose').dirty).toBe(false, 'field purpose should not be dirty after initialization');
    expect(form.get('purpose').validator).not.toBeNull();
    expect(form.get('proposalName').dirty).toBe(false, 'field proposalName should not be dirty after initialization');
    expect(form.get('proposalName').validator).not.toBeNull();
  });

  it('should create new empty proposal details form', () => {
    const form = proposalCreateService.newEmptyPersonDetailsForm();
    expect(form.get('firstName').dirty).toBe(false, 'field firstName should not be dirty after initialization');
    expect(form.get('firstName').validator).not.toBeNull();
    expect(form.get('firstName').disabled).toBeTruthy();
    expect(form.get('lastName').dirty).toBe(false, 'field lastName should not be dirty after initialization');
    expect(form.get('lastName').validator).not.toBeNull();
    expect(form.get('lastName').disabled).toBeTruthy();
    expect(form.get('email').dirty).toBe(false, 'field email should not be dirty after initialization');
    expect(form.get('email').validator).not.toBeNull();
    expect(form.get('email').disabled).toBeTruthy();
    expect(form.get('phone').dirty).toBe(false, 'field phone should not be dirty after initialization');
    expect(form.get('phone').validator).not.toBeNull();
    expect(form.get('phone').disabled).toBeTruthy();
    expect(form.get('university').dirty).toBe(false, 'field university should not be dirty after initialization');
    expect(form.get('university').validator).not.toBeNull();
    expect(form.get('institute').dirty).toBe(false, 'field institute should not be dirty after initialization');
    expect(form.get('institute').validator).not.toBeNull();
  });

  it('should intialize the personal form with values', () => {

    const form = proposalCreateService.newEmptyPersonDetailsForm();
    const formNew = proposalCreateService.initializePersonalDetailFormWithValues(form, UserMockData.user1);
    expect(formNew.get('firstName').value).toBe('Arjan', 'field firstName should be `Arjan`after initialization');
    expect(formNew.get('firstName').validator).not.toBeNull();
    expect(formNew.get('firstName').disabled).toBeTruthy();
    expect(formNew.get('lastName').value).toBe('van Doesburg', 'field lastName should be `van Doesburg`after initialization');
    expect(formNew.get('lastName').validator).not.toBeNull();
    expect(formNew.get('lastName').disabled).toBeTruthy();
    expect(formNew.get('email').value).toBe('arjan.vandoesburg@fhnw.ch',
      'field email should be `arjan.vandoesburg@fhnw.ch`after initialization');
    expect(formNew.get('email').validator).not.toBeNull();
    expect(formNew.get('email').disabled).toBeTruthy();
    expect(formNew.get('phone').value).toBe('+41 56 200 00 00',
      'field phone should be `+41 56 200 00 00` after initialization');
    expect(formNew.get('phone').validator).not.toBeNull();
    expect(formNew.get('phone').disabled).toBeTruthy();
    expect(formNew.get('university').validator).not.toBeNull();
    expect(formNew.get('institute').validator).not.toBeNull();
  });

  it('should build a list of technology select items', () => {
    const items: SelectItem[] = proposalCreateService.prepareTechnologySelectItems();
    expect(items[0]).isPrototypeOf(SelectItem.prototype);
    expect(items[0].key).toBe('key1');
    expect(items[1].key).toBe('key2');
  });

  it('should build a list of institute select items', () => {
    const items: SelectItem[] = proposalCreateService.prepareInstituteSelectItems();
    expect(items[0]).isPrototypeOf(SelectItem.prototype);
    expect(items[0].key).toBe('token');
    expect(items[1].key).toBe('token2');
  });

  it('should build a list of university select items', () => {
    const items: SelectItem[] = proposalCreateService.prepareUniversitySelectItems();
    expect(items[0]).isPrototypeOf(SelectItem.prototype);
    expect(items[0].key).toBe('token');
    expect(items[1].key).toBe('token2');
  });

  it('should build a list of proposal-type select items', () => {
    const items: SelectItem[] = proposalCreateService.prepareProposalTypeSelectItems();
    expect(items[0]).isPrototypeOf(SelectItem.prototype);
    expect(items[0].key).toBe('key');
    expect(items[1].key).toBe('key2');
  });

  it('should re-build a list of institute select items', () => {
    const items: SelectItem[] = proposalCreateService.rebuildInstituteSelectItems(InstituteMockData.getMockInstitutes());
    expect(items[0]).isPrototypeOf(SelectItem.prototype);
    expect(items[0].key).toBe('key');
    expect(items[1].key).toBe('key2');
  });

  it('should validate the dirty person form', () => {
    const personForm = proposalCreateService.newEmptyPersonDetailsForm();
    const personFormUpdated = proposalCreateService.initializePersonalDetailFormWithValues(personForm,
      new User());
    expect(proposalCreateService.isPersonFormDirty(personFormUpdated)).toBe(true);
  });

  it('should validate the dirty new-proposal form', () => {
    const proposalForm = proposalCreateService.newEmptyNewProposalDetailsForm();
    proposalForm.get('purpose').setValue('test purpose');
    proposalForm.get('proposalName').setValue('testname');
    proposalForm.get('admin_email').setValue('test@mail.com');
    proposalForm.get('proposalType').setValue('proposalType');
    proposalForm.get('technology').setValue('technology');
    expect(proposalCreateService.isNewProposalFormDirty(proposalForm)).toBe(true);
  });

  it('should validate the dirty delete-proposal form', () => {
    const proposalForm = proposalCreateService.newEmptyDeleteProposalDetailsForm();
    proposalForm.get('purpose').setValue('test purpose');
    proposalForm.get('proposalName').setValue('testname');
    expect(proposalCreateService.isDeleteProposalFormDirty(proposalForm)).toBe(true);
  });
});







