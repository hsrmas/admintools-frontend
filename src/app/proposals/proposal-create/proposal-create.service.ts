import '../../core/rx-js-exstensions';
import { Injectable } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { Proposal } from '../../core/domain/proposal';
import { SelectItem } from '../../core/domain/select.item';
import { TechnologyService } from '../../core/services/adminTools/technology.service';
import { Technology } from '../../core/domain/technology';
import { UserTemp } from '../../core/domain/userTemp';
import { domainNameValidator } from '../../core/validation/domain-name.validator';
import { ProposalTypeService } from '../../core/services/adminTools/proposal-type.service';
import { ProposalType } from '../../core/domain/proposal-type';
import { InstituteService } from '../../core/services/adminTools/institute.service';
import { UniversityService } from '../../core/services/adminTools/university.service';
import { University } from '../../core/domain/university';
import { Institute } from '../../core/domain/institute';
import { ProposalService } from '../../core/services/adminTools/proposal.service';
import { ProposalCreate } from '../../core/domain/proposal-create';
import { Store } from '@ngrx/store';
import { AppState } from '../../core/statemanagement/app.state';
import {
  ProposalTypeSelector,
  TechnologySelector
} from '../../core/statemanagement/selectors/';
import { CONSTANTS } from '../../core/constants';
import { Subject } from 'rxjs/Subject';
import { ApplicationNameValidator } from '../../core/validation/application-name.validator';
import { ApplicationService } from '../../core/services/adminTools/application.service';
import { Application } from '../../core/domain/application';
import { ProposalNameValidator } from '../../core/validation/proposal-name.validator';
import { ToastsService } from '../../core/services/toasts/toasts-service';
import { TranslateService } from '@ngx-translate/core';
import { User } from '../../core/domain/user';

@Injectable()
export class ProposalCreateService {

  onCreateProposalCalled: Subject<any>;
  onDeleteProposalCalled: Subject<any>;

  constructor(private formBuilder: FormBuilder,
              private proposalService: ProposalService,
              private technologyService: TechnologyService,
              private proposalTypeService: ProposalTypeService,
              private instituteService: InstituteService,
              private universityService: UniversityService,
              private applicationNameValidator: ApplicationNameValidator,
              private proposalNameValidator: ProposalNameValidator,
              private applicationService: ApplicationService,
              private toastService: ToastsService,
              private translateService: TranslateService,
              private store: Store<AppState>) {

    this.onCreateProposalCalled = new Subject<boolean>();
    this.onDeleteProposalCalled = new Subject<boolean>();
  }

  onCreateProposal(): void {
    this.onCreateProposalCalled.next();
  }

  onDeleteProposal(): void {
    this.onDeleteProposalCalled.next();
  }

  /**
   * Fetch all technologies from store/backend and set select-items
   * @returns {SelectItem[]}
   */
  prepareTechnologySelectItems(): SelectItem[] {
    const technologies: SelectItem[] = [];
    this.technologyService.getAllTechnologies()
      .subscribe((technology: Technology[]) => {
        technology.forEach((ref: Technology) => {
          technologies.push(new SelectItem(ref.id, ref.key, ref.name));
        });
      });
    return technologies;
  }

  /**
   * Fetch all proposal_types from store/backend and set select-items
   * @returns {SelectItem[]}
   */
  prepareProposalTypeSelectItems(): SelectItem[] {
    const proposalTypes: SelectItem[] = [];
    this.proposalTypeService.getAllProposalTypes()
      .subscribe((proposalType: ProposalType[]) => {
        proposalType.forEach((ref: ProposalType) => {
          proposalTypes.push(new SelectItem(ref.id, ref.key.toString(), ref.name));
        });
      });
    return proposalTypes;
  }


  /**
   * Fetch all universities from store/backend and set select-items
   * @returns {SelectItem[]}
   */
  prepareUniversitySelectItems(): SelectItem[] {
    const universityItems: SelectItem[] = [];
    this.universityService.getAllUniversities()
      .subscribe((universities: University[]) => {
        universities.forEach((ref: University) => {
          universityItems.push(new SelectItem(ref.id, ref.token, ref.name));
        });
      });
    return universityItems;
  }

  /**
   * Fetch all institutes from store/backend and set select-items
   * @returns {SelectItem[]}
   */
  prepareInstituteSelectItems(): SelectItem[] {
    const instituteItems: SelectItem[] = [];
    this.instituteService.getAllInstitutes()
      .subscribe((institutes: Institute[]) => {
        institutes.forEach((ref: Institute) => {
          instituteItems.push(new SelectItem(ref.id, ref.token, ref.name));
        });
      });
    return instituteItems;
  }

  /**
   * Rebuild the institute select-items
   * @param {Institute[]} institutes
   * @returns {SelectItem[]}
   */
  rebuildInstituteSelectItems(institutes: Institute[]): SelectItem[] {
    const instituteItems: SelectItem[] = [];
    institutes.forEach((ref: Institute) => {
      instituteItems.push(new SelectItem(ref.id, ref.key, ref.name));
    });
    return instituteItems;
  }


  /**
   * Create a new Proposal-Create Object from
   * form-values for a 'New Proposal' action
   *
   * @param {Proposal} proposalForm
   * @returns {Observable<Proposal>}
   */
  mapNewProposalFormValuesToDTO(proposaslForm: FormGroup,
                                personalForm: FormGroup,
                                proposalNewForm: FormGroup, user: User): Observable<ProposalCreate> {

    let proposalTypeId;
    let technologyId;
    // find id of proposalTypeKey
    this.store.select(ProposalTypeSelector.findProposalTypeIdByKey(proposaslForm.get('proposalType').value))
      .subscribe((result: ProposalType) => {
        proposalTypeId = result.id;
      });
    // find id of the technologyKey
    this.store.select(TechnologySelector.findTechnologyIdByKey(proposalNewForm.get('technology').value))
      .subscribe((result: Technology) => {
        technologyId = result.id;
      });

    const newProposal = Object.assign(Object.create(ProposalCreate.prototype), {
      purpose: proposalNewForm.get('purpose').value,
      admin_email: proposalNewForm.get('admin_email').value,
      name: proposalNewForm.get('proposalName').value,
      owner_id: user.id,
      status_id: CONSTANTS.APPLICATION_STATE_NEW,
      technology_id: technologyId,
      proposal_type_id: proposalTypeId,
      institute_id: parseInt(personalForm.get('institute').value, 10),
      university_id: parseInt(personalForm.get('university').value, 10)
    });
    return Observable.of(newProposal);
  }

  /**
   * Create a new Proposal-Create Object from
   * form-values for a 'New Proposal' action
   *
   * @param {Proposal} proposalForm
   * @returns {Observable<Proposal>}
   */
  mapDeleteProposalFormValuesToDTO(proposalForm: FormGroup,
                                   personalForm: FormGroup,
                                   proposalDeleteForm: FormGroup, user: User): Observable<ProposalCreate> {

    let proposalTypeId;
    let proposal: Proposal;
    let applicationFound: Application;

    applicationFound = this.applicationService.getApplicationFromStoreByName(proposalDeleteForm.get('proposalName').value);
    if (applicationFound === null || applicationFound === undefined) {
      this.toastService.genericErrorToast(); // TODO: Maybe add a more descriptive error message
      return Observable.of(null);
    }
    proposal = this.proposalService.getProposalFromStoreByApplicationId(applicationFound.id);

    this.store.select(ProposalTypeSelector.findProposalTypeIdByKey(proposalForm.get('proposalType').value))
      .subscribe((result: ProposalType) => {
        proposalTypeId = result.id;
      });

    const newProposal = Object.assign(Object.create(ProposalCreate.prototype), {
      purpose: proposalDeleteForm.get('purpose').value,
      admin_email: proposal.admin_email,
      name: proposal.name,
      owner_id: user.id,
      status_id: CONSTANTS.APPLICATION_STATE_NEW,
      technology_id: proposal.technology_id,
      proposal_type_id: proposalTypeId,
      institute_id: parseInt(personalForm.get('institute').value, 10),
      university_id: parseInt(personalForm.get('university').value, 10)
    });

    return Observable.of(newProposal);
  }

  /**
   *
   * @param {Proposal} proposal
   * @returns {Observable<Proposal>}
   */
  createNewProposal(proposal: ProposalCreate): Observable<Proposal> {
    return this.proposalService.createNewProposal(proposal);
  }

  /**
   * Create a new Form for a 'New' Proposal action details
   * @returns {FormGroup}
   */
  newEmptyNewProposalDetailsForm(): FormGroup {
    return this.formBuilder.group({
      'purpose': ['', Validators.required], // TODO: Validator triggers only at the first character input....!!!
      'proposalName': ['', Validators.compose([Validators.required, domainNameValidator,
        this.validateForExistingApplicationAndProposalName.bind(this)])],
      'admin_email': ['', Validators.email],
      'proposalType': ['', Validators.required],
      'technology': ['', Validators.required],
    });
  }


  /**
   * Create a new Form for a 'Delete' Proposal action details
   * @returns {FormGroup}
   */
  newEmptyDeleteProposalDetailsForm(): FormGroup {
    return this.formBuilder.group({
      'purpose': ['', Validators.required], // TODO: Validator triggers only at the first character input....!!!
      'proposalName': ['', Validators.compose([Validators.required, domainNameValidator,
        this.validateForExistingApplicationName.bind(this)])]
    });
  }

  /**
   * Create new empty personal details form
   * @returns {FormGroup}
   */
  newEmptyPersonDetailsForm(): FormGroup {
    return this.formBuilder.group({
      // request details person
      'firstName': [{value: '', disabled: true}, Validators.required],
      'lastName': [{value: '', disabled: true}, Validators.required],
      'email': [{value: '', disabled: true}, Validators.email],
      'phone': [{value: '', disabled: true}, Validators.required],
      'university': ['', Validators.required],
      'institute': ['', Validators.required]
    });
  }

  /**
   * Create new empty proposal form
   * @returns {FormGroup}
   */
  newEmptyProposalForm(): FormGroup {
    return this.formBuilder.group({
      'proposalType': ['', Validators.required],
    });
  }

  /**
   * Validate for existing applications
   * @param {AbstractControl} control
   * @returns {{[p: string]: boolean}}
   */
  validateForExistingApplicationName(control: AbstractControl): { [value: string]: boolean } {
    let result;
    this.applicationNameValidator.validate(control).subscribe(res => {
      result = res;
    });
    return result ? {applicationNameTaken: true} : {applicationNameTaken: false};
  }

  /**
   *  Validate for both existing application and proposal names
   * @param {AbstractControl} control
   * @returns {{[p: string]: boolean}}
   */
  validateForExistingApplicationAndProposalName(control: AbstractControl): { [value: string]: boolean } {
    let resultApp;
    let resultProp;
    this.applicationNameValidator.validate(control).subscribe(res => {
      resultApp = res;
    });

    this.proposalNameValidator.validate(control).subscribe(res => {
      resultProp = res;
    });

    if (resultApp === true || resultProp === true) {
      return {applicationNameTaken: true};
    } else {
      return null;
    }
  }

  /**
   * Initialize personal detail form with user values
   * @param {UserTemp} user
   */
  initializePersonalDetailFormWithValues(form: FormGroup, user: User): FormGroup {
    form.controls['firstName'].patchValue(user.first_name);
    form.controls['lastName'].patchValue(user.last_name);
    form.controls['email'].patchValue(user.email);
    form.controls['phone'].patchValue('+41 56 200 00 00');
    // form.controls['university'].patchValue('FHNW');
    // form.controls['institution'].patchValue('SomeInstitute');
    return form;
  }

  /**
   * Custom implementation of form.dirty
   * @param formGroup
   * @returns {boolean}
   */
  isPersonFormDirty(formGroup: FormGroup): boolean {
    return ((formGroup.get('firstName').value && formGroup.get('firstName').value.trim().length > 0)
      || (formGroup.get('lastName').value && formGroup.get('lastName').value.trim().length > 0)
      || (formGroup.get('email').value && formGroup.get('email').value.trim().length > 0)
      || (formGroup.get('phone').value && formGroup.get('phone').value.trim().length > 0)
      || (formGroup.get('university').value && formGroup.get('university').value.trim().length > 0)
      || (formGroup.get('institute').value && formGroup.get('institute').value.trim().length > 0)
    );
  }

  /**
   * Check is proposal form is dirty
   * @param {FormGroup} formGroup
   * @returns {boolean}
   */
  isNewProposalFormDirty(formGroup: FormGroup): boolean {
    return ((formGroup.get('admin_email').value && formGroup.get('admin_email').value.trim().length > 0)
      || (formGroup.get('proposalName').value && formGroup.get('proposalName').value.trim().length > 0)
      || (formGroup.get('purpose').value && formGroup.get('purpose').value.trim().length > 0)
      || (formGroup.get('proposalType').value && formGroup.get('proposalType').value.trim().length > 0)
      || (formGroup.get('technology').value && formGroup.get('technology').value.trim().length > 0)
    );
  }

  /**
   * Check if delete proposal form is dirty
   * @param {FormGroup} formGroup
   * @returns {boolean}
   */
  isDeleteProposalFormDirty(formGroup: FormGroup): boolean {
    return ((formGroup.get('proposalName').value && formGroup.get('proposalName').value.trim().length > 0)
      || (formGroup.get('purpose').value && formGroup.get('purpose').value.trim().length > 0)
    );
  }
}
