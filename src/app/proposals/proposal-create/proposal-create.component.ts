import {
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output
} from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup
} from '@angular/forms';
import { ProposalCreateService } from './proposal-create.service';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { ToastsService } from '../../core/services/toasts/toasts-service';
import { UserService } from '../../core/services/adminTools/user.service';
import { Observable } from 'rxjs/Observable';
import { CONSTANTS } from '../../core/constants';
import { ProposalCreate } from '../../core/domain/proposal-create';
import { Router } from '@angular/router';
import { ToastData } from '../../core/services/toasts/toast-data';
import { ToastType } from '../../core/services/toasts/toast-type';
import { TranslateService } from '@ngx-translate/core';
import { Proposal } from '../../core/domain/proposal';

@Component({
  selector: 'app-proposal-create',
  templateUrl: './proposal-create.component.html',
  styleUrls: ['./proposal-create.component.scss']
})
export class ProposalCreateComponent implements OnInit, OnDestroy {

  proposalForm: FormGroup;
  proposalPersonDetailsForm: FormGroup;
  proposalNewProposalDetailsForm: FormGroup;
  proposalDeleteProposalDetailsForm: FormGroup;
  proposalName: string;
  proposalNameControl: AbstractControl;
  personPanelOpen: boolean;
  proposalPanelOpen: boolean;

  @Output() submitProposal = new EventEmitter<boolean>();

  private destroyed$: ReplaySubject<boolean> = new ReplaySubject<boolean>(1);

  constructor(private fb: FormBuilder,
              private proposalCreateService: ProposalCreateService,
              private toastService: ToastsService,
              private userService: UserService,
              private router: Router,
              private transtlateService: TranslateService,
  ) {
  }

  ngOnInit() {

    this.proposalName = CONSTANTS.EXAMPLE_PROPOSAL_NAME;
    this.proposalForm = this.proposalCreateService.newEmptyProposalForm();
    this.proposalPersonDetailsForm = this.proposalCreateService.newEmptyPersonDetailsForm();
    this.proposalNewProposalDetailsForm = this.proposalCreateService.newEmptyNewProposalDetailsForm();
    this.proposalDeleteProposalDetailsForm = this.proposalCreateService.newEmptyDeleteProposalDetailsForm();

    this.proposalCreateService.initializePersonalDetailFormWithValues(this.proposalPersonDetailsForm,
      this.userService.getUser());

    this.personPanelOpen = true;
    this.proposalPanelOpen = false;

    this.proposalNameControl = this.proposalNewProposalDetailsForm.controls['proposalName'];
    this.proposalNameControl.valueChanges.subscribe((value: string) => {
      this.proposalName = value;
    });

    // handle create event
    this.proposalCreateService.onCreateProposalCalled
      .takeUntil(this.destroyed$)
      .catch(() => {
        this.toastService.genericErrorToast();
        return Observable.empty();
      })
      .subscribe((response: any) => {
        if (this.proposalCreateService.isPersonFormDirty(this.proposalPersonDetailsForm) &&
          this.proposalCreateService.isNewProposalFormDirty(this.proposalNewProposalDetailsForm)) {
          this.proposalCreateService.mapNewProposalFormValuesToDTO(this.proposalForm,
            this.proposalPersonDetailsForm, this.proposalNewProposalDetailsForm, this.userService.getUser())
            .takeUntil(this.destroyed$)
            .switchMap((proposalRequest: ProposalCreate) => this.proposalCreateService.createNewProposal(proposalRequest))
            .catch(() => {
              this.router.navigate(['errorpage/']);
              this.toastService.genericErrorToast();
              return Observable.empty();
            })
            .subscribe((result: Proposal) => {
              this.router.navigate(['proposal-detail/' + result.id]);
              this.toastService.makeToast(new ToastData(this.transtlateService.instant('TOAST_MESSAGES.SUCCESS.CREATE_PROPOSAL.TITLE'),
                this.transtlateService.instant('TOAST_MESSAGES.SUCCESS.CREATE_PROPOSAL.MESSAGE', {antragname: result.name})
                , ToastType.success));
            });
        }
      });

    // handle delete event
    this.proposalCreateService.onDeleteProposalCalled
      .takeUntil(this.destroyed$)
      .catch(() => {
        this.toastService.genericErrorToast();
        return Observable.empty();
      })
      .subscribe((response: any) => {
        if (this.proposalCreateService.isPersonFormDirty(this.proposalPersonDetailsForm) &&
          this.proposalCreateService.isDeleteProposalFormDirty(this.proposalDeleteProposalDetailsForm)) {
          this.proposalCreateService.mapDeleteProposalFormValuesToDTO(this.proposalForm, this.proposalPersonDetailsForm,
            this.proposalDeleteProposalDetailsForm, this.userService.getUser())
            .takeUntil(this.destroyed$)
            .switchMap((proposalDeleteRequest: ProposalCreate) => this.proposalCreateService.createNewProposal(proposalDeleteRequest))
            .catch(() => {
              this.router.navigate(['errorpage/']);
              this.toastService.genericErrorToast();
              return Observable.empty();
            })
            .subscribe((result: Proposal) => {
              this.router.navigate(['proposal-detail/' + result.id]);
              this.toastService.makeToast(new ToastData(this.transtlateService.instant('TOAST_MESSAGES.SUCCESS.DELETE_PROPOSAL.TITLE'),
                this.transtlateService.instant('TOAST_MESSAGES.SUCCESS.DELETE_PROPOSAL.MESSAGE', {antragname: result.name})
                , ToastType.success));
            });
        }
      });
  }

  // toggle the expansion panel
  onStepComplete(onComplete: boolean): void {
    this.personPanelOpen = false;
    this.proposalPanelOpen = true;
  }

  // toggle the expansion panel
  onPreviousStep(onComplete: boolean): void {
    this.personPanelOpen = true;
    this.proposalPanelOpen = false;
  }

  // call createNewProposal event on service
  onSubmitCreateProposal(): void {
    this.proposalCreateService.onCreateProposal();
  }

  // call delete event on service
  onSubmitDeleteProposal(): void {
    this.proposalCreateService.onDeleteProposal();
  }

  ngOnDestroy(): void {
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }
}
