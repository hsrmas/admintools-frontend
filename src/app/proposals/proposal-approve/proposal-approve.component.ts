import {
  Component,
  OnInit
} from '@angular/core';
import { CONSTANTS } from '../../core/constants';

@Component({
  selector: 'app-proposal-approve',
  templateUrl: './proposal-approve.component.html',
  styleUrls: ['./proposal-approve.component.scss']
})
export class ProposalApproveComponent implements OnInit {

  origin: string = CONSTANTS.ORIGIN_MYTODOS;
  constructor() { }

  ngOnInit() {
  }

}
