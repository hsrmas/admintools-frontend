import {
  async,
  TestBed
} from '@angular/core/testing';

import { ProposalMyProposalsComponent } from './proposal-my-proposals.component';
import {
  TranslateLoader,
  TranslateModule
} from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import { ProposalCreateService } from '../proposal-create/proposal-create.service';
import {
  HttpClient,
  HttpHandler
} from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '../../catalogue/material.module';
import { createTranslationLoader } from '../../core/services/language/translation.loader';
import { ToastsService } from '../../core/services/toasts/toasts-service';
import { RouterModule } from '@angular/router';
import {
  FormsModule,
  ReactiveFormsModule
} from '@angular/forms';
import { EnvironmentService } from '../../core/services/environment/environment.service';
import { LanguageService } from '../../core/services/language/language.service';
import { HttpService } from '../../core/services/http/http.service';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('ProposalMyProposalsComponent', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProposalMyProposalsComponent],
      imports: [
        RouterModule,
        MaterialModule,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: (createTranslationLoader),
            deps: [HttpClient]
          }
        })
      ],
      providers: [
        HttpService, EnvironmentService, HttpClient, HttpHandler, LanguageService, ProposalCreateService, ToastsService
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
      .compileComponents();
  }));


  it('should create ProposalMyProposalsComponent', () => {
    const fixture = TestBed.createComponent(ProposalMyProposalsComponent);
    const component = fixture.componentInstance;
    expect(component).toBeTruthy();
  });
});
