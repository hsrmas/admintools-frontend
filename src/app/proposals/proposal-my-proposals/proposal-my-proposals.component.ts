import { Component } from '@angular/core';
import { CONSTANTS } from '../../core/constants';


@Component({
  selector: 'app-proposal-my-proposals',
  templateUrl: './proposal-my-proposals.component.html',
  styleUrls: ['./proposal-my-proposals.component.scss']
})
export class ProposalMyProposalsComponent {
  origin: string = CONSTANTS.ORIGIN_MYPROPOSALS;
}
