import { Component } from '@angular/core';
import { CONSTANTS } from '../../core/constants';

@Component({
  selector: 'app-proposal-admin',
  templateUrl: './proposal-admin.component.html',
  styleUrls: ['./proposal-admin.component.scss']
})
export class ProposalAdminComponent {
  origin: string = CONSTANTS.ORIGIN_ADMIN;
}
