import {
  Component,
  Input,
  OnInit,
  ViewChild
} from '@angular/core';
import { LanguageService } from '../../../core/services/language/language.service';
import { MatSidenav } from '@angular/material';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @ViewChild('sideNav', {read: MatSidenav}) sideNav: MatSidenav;
  navMode = 'side';

  supportedLanguages: string[];
  selectedLanguage: string;
  @Input() loggedInUser: string;


  constructor(private languageService: LanguageService) {
  }

  ngOnInit() {
    this.supportedLanguages = this.languageService.supportedLanguages;
    this.selectedLanguage = this.languageService.getDefaultLanguage();
  }

  /**
   * Change language
   * @param {string} value
   */
  changeLanguage(value: string) {
    if (this.languageService.supportedLanguages.find(lang => lang === value)) {
      this.languageService.switchLanguage(value);
    }
  }

}
