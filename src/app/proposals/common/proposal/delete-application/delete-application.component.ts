import {
  Component,
  Input,
  OnInit
} from '@angular/core';
import {
  AbstractControl,
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';
import { ProposalCreateService } from '../../../proposal-create/proposal-create.service';

import { Application } from '../../../../core/domain/application';
import { Observable } from 'rxjs/Observable';
import {
  map,
  startWith
} from 'rxjs/operators';
import { ApplicationService } from '../../../../core/services/adminTools/application.service';
import { ApplicationNameValidator } from '../../../../core/validation/application-name.validator';

@Component({
  selector: 'app-delete-application',
  templateUrl: './delete-application.component.html',
  styleUrls: ['./delete-application.component.scss']
})
export class DeleteApplicationComponent implements OnInit {


  @Input() proposalDeleteProposalDetailsForm: FormGroup;
  applicationNameCtrl: FormControl;
  filteredApps: Observable<any[]>;
  applications: Application[];

  constructor(private proposalCreateService: ProposalCreateService,
              private applicationNameValidator: ApplicationNameValidator,
              private applicationService: ApplicationService) {
  }

  ngOnInit() {
    this.applicationNameCtrl = new FormControl('', Validators.compose(
      [Validators.required, this.validateForExistingApplicationName.bind(this)]));

    this.applicationService.getAllApplications()
      .subscribe((applications: Application[]) => {
        if (applications) {
          this.applications = applications;
        }
      });

    this.filteredApps = this.applicationNameCtrl.valueChanges
      .pipe(
        startWith(''),
        map(searchString => searchString ? this.filterApps(searchString) : this.applications.slice()));
  }

  /**
   * Bind the applicationNameCtrl to the custom validator
   * @param {AbstractControl} control
   * @returns {{[p: string]: boolean}}
   */
  validateForExistingApplicationName(control: AbstractControl): { [value: string]: boolean } {
    let result;
    this.applicationNameValidator.validate(control).subscribe(res => {
      result = res;
    });
    if (result === true) {
      this.proposalDeleteProposalDetailsForm.get('proposalName').patchValue(control.value);
      return null;
    } else {
      return {applicationNameExisting: true};
    }
  }

  private filterApps(searchString: string) {
    return this.applications.filter(app =>
      app.name.toLowerCase().indexOf(searchString.toLowerCase()) === 0);
  }


}
