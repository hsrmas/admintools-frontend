import {
  async,
  TestBed
} from '@angular/core/testing';

import { DeleteApplicationComponent } from './delete-application.component';
import { MaterialModule } from '../../../../catalogue/material.module';
import {
  FormsModule,
  ReactiveFormsModule
} from '@angular/forms';
import {
  TranslateLoader,
  TranslateModule
} from '@ngx-translate/core';
import {
  HttpClient,
  HttpHandler
} from '@angular/common/http';
import { createTranslationLoader } from '../../../../core/services/language/translation.loader';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ToastsService } from '../../../../core/services/toasts/toasts-service';
import { RouterModule } from '@angular/router';
import { LanguageService } from '../../../../core/services/language/language.service';
import { ProposalCreateServiceMock } from '../../../../unittest/mocks/proposal-create.service.mock';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpService } from '../../../../core/services/http/http.service';
import { EnvironmentService } from '../../../../core/services/environment/environment.service';
import { ProposalCreateService } from '../../../proposal-create/proposal-create.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ApplicationService } from '../../../../core/services/adminTools/application.service';
import { ApplicationServiceMock } from '../../../../unittest/mocks/application.service.mock';
import { ApplicationNameValidator } from '../../../../core/validation/application-name.validator';
import { ApplicationNameValidatorMock } from '../../../../unittest/mocks/application-name.validator.mock';

describe('DeleteApplicationComponent', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DeleteApplicationComponent],
      imports: [
        RouterModule,
        MaterialModule,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: (createTranslationLoader),
            deps: [HttpClient]
          }
        })
      ],
      providers: [
        HttpService,
        EnvironmentService,
        HttpClient,
        HttpHandler,
        LanguageService,
        ToastsService,
        {provide: ProposalCreateService, useClass: ProposalCreateServiceMock},
        {provide: ApplicationService, useClass: ApplicationServiceMock},
        {provide: ApplicationNameValidator, useClass: ApplicationNameValidatorMock},
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
      .compileComponents();
  }));
  it('should create DeleteApplicationComponent', () => {
    const fixture = TestBed.createComponent(DeleteApplicationComponent);
    const component = fixture.componentInstance;
    expect(component).toBeDefined();
  });
});
