import {
  async,
  TestBed
} from '@angular/core/testing';

import { NewApplicationComponent } from './new-application.component';
import { ToastsService } from '../../../../core/services/toasts/toasts-service';
import { RouterModule } from '@angular/router';
import {
  FormsModule,
  ReactiveFormsModule
} from '@angular/forms';
import { LanguageService } from '../../../../core/services/language/language.service';
import { ProposalCreateServiceMock } from '../../../../unittest/mocks/proposal-create.service.mock';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpService } from '../../../../core/services/http/http.service';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {
  HttpClient,
  HttpHandler
} from '@angular/common/http';
import {
  TranslateLoader,
  TranslateModule
} from '@ngx-translate/core';
import { EnvironmentService } from '../../../../core/services/environment/environment.service';
import { MaterialModule } from '../../../../catalogue/material.module';
import { ProposalCreateService } from '../../../proposal-create/proposal-create.service';
import { createTranslationLoader } from '../../../../core/services/language/translation.loader';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('NewApplicationComponent', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NewApplicationComponent],
      imports: [
        RouterModule,
        MaterialModule,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: (createTranslationLoader),
            deps: [HttpClient]
          }
        })
      ],
      providers: [
        HttpService,
        EnvironmentService,
        HttpClient,
        HttpHandler,
        LanguageService,
        ToastsService,
        {provide: ProposalCreateService, useClass: ProposalCreateServiceMock},
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
      .compileComponents();
  }));

  it('should create NewApplicationComponent', () => {
    const fixture = TestBed.createComponent(NewApplicationComponent);
    const component = fixture.componentInstance;
    expect(component).toBeDefined();
  });
});
