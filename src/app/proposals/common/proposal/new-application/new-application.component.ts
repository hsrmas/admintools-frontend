import {
  Component,
  Input,
  OnInit
} from '@angular/core';
import {
  AbstractControl,
  FormGroup
} from '@angular/forms';
import { SelectItem } from '../../../../core/domain/select.item';
import { ProposalCreateService } from '../../../proposal-create/proposal-create.service';
import { CONSTANTS } from '../../../../core/constants';

@Component({
  selector: 'app-new-application',
  templateUrl: './new-application.component.html',
  styleUrls: ['./new-application.component.scss']
})
export class NewApplicationComponent implements OnInit {

  @Input() proposalProposalDetailsForm: FormGroup;

  proposalName: string;
  proposalNameControl: AbstractControl;
  technologies: SelectItem[];

  constructor(private proposalCreateService: ProposalCreateService) {
  }

  ngOnInit() {

    this.proposalName = CONSTANTS.DEFAULT_PROPOSAL_NAME;
    this.technologies = this.proposalCreateService.prepareTechnologySelectItems();

    this.proposalNameControl = this.proposalProposalDetailsForm.controls['proposalName'];
    this.proposalNameControl.valueChanges.subscribe((value: string) => {
        this.proposalName = value;
      }
    );
  }

}
