import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
} from '@angular/core';
import {
  AbstractControl,
  FormGroup
} from '@angular/forms';
import { SelectItem } from '../../../../core/domain/select.item';
import { ProposalCreateService } from '../../../proposal-create/proposal-create.service';
import { University } from '../../../../core/domain/university';
import {
  InstituteSelector,
  UniversitySelector
} from '../../../../core/statemanagement/selectors/index';
import { Institute } from '../../../../core/domain/institute';
import { AppState } from '../../../../core/statemanagement/app.state';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-personal-details',
  templateUrl: './personal-details.component.html',
  styleUrls: ['./personal-details.component.scss']
})
export class PersonalDetailsComponent implements OnInit {

  @Input() proposalPersonDetailsForm: FormGroup;
  @Input() personPanelOpen: boolean;
  @Input() proposalPanelOpen: boolean;
  @Output() stepComplete = new EventEmitter<boolean>();

  instituteTypes: SelectItem[];
  universityTypes: SelectItem[];
  universityControl: AbstractControl;
  universitySelected: number;

  constructor(private proposalCreateService: ProposalCreateService,
              private store: Store<AppState>) {
  }

  ngOnInit() {

    let universityURL: string;

    this.universityTypes = this.proposalCreateService.prepareUniversitySelectItems();
    this.proposalCreateService.prepareInstituteSelectItems();


    // react to changes on the university select option
    this.universityControl = this.proposalPersonDetailsForm.controls['university'];
    this.universityControl.valueChanges.subscribe((key: number) => {
      this.universitySelected = key;
      // find university by key (university.id)
      this.store.select(UniversitySelector.findUniversityById(this.universitySelected)).subscribe((university: University) => {
        universityURL = university.url;
      });
      // find mapped institutions of prev. selected university and rebuild the institution select options
      this.store.select(InstituteSelector.findInstitutesByUniversity(universityURL)).subscribe((result: Institute[]) => {
        this.instituteTypes = this.proposalCreateService.rebuildInstituteSelectItems(result);
      });
    });
  }

  /**
   * Forward to proposal deails if personal form is valid
   */
  nextStep() {
    if (this.proposalPersonDetailsForm.valid) {
      if (this.proposalCreateService.isPersonFormDirty(this.proposalPersonDetailsForm)) {
        this.stepComplete.emit(true);
      }
    }
  }
}
