export interface RejectDialogData {
  reason: string;
  isCancel: boolean;
}
