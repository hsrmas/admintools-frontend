import {
  Component,
  Inject,
  OnInit
} from '@angular/core';
import {
  MAT_DIALOG_DATA,
  MatDialogRef
} from '@angular/material';
import { RejectDialogData } from './reject-dialog.data';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';

@Component({
  selector: 'app-reject-dialog',
  templateUrl: './reject-dialog.component.html',
  styleUrls: ['./reject-dialog.component.scss']
})
export class RejectDialogComponent implements OnInit {

  rejectForm: FormGroup;
  reasonInputControl: AbstractControl;

  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<RejectDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: RejectDialogData) {
  }

  onCancel(): void {
    this.dialogRef.close(true);
  }

  onReject(): void {
    this.dialogRef.close(this.rejectForm.value);
  }

  ngOnInit() {
    this.createForm();
    this.reasonInputControl = this.rejectForm.controls['reason'];
    this.reasonInputControl.valueChanges.subscribe((value: string) => {
      this.data.reason = value;
    });
  }

  private createForm(): void {
    this.rejectForm = this.fb.group({
      reason: ['', Validators.required]
    });
  }

}
