import {
  async,
  ComponentFixture,
  TestBed
} from '@angular/core/testing';

import { RejectDialogComponent } from './reject-dialog.component';
import { UserService } from '../../../../core/services/adminTools/user.service';
import {
  FormsModule,
  ReactiveFormsModule
} from '@angular/forms';
import { LanguageService } from '../../../../core/services/language/language.service';
import { MaterialModule } from '../../../../catalogue/material.module';
import { HttpService } from '../../../../core/services/http/http.service';
import {
  HttpClient,
  HttpHandler
} from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { ToastsService } from '../../../../core/services/toasts/toasts-service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { createTranslationLoader } from '../../../../core/services/language/translation.loader';
import {
  TranslateLoader,
  TranslateModule
} from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import { EnvironmentService } from '../../../../core/services/environment/environment.service';
import {
  MatDialogModule,
  MatDialogRef
} from '@angular/material';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

xdescribe('RejectDialogComponent', () => {
  let component: RejectDialogComponent;
  let fixture: ComponentFixture<RejectDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RejectDialogComponent],
      imports: [
        RouterModule,
        MaterialModule,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule,
        MatDialogModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: (createTranslationLoader),
            deps: [HttpClient]
          }
        })
      ],
      providers: [
        HttpService,
        EnvironmentService,
        HttpClient,
        HttpHandler,
        LanguageService,
        ToastsService,
        UserService,
        {provide: MatDialogRef, useValue: {}},
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RejectDialogComponent);
    component = fixture.componentInstance;
    // fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
