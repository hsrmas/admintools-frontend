import { Injectable } from '@angular/core';
import { ProposalService } from '../../../../core/services/adminTools/proposal.service';
import { Proposal } from '../../../../core/domain/proposal';
import { Observable } from 'rxjs/Observable';
import { CONSTANTS } from '../../../../core/constants';

@Injectable()
export class ProposalTableService {

  constructor(private proposalService: ProposalService) {
  }

  /**
   * Depending on the navigation origin we load different proposals
   * (filtered by the user-credentials in the backend)
   * @param {string} origin
   * @returns {Observable<Proposal[]>}
   */
  initTable(origin: string): Observable<Proposal[]> {
    switch (origin) {
      case CONSTANTS.ORIGIN_MYPROPOSALS:
        return this.proposalService.getAllMyProposals();
      case CONSTANTS.ORIGIN_MYTODOS:
        return this.proposalService.getAllMyTodos();
      case CONSTANTS.ORIGIN_ADMIN:
        return this.proposalService.getAllProposals();
      default:
        return this.proposalService.getAllProposals();
    }
  }
}
