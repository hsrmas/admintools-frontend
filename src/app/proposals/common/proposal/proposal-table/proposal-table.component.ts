import {
  Component,
  Input,
  OnInit,
  ViewChild
} from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import {
  MatPaginator,
  MatSort,
  MatTableDataSource
} from '@angular/material';
import { Proposal } from '../../../../core/domain/proposal';
import { ProposalTableService } from './proposal-table.service';

@Component({
  selector: 'app-proposal-table',
  templateUrl: './proposal-table.component.html',
  styleUrls: ['./proposal-table.component.scss']
})
export class ProposalTableComponent implements OnInit {

  @Input() origin: string;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  displayedColumns = ['id', 'name', 'proposal_type', 'technology_id', 'status', 'action'];
  dataSource = new MatTableDataSource<Proposal>();

  selection = new SelectionModel<Proposal>(true, []);
  resultsLength: number;
  pageSize: number;
  isLoadingResults = true;
  isRateLimitReached = false;

  constructor(private proposalTableService: ProposalTableService) {
  }

  ngOnInit() {
    this.resultsLength = 0;
    this.pageSize = 10;
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    this.proposalTableService.initTable(this.origin)
      .map((proposal: Proposal[]) => this.dataSource = new MatTableDataSource(proposal))
      .subscribe();
  }

  applyFilter(filterValue: string): void {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();

    this.dataSource.filter = filterValue;
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
