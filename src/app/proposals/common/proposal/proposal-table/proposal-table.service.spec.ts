import { UserService } from '../../../../core/services/adminTools/user.service';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { HttpServiceImpl } from '../../../../core/services/http/http.service.impl';
import { ProposalService } from '../../../../core/services/adminTools/proposal.service';
import {
  MatSnackBar,
  MatSnackBarModule
} from '@angular/material';
import { HttpService } from '../../../../core/services/http/http.service';
import {
  Overlay,
  ScrollDispatcher,
  ScrollStrategyOptions
} from '@angular/cdk/overlay';
import { TranslateService } from '@ngx-translate/core';
import { UniversityService } from '../../../../core/services/adminTools/university.service';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { ToastsService } from '../../../../core/services/toasts/toasts-service';
import { TestBed } from '@angular/core/testing';
import { EnvironmentService } from '../../../../core/services/environment/environment.service';
import { EnvironmentServiceImpl } from '../../../../core/services/environment/environment-impl.service';
import { ProposalTableService } from './proposal-table.service';
import { ProposalServiceMock } from '../../../../unittest/mocks/proposal.service.mock';
import { TranslateServiceMock } from '../../../../unittest/mocks/translate.service.mock';
import { HttpClientModule } from '@angular/common/http';
import { StoreMock } from '../../../../unittest/mocks/store.mock';

describe('proposal-detail.service', () => {
  let store: StoreMock;
  let proposalService: ProposalService;
  let universityService: UniversityService;
  let environmenService: EnvironmentService;
  let userService: UserService;
  let httpTestingController: HttpTestingController;
  let proposalTableService: ProposalTableService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        MatSnackBarModule,
        HttpClientTestingModule
      ],
      providers: [
        FormBuilder,
        ProposalTableService,
        {provide: Store, useClass: StoreMock},
        {provide: HttpService, useClass: HttpServiceImpl},
        {provide: ToastsService, useClass: ToastsService},
        {provide: MatSnackBar, useClass: MatSnackBar},
        {provide: Overlay, useClass: Overlay},
        {provide: ScrollStrategyOptions, useClass: ScrollStrategyOptions},
        {provide: ScrollDispatcher, useClass: ScrollDispatcher},
        {provide: ProposalService, useClass: ProposalServiceMock},
        {provide: UniversityService, useClass: UniversityService},
        {provide: EnvironmentService, useClass: EnvironmentServiceImpl},
        {provide: UserService, useClass: UserService},
        {provide: TranslateService, useClass: TranslateServiceMock}
      ]
    });

    proposalTableService = TestBed.get(ProposalTableService);
    proposalService = TestBed.get(ProposalService);
    universityService = TestBed.get(UniversityService);
    environmenService = TestBed.get(EnvironmentService);
    userService = TestBed.get(UserService);

    httpTestingController = TestBed.get(HttpTestingController);
    store = TestBed.get(Store);
  });


  it('should sould call getMyProposals()', () => {
    proposalTableService.initTable('MYPROPOSALS');
  });

  it('should sould call getMyTodos()', () => {
    proposalTableService.initTable('MYTODOS');
  });

  it('should sould call default getAllProposals()', () => {
    proposalTableService.initTable('');
  });


});
