import { Store } from '@ngrx/store';
import { EnvironmentService } from '../../../../core/services/environment/environment.service';
import { HttpServiceImpl } from '../../../../core/services/http/http.service.impl';
import { UserService } from '../../../../core/services/adminTools/user.service';
import { ProposalService } from '../../../../core/services/adminTools/proposal.service';
import { UniversityService } from '../../../../core/services/adminTools/university.service';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { EnvironmentServiceImpl } from '../../../../core/services/environment/environment-impl.service';
import { HttpService } from '../../../../core/services/http/http.service';
import { TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { ProposalDetailService } from './proposal-detail.service';
import { Proposal } from '../../../../core/domain/proposal';
import { ToastsService } from '../../../../core/services/toasts/toasts-service';
import {
  MatSnackBar,
  MatSnackBarModule
} from '@angular/material';
import {
  Overlay,
  ScrollDispatcher,
  ScrollStrategyOptions
} from '@angular/cdk/overlay';
import { TranslateService } from '@ngx-translate/core';
import { ProposalMockData } from '../../../../unittest/mock-data/proposal-mock-data';

describe('proposal-detail.service', () => {
  let store: MockStore;
  let proposalService: ProposalService;
  let universityService: UniversityService;
  let environmenService: EnvironmentService;
  let userService: UserService;
  let httpTestingController: HttpTestingController;
  let proposalDetailService: ProposalDetailService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        MatSnackBarModule,
        HttpClientTestingModule
      ],
      providers: [
        FormBuilder,
        ProposalDetailService,
        {provide: Store, useClass: MockStore},
        {provide: HttpService, useClass: HttpServiceImpl},
        {provide: ToastsService, useClass: ToastsService},
        {provide: MatSnackBar, useClass: MatSnackBar},
        {provide: Overlay, useClass: Overlay},
        {provide: ScrollStrategyOptions, useClass: ScrollStrategyOptions},
        {provide: ScrollDispatcher, useClass: ScrollDispatcher},
        {provide: ProposalService, useClass: ProposalServiceMock},
        {provide: UniversityService, useClass: UniversityService},
        {provide: EnvironmentService, useClass: EnvironmentServiceImpl},
        {provide: UserService, useClass: UserService},
        {provide: TranslateService, useClass: TranslateServiceMock}
      ]
    });

    proposalDetailService = TestBed.get(ProposalDetailService);
    proposalService = TestBed.get(ProposalService);
    universityService = TestBed.get(UniversityService);
    environmenService = TestBed.get(EnvironmentService);
    userService = TestBed.get(UserService);

    httpTestingController = TestBed.get(HttpTestingController);
    store = TestBed.get(Store);
  });


  it('should create a new empty proposal-details form', () => {
    const form = proposalDetailService.createNewEmptyProposalDetailsForm();

    expect(form.get('firstname').dirty).toBe(false, 'field firstName should not be dirty after initialization');
    expect(form.get('firstname').disabled).toBeTruthy();
    expect(form.get('lastname').dirty).toBe(false, 'field lastName should not be dirty after initialization');
    expect(form.get('lastname').disabled).toBeTruthy();
    expect(form.get('email').dirty).toBe(false, 'field email should not be dirty after initialization');
    expect(form.get('email').disabled).toBeTruthy();
    expect(form.get('telephone').dirty).toBe(false, 'field phone should not be dirty after initialization');
    expect(form.get('telephone').disabled).toBeTruthy();

    expect(form.get('university').dirty).toBe(false, 'field university should not be dirty after initialization');
    expect(form.get('university').disabled).toBeTruthy();
    expect(form.get('institute').dirty).toBe(false, 'field institute should not be dirty after initialization');
    expect(form.get('institute').disabled).toBeTruthy();

    expect(form.get('purpose').dirty).toBe(false, 'field purpose should not be dirty after initialization');
    expect(form.get('purpose').disabled).toBeTruthy();
    expect(form.get('proposalName').dirty).toBe(false, 'field proposalName should not be dirty after initialization');
    expect(form.get('proposalName').disabled).toBeTruthy();
    expect(form.get('admin_email').dirty).toBe(false, 'field admin_email should not be dirty after initialization');
    expect(form.get('admin_email').disabled).toBeTruthy();
    expect(form.get('proposalType').dirty).toBe(false, 'field proposalType should not be dirty after initialization');
    expect(form.get('proposalType').disabled).toBeTruthy();
    expect(form.get('technology').dirty).toBe(false, 'field technology should not be dirty after initialization');
    expect(form.get('technology').disabled).toBeTruthy();
  });

  it('should fetch a proposal with initProposal', () => {
    let result;
    proposalDetailService.initProposal(3).subscribe((value: Proposal) => {
      result = value;
      expect(result).toEqual(ProposalMockData.newProposal());
    });
  });
});

class MockStore {
}

class ProposalServiceMock {
  getProposalFromStoreById(id: number): Proposal {
    return ProposalMockData.newProposal();
  }
}

class TranslateServiceMock {
}
