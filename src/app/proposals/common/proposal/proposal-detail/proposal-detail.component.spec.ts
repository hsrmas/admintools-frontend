import {
  async,
  TestBed
} from '@angular/core/testing';

import { ProposalDetailComponent } from './proposal-detail.component';
import { ToastsService } from '../../../../core/services/toasts/toasts-service';
import { RouterModule } from '@angular/router';
import {
  FormsModule,
  ReactiveFormsModule
} from '@angular/forms';
import { LanguageService } from '../../../../core/services/language/language.service';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpService } from '../../../../core/services/http/http.service';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {
  HttpClient,
  HttpHandler
} from '@angular/common/http';
import {
  TranslateLoader,
  TranslateModule
} from '@ngx-translate/core';
import { EnvironmentService } from '../../../../core/services/environment/environment.service';
import { MaterialModule } from '../../../../catalogue/material.module';
import { createTranslationLoader } from '../../../../core/services/language/translation.loader';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ProposalDetailService } from './proposal-detail.service';
import { ProposalDetailServiceMock } from '../../../../unittest/mocks/proposal-detail.service.mock';
import { UserService } from '../../../../core/services/adminTools/user.service';
import { UserBackendServiceMock } from '../../../../unittest/mocks/user-backend.service.mock';
import { EventService } from '../../../../core/services/events/event-service';

describe('ProposalDetailComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProposalDetailComponent],
      imports: [
        RouterModule,
        MaterialModule,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: (createTranslationLoader),
            deps: [HttpClient]
          }
        })
      ],
      providers: [
        HttpService,
        EnvironmentService,
        HttpClient,
        HttpHandler,
        LanguageService,
        ToastsService,
        EventService,
        {provide: UserService, useClass: UserBackendServiceMock},
        {provide: ProposalDetailService, useClass: ProposalDetailServiceMock}
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
      .compileComponents();
  }));

  it('should create ProposalDetailComponent', () => {
    const fixture = TestBed.createComponent(ProposalDetailComponent);
    const component = fixture.componentInstance;
    expect(component).toBeDefined();
  });
});
