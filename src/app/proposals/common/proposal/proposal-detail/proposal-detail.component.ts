import {
  Component,
  OnInit,
  ViewChild
} from '@angular/core';
import {
  FormBuilder,
  FormGroup
} from '@angular/forms';
import { StepStatus } from '../../../../core/domain/step-status';
import {
  ActivatedRoute,
  Router
} from '@angular/router';
import { ToastsService } from '../../../../core/services/toasts/toasts-service';
import {
  MatDialog,
  MatVerticalStepper
} from '@angular/material';
import { ProposalDetailService } from './proposal-detail.service';
import { Proposal } from '../../../../core/domain/proposal';
import { UserService } from '../../../../core/services/adminTools/user.service';
import { Observable } from 'rxjs/Observable';
import { ToastData } from '../../../../core/services/toasts/toast-data';
import { ToastType } from '../../../../core/services/toasts/toast-type';
import { TranslateService } from '@ngx-translate/core';
import { EventService } from '../../../../core/services/events/event-service';
import { RejectDialogComponent } from '../reject-dialog/reject-dialog.component';

@Component({
  selector: 'app-proposal-detail',
  templateUrl: './proposal-detail.component.html',
  styleUrls: ['./proposal-detail.component.scss']
})
export class ProposalDetailComponent implements OnInit {


  @ViewChild('stepper') public stepper: MatVerticalStepper;

  proposalId: number;

  proposal: Proposal;
  proposalDetailsForm: FormGroup;
  stepArray: StepStatus[] = [];
  selectedIndex: number;
  dataLoaded: boolean;
  isApprover: boolean;
  isRejected: boolean;
  rejectReason: string;
  applicationStatus: string;

  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private proposalDetailService: ProposalDetailService,
              private userService: UserService,
              private router: Router,
              private transtlateService: TranslateService,
              private eventService: EventService,
              private toastService: ToastsService,
              private rejectDialog: MatDialog) {
    // listen to route changes
    route.params.subscribe(params => {
      this.proposalId = params['id'];
    });
  }


  ngOnInit() {
    this.dataLoaded = false;
    this.proposalDetailsForm = this.proposalDetailService.createNewEmptyProposalDetailsForm();
    this.initProposal();
  }

  initProposal(): void {
    this.proposalDetailService.initProposal(Number(this.proposalId))
      .map((proposal: Proposal) => this.proposalDetailService.mapProposalToForm(proposal,
        proposal.owner, this.proposalDetailsForm))
      .subscribe((proposal: Proposal) => {
        this.proposal = proposal;
        this.stepArray = this.proposal.all_states;
        this.dataLoaded = true;
        this.isRejected = proposal.is_rejected;
        this.rejectReason = proposal.rejection_reason;
        this.applicationStatus = proposal.application_status;
      });
    this.isApprover = this.userService.isApprover() || this.userService.isAdmin();
  }

  approveProposal() {
    this.proposalDetailService.approveProposal(this.proposalId)
      .catch(() => {
        // parse response. if "not allowed" make toast
        this.toastService.genericErrorToast();
        this.router.navigate(['errorpage/']);
        return Observable.empty();
      })
      .subscribe((result: Proposal) => {
        this.toastService.makeToast(new ToastData(this.transtlateService.instant('TOAST_MESSAGES.SUCCESS.APPROVE_PROPOSAL.TITLE'),
          this.transtlateService.instant('TOAST_MESSAGES.SUCCESS.APPROVE_PROPOSAL.MESSAGE', {antragname: result.name}),
          ToastType.success));
        this.eventService.proposalEvent(true);
        this.router.navigate(['proposal-approve/']);
      });
  }

  /**
   * Open Reject Modal and handle user actions
   */
  rejectProposal() {
    let reason: string;
    const dialogRef = this.rejectDialog.open(RejectDialogComponent, {
      data: {reason: '', isCancel: false}
    });

    dialogRef.afterClosed().subscribe(result => {
      reason = result;
      // cancel action workaround
      if (result !== true) {
        this.proposalDetailService.rejectProposal(this.proposalId, reason)
          .catch(() => {
            // parse response. if "not allowed" make toast
            this.toastService.genericErrorToast();
            this.router.navigate(['errorpage/']);
            return Observable.empty();
          })
          .subscribe((proposal: Proposal) => {
            this.toastService.makeToast(new ToastData(this.transtlateService.instant('TOAST_MESSAGES.SUCCESS.REJECT_PROPOSAL.TITLE'),
              this.transtlateService.instant('TOAST_MESSAGES.SUCCESS.REJECT_PROPOSAL.MESSAGE', {antragname: proposal.name}),
              ToastType.success));
            this.eventService.proposalEvent(true);
            this.router.navigate(['proposal-approve/']);
          });
      }
    });
  }
}
