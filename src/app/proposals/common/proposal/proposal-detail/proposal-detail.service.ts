import { Injectable } from '@angular/core';
import {
  FormBuilder,
  FormGroup
} from '@angular/forms';
import { ProposalService } from '../../../../core/services/adminTools/proposal.service';
import { Proposal } from '../../../../core/domain/proposal';
import { StepStatus } from '../../../../core/domain/step-status';
import { AppState } from '../../../../core/statemanagement/app.state';
import { Store } from '@ngrx/store';
import { University } from '../../../../core/domain/university';
import { Observable } from 'rxjs/Observable';
import { UniversityService } from '../../../../core/services/adminTools/university.service';
import { UniversitySelector } from '../../../../core/statemanagement/selectors';
import { ToastsService } from '../../../../core/services/toasts/toasts-service';
import { User } from '../../../../core/domain/user';
import { RejectReason } from '../../../../core/domain/reject-reason';

@Injectable()
export class ProposalDetailService {

  proposal: Proposal;
  stepArraySrv: StepStatus[] = [];
  selectedIndex: number;

  constructor(private fb: FormBuilder,
              private proposalService: ProposalService,
              private universityService: UniversityService,
              private toastService: ToastsService,
              private store: Store<AppState>) {

  }

  /**
   * Create new empty proposal details form
   * Fields are disabled by default.
   * @returns {FormGroup}
   */
  createNewEmptyProposalDetailsForm(): FormGroup {
    return this.fb.group({
      // personal
      'firstname': [{value: '', disabled: true}],
      'lastname': [{value: '', disabled: true}],
      'email': [{value: '', disabled: true}],
      'telephone': [{value: '', disabled: true}],

      // proposal details
      'username': [{value: '', disabled: true}],
      'admin_email': [{value: '', disabled: true}],
      'university': [{value: '', disabled: true}],
      'institute': [{value: '', disabled: true}],
      'purpose': [{value: '', disabled: true}],
      'proposalName': [{value: '', disabled: true}],
      'proposalType': [{value: '', disabled: true}],
      'technology': [{value: '', disabled: true}],
    });
  }

  initProposal(proposalId: number): Observable<Proposal> {
    const proposalFromStoreById = this.proposalService.getProposalFromStoreById(proposalId);
    if (proposalFromStoreById) {
      return Observable.of(proposalFromStoreById);
    } else if (proposalFromStoreById === null || proposalFromStoreById === undefined) {
      this.toastService.genericErrorToast();
    }
  }

  /**
   * Approve a proposal
   * @param {number} proposalId
   * @returns {Observable<Proposal>}
   */
  approveProposal(proposalId: number): Observable<Proposal> {
    return this.proposalService.approveProposal(proposalId);
  }

  /**
   * Reject a proposal
   * @param {number} proposalId
   * @param {string} rejectReason
   * @returns {Observable<Proposal>}
   */
  rejectProposal(proposalId: number, rejectReason: string): Observable<Proposal> {
    let rejectReasonObj: RejectReason;
    rejectReasonObj = Object.assign(Object.create(RejectReason.prototype), {
      reason: rejectReason,
    });
    return this.proposalService.rejectProposal(rejectReasonObj, proposalId);
  }

  /**
   * Map the proposal values to form
   * @param {Proposal} proposal
   * @param {User} user
   * @param {FormGroup} proposalDetailForm
   * @returns {Proposal}
   */
  mapProposalToForm(proposal: Proposal, user: User, proposalDetailForm: FormGroup): Proposal {
    let universityName;
    this.store.select(UniversitySelector.findUniversityByURL(proposal.institute.university))
      .subscribe((university: University) => {
        universityName = university.name;
      });

    // personal user information (shibboleth)
    proposalDetailForm.get('firstname').patchValue(user.first_name);
    proposalDetailForm.get('lastname').patchValue(user.last_name);
    proposalDetailForm.get('email').patchValue(proposal.owner.email);
    proposalDetailForm.get('telephone').patchValue('+41 56 200 19 20');

    // proposal details
    proposalDetailForm.get('username').patchValue(proposal.owner.username);
    proposalDetailForm.get('admin_email').patchValue(proposal.admin_email);
    proposalDetailForm.get('university').patchValue(universityName);
    proposalDetailForm.get('institute').patchValue(proposal.institute.name);
    proposalDetailForm.get('purpose').patchValue(proposal.purpose);
    proposalDetailForm.get('proposalName').patchValue(proposal.name);
    proposalDetailForm.get('proposalType').patchValue(proposal.proposal_type.name);
    proposalDetailForm.get('technology').patchValue(proposal.technology.name);
    return proposal;
  }

}
