import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
} from '@angular/core';
import {
  AbstractControl,
  FormGroup
} from '@angular/forms';
import { SelectItem } from '../../../../core/domain/select.item';
import { ProposalCreateService } from '../../../proposal-create/proposal-create.service';
import { CONSTANTS } from '../../../../core/constants';

@Component({
  selector: 'app-proposal',
  templateUrl: './proposal.component.html',
  styleUrls: ['./proposal.component.scss']
})
export class ProposalComponent implements OnInit {

  @Input() proposalForm: FormGroup;
  @Input() proposalPanelOpen: boolean;
  @Input() proposalNewProposalDetailsForm: FormGroup;
  @Input() proposalDeleteProposalDetailsForm: FormGroup;
  @Output() previousStep = new EventEmitter<boolean>();
  @Output() submitCreateCalledEvent = new EventEmitter<boolean>();
  @Output() submitDeleteCalledEvent = new EventEmitter<boolean>();

  proposalTypes: SelectItem[];
  selectedProposalTypeControl: AbstractControl;

  newAppCompActive: boolean;
  deleteAppCompActive: boolean;

  constructor(
    private proposalCreateService: ProposalCreateService) {
  }

  ngOnInit() {

    this.newAppCompActive = false;
    this.deleteAppCompActive = false;

    this.proposalTypes = this.proposalCreateService.prepareProposalTypeSelectItems();

    this.selectedProposalTypeControl = this.proposalForm.controls['proposalType'];
    this.selectedProposalTypeControl.valueChanges.subscribe((key: string) => {
      if (key === CONSTANTS.PROPOSAL_TYPE_NEW) {
        this.newAppCompActive = true;
        this.deleteAppCompActive = false;
      } else {
        this.deleteAppCompActive = true;
        this.newAppCompActive = false;
      }
    });
  }

  /**
   * Navigate back to personal form step
   */
  prevStep() {
    this.previousStep.emit(true);
  }


  /**
   * Depending on the create action selected emit event
   */
  onSubmit() {
    if (this.newAppCompActive) {
      this.submitCreateCalledEvent.emit();
    } else if (this.deleteAppCompActive) {
      this.submitDeleteCalledEvent.emit();
    }
  }
}
