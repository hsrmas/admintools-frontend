import {
  async,
  ComponentFixture,
  TestBed
} from '@angular/core/testing';

import { ProposalComponent } from './proposal.component';
import { MaterialModule } from '../../../../catalogue/material.module';
import {
  FormsModule,
  ReactiveFormsModule
} from '@angular/forms';
import {
  TranslateLoader,
  TranslateModule,
  TranslateService
} from '@ngx-translate/core';
import {
  HttpClient,
  HttpClientModule
} from '@angular/common/http';
import { createTranslationLoader } from '../../../../core/services/language/translation.loader';
import {
  Component,
  CUSTOM_ELEMENTS_SCHEMA,
  OnInit
} from '@angular/core';
import { ProposalCreateService } from '../../../proposal-create/proposal-create.service';
import { EnvironmentService } from '../../../../core/services/environment/environment.service';
import { LanguageService } from '../../../../core/services/language/language.service';
import { HttpService } from '../../../../core/services/http/http.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ProposalService } from '../../../../core/services/adminTools/proposal.service';
import { Store } from '@ngrx/store';
import { StoreMock } from '../../../../unittest/mocks/store.mock';
import { TechnologyServiceMock } from '../../../../unittest/mocks/technology.service.mock';
import { TechnologyService } from '../../../../core/services/adminTools/technology.service';
import { ProposalTypeServiceMock } from '../../../../unittest/mocks/proposal-type.service.mock';
import { ProposalTypeService } from '../../../../core/services/adminTools/proposal-type.service';
import { InstituteService } from '../../../../core/services/adminTools/institute.service';
import { InstituteServiceMock } from '../../../../unittest/mocks/institute.service.mock';
import { UniversityService } from '../../../../core/services/adminTools/university.service';
import { UniversityServiceMock } from '../../../../unittest/mocks/university.service.mock';
import { ApplicationNameValidatorMock } from '../../../../unittest/mocks/application-name.validator.mock';
import { ProposalNameValidatorMock } from '../../../../unittest/mocks/proposal-name.validator.mock';
import { EnvironmentServiceImpl } from '../../../../core/services/environment/environment-impl.service';
import { ProposalNameValidator } from '../../../../core/validation/proposal-name.validator';
import { ApplicationNameValidator } from '../../../../core/validation/application-name.validator';
import { UserService } from '../../../../core/services/adminTools/user.service';
import { ApplicationService } from '../../../../core/services/adminTools/application.service';
import { TranslateServiceMock } from '../../../../unittest/mocks/translate.service.mock';
import { ToastsService } from '../../../../core/services/toasts/toasts-service';
import { HttpServiceImpl } from '../../../../core/services/http/http.service.impl';
import { UserBackendServiceMock } from '../../../../unittest/mocks/user-backend.service.mock';
import { ToastServiceMock } from '../../../../unittest/mocks/toast.service.mock';

describe('ProposalComponent', () => {


  let fixture: ComponentFixture<ProposalComponent>;
  let component: ProposalComponent;
  let proposalCreateService: ProposalCreateService;
  // let fixtureTest: ComponentFixture<ProposalTestComponent>;
  // let componentTest: ProposalTestComponent;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ProposalComponent,
        ProposalTestComponent,
      ],
      imports: [
        RouterModule,
        MaterialModule,
        FormsModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        RouterTestingModule,
        HttpClientModule,
        HttpClientTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: (createTranslationLoader),
            deps: [HttpClient]
          }
        })
      ],
      providers: [
        EnvironmentService,
        LanguageService,
        ToastsService,
        {provide: Store, useClass: StoreMock},
        {provide: HttpService, useClass: HttpServiceImpl},
        {provide: UserService, useClass: UserBackendServiceMock},
        {provide: ToastsService, useClass: ToastServiceMock},
        {provide: UserService, useClass: UserBackendServiceMock},
        {provide: TranslateService, useClass: TranslateServiceMock},
        {provide: ProposalService, useClass: ProposalService},
        {provide: ProposalCreateService, useClass: ProposalCreateService},
        {provide: ProposalTypeService, useClass: ProposalTypeServiceMock},
        {provide: TechnologyService, useClass: TechnologyServiceMock},
        {provide: InstituteService, useClass: InstituteServiceMock},
        {provide: UniversityService, useClass: UniversityServiceMock},
        {provide: ApplicationNameValidator, useClass: ApplicationNameValidatorMock},
        {provide: ProposalNameValidator, useClass: ProposalNameValidatorMock},
        {provide: ApplicationService, useClass: ApplicationService},
        {provide: EnvironmentService, useClass: EnvironmentServiceImpl},
        {provide: UserService, useClass: UserService},
        {provide: TranslateService, useClass: TranslateServiceMock},
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProposalComponent);
    component = fixture.componentInstance;
    proposalCreateService = TestBed.get(ProposalCreateService);
  });

  xit('should create ProposalComponent', () => {
    fixture = TestBed.createComponent(ProposalComponent);
    component = fixture.componentInstance;
    component.ngOnInit();
    expect(component).toBeDefined();
  });

  xit('should call the ngOnInit() and the proposalCreateService prepareProposalTypeSelectItems() method ', () => {
    spyOn(component, 'ngOnInit');
    component.ngOnInit();
    expect(component.ngOnInit).toHaveBeenCalled();

    spyOn(proposalCreateService, 'prepareProposalTypeSelectItems');
    proposalCreateService.prepareProposalTypeSelectItems();
    expect(proposalCreateService.prepareProposalTypeSelectItems).toHaveBeenCalled();
  });

});


@Component({
  template: `
    <app-proposal>
      <mat-expansion-panel [expanded]="true"></mat-expansion-panel>
    </app-proposal>
  `
})
class ProposalTestComponent implements OnInit {
  ngOnInit() {}
}
