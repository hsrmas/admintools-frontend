import {
  async,
  ComponentFixture,
  TestBed
} from '@angular/core/testing';

import { ErrorPageComponent } from './error-page.component';
import { MaterialModule } from '../../../catalogue/material.module';

describe('ErrorPageComponent', () => {
  let component: ErrorPageComponent;
  let fixture: ComponentFixture<ErrorPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MaterialModule,
      ],
      declarations: [
        ErrorPageComponent
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create ErrorPageComponent', () => {
    expect(component).toBeTruthy();
  });
});
