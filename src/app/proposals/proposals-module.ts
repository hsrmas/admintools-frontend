import { NgModule } from '@angular/core';
import { ProposalHomeComponent } from './proposal-home/proposal-home.component';
import { ProposalApproveComponent } from './proposal-approve/proposal-approve.component';
import { ProposalMyProposalsComponent } from './proposal-my-proposals/proposal-my-proposals.component';
import { ProposalCreateComponent } from './proposal-create/proposal-create.component';
import { HttpService } from '../core/services/http/http.service';
import { HttpServiceImpl } from '../core/services/http/http.service.impl';
import { LanguageService } from '../core/services/language/language.service';
import { EnvironmentServiceImpl } from '../core/services/environment/environment-impl.service';
import { ROUTES } from '../app.routes';
import { EnvironmentService } from '../core/services/environment/environment.service';
import {
  FormsModule,
  ReactiveFormsModule
} from '@angular/forms';
import {
  TranslateLoader,
  TranslateModule
} from '@ngx-translate/core';
import { createTranslationLoader } from '../core/services/language/translation.loader';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '../catalogue/material.module';
import {
  HttpClient,
  HttpClientModule
} from '@angular/common/http';
import { ProposalCreateService } from './proposal-create/proposal-create.service';
import { ToastsModule } from '../core/services/toasts/toasts-module';
import { ToastsComponent } from '../core/services/toasts/toasts.component';
import { ToastsService } from '../core/services/toasts/toasts-service';
import { TechnologyService } from '../core/services/adminTools/technology.service';
import { ProposalService } from '../core/services/adminTools/proposal.service';
import { ProposalTypeService } from '../core/services/adminTools/proposal-type.service';
import { PersonalDetailsComponent } from './common/proposal/personal-details/personal-details.component';
import { ProposalComponent } from './common/proposal/proposal/proposal.component';
import { InstituteService } from '../core/services/adminTools/institute.service';
import { UniversityService } from '../core/services/adminTools/university.service';
import { NewApplicationComponent } from './common/proposal/new-application/new-application.component';
import { DeleteApplicationComponent } from './common/proposal/delete-application/delete-application.component';
import { ProposalDetailService } from './common/proposal/proposal-detail/proposal-detail.service';
import { ProposalDetailComponent } from './common/proposal/proposal-detail/proposal-detail.component';
import { ProposalTableComponent } from './common/proposal/proposal-table/proposal-table.component';
import { ProposalTableService } from './common/proposal/proposal-table/proposal-table.service';
import { ApplicationService } from '../core/services/adminTools/application.service';
import { ApplicationNameValidator } from '../core/validation/application-name.validator';
import { ProposalNameValidator } from '../core/validation/proposal-name.validator';
import { EventService } from '../core/services/events/event-service';
import { RejectDialogComponent } from './common/proposal/reject-dialog/reject-dialog.component';
import { ProposalAdminComponent } from './proposal-admin/proposal-admin.component';

@NgModule({
  declarations: [
    ProposalCreateComponent,
    ProposalHomeComponent,
    ProposalApproveComponent,
    ProposalMyProposalsComponent,
    ProposalComponent,
    PersonalDetailsComponent,
    ProposalDetailComponent,
    ProposalAdminComponent,
    NewApplicationComponent,
    DeleteApplicationComponent,
    ProposalTableComponent,
    RejectDialogComponent
  ],
  imports: [
    BrowserModule,
    FlexLayoutModule,
    RouterModule.forRoot(ROUTES, {useHash: true}),
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    HttpClientModule,
    ToastsModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslationLoader),
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    LanguageService,
    ProposalCreateService,
    ToastsService,
    TechnologyService,
    ProposalService,
    ProposalTypeService,
    InstituteService,
    UniversityService,
    ProposalDetailService,
    ProposalTableService,
    ApplicationService,
    ApplicationNameValidator,
    ProposalNameValidator,
    ProposalService,
    EventService,

    {provide: HttpService, useClass: HttpServiceImpl},
    {provide: EnvironmentService, useClass: EnvironmentServiceImpl}
  ],
  entryComponents: [
    ToastsComponent,
    RejectDialogComponent
  ]
})
export class ProposalsModule {
}
