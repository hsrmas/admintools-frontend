import {
  RouterModule,
  Routes
} from '@angular/router';
import { ProposalCreateComponent } from './proposals/proposal-create/proposal-create.component';
import { ProposalHomeComponent } from './proposals/proposal-home/proposal-home.component';
import { ProposalMyProposalsComponent } from './proposals/proposal-my-proposals/proposal-my-proposals.component';
import { ProposalApproveComponent } from './proposals/proposal-approve/proposal-approve.component';
import { ProposalDetailComponent } from './proposals/common/proposal/proposal-detail/proposal-detail.component';
import { ErrorPageComponent } from './proposals/common/error-page/error-page.component';
import { ProposalAdminComponent } from './proposals/proposal-admin/proposal-admin.component';

export const ROUTES: Routes = [
  {path: 'proposal-home', component: ProposalHomeComponent},
  {path: 'proposal-create', component: ProposalCreateComponent},
  {path: 'proposal-my-proposals', component: ProposalMyProposalsComponent},
  {path: 'proposal-approve', component: ProposalApproveComponent},
  {path: 'proposal-admin', component: ProposalAdminComponent},
  {path: 'proposal-detail/:id', component: ProposalDetailComponent},
  {path: 'errorpage', component: ErrorPageComponent},
  {
    path: '',
    redirectTo: '/proposal-home',
    pathMatch: 'full',
  },
];

export const routing = RouterModule.forRoot(ROUTES, {useHash: true});
