import {
  async,
  ComponentFixture,
  TestBed
} from '@angular/core/testing';
import { AppComponent } from './app.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MaterialModule } from './catalogue/material.module';
import {
  FormsModule,
  ReactiveFormsModule
} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { createTranslationLoader } from './core/services/language/translation.loader';
import {
  TranslateLoader,
  TranslateModule
} from '@ngx-translate/core';
import {
  HttpClient,
  HttpHandler
} from '@angular/common/http';
import { LanguageService } from './core/services/language/language.service';
import { NavigationComponent } from './proposals/common/navigation/navigation.component';
import { FooterComponent } from './proposals/common/footer/footer.component';
import { ObservableMedia } from '@angular/flex-layout';
import { StoreMock } from './unittest/mocks/store.mock';
import { Store } from '@ngrx/store';
import { EnvironmentService } from './core/services/environment/environment.service';
import { ProposalService } from './core/services/adminTools/proposal.service';
import { HttpService } from './core/services/http/http.service';
import { EventService } from './core/services/events/event-service';
import { ProposalServiceMock } from './unittest/mocks/proposal.service.mock';
import { EventServiceMock } from './unittest/mocks/event.service.mock';
import { EnvironmentServiceMock } from './unittest/mocks/environment.service.mock';
import { UserService } from './core/services/adminTools/user.service';

describe('AppComponent', () => {

  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent, NavigationComponent, FooterComponent
      ],
      imports: [
        RouterModule,
        MaterialModule,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: (createTranslationLoader),
            deps: [HttpClient]
          }
        })
      ],
      providers: [
        HttpClient, HttpHandler, LanguageService, ObservableMedia, HttpService, UserService,
        {provide: EnvironmentService, useClass: EnvironmentServiceMock},
        {provide: EventService, useClass: EventServiceMock},
        {provide: ProposalService, useClass: ProposalServiceMock},
        {provide: Store, useClass: StoreMock}
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
  });

  it('should create AppComponent', () => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    // component.ngOnInit();
    expect(component).toBeDefined();
    // component.proposalEventSubscription.unsubscribe();
  });

  afterEach(() => {

  });
});
