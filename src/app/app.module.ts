import './core/rx-js-exstensions';
import { BrowserModule } from '@angular/platform-browser';
import {
  APP_INITIALIZER,
  Injector,
  NgModule
} from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { RouterModule } from '@angular/router';
import { ROUTES } from './app.routes';
import {
  FormsModule,
  ReactiveFormsModule
} from '@angular/forms';
import { MaterialModule } from './catalogue/material.module';
import {
  HTTP_INTERCEPTORS,
  HttpClient,
  HttpClientModule,
  HttpClientXsrfModule
} from '@angular/common/http';
import {
  TranslateLoader,
  TranslateModule
} from '@ngx-translate/core';
import { createTranslationLoader } from './core/services/language/translation.loader';
import { adminToolsFrontendInitializer } from './core/application.initializer';

import { NavigationComponent } from './proposals/common/navigation/navigation.component';
import { HeaderComponent } from './proposals/common/header/header.component';
import { FooterComponent } from './proposals/common/footer/footer.component';
import { ProposalsModule } from './proposals/proposals-module';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { appReducers } from './core/statemanagement/reducers';
import { EnvironmentService } from './core/services/environment/environment.service';
import { DefaultInterceptor } from './core/interceptors/default-interceptor';
import { ErrorPageComponent } from './proposals/common/error-page/error-page.component';
import { ErrorInterceptor } from './core/interceptors/error-interceptor';
import { UniversityService } from './core/services/adminTools/university.service';
import { ProposalService } from './core/services/adminTools/proposal.service';
import { ProposalTypeService } from './core/services/adminTools/proposal-type.service';
import { InstituteService } from './core/services/adminTools/institute.service';
import { ApplicationService } from './core/services/adminTools/application.service';
import { LanguageService } from './core/services/language/language.service';
import { EventService } from './core/services/events/event-service';
import { UserService } from './core/services/adminTools/user.service';


@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    HeaderComponent,
    FooterComponent,
    ErrorPageComponent,
  ],
  imports: [
    BrowserModule,
    FlexLayoutModule,
    RouterModule.forRoot(ROUTES, {useHash: true}),
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    HttpClientModule,
    HttpClientXsrfModule.withOptions({
      cookieName: 'csrftoken',
      headerName: 'X-CSRFToken'
    }),
    ProposalsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslationLoader),
        deps: [HttpClient]
      }
    }),
    StoreModule.forRoot(appReducers),
    StoreDevtoolsModule.instrument({
      maxAge: 5
    })
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: adminToolsFrontendInitializer,
      deps: [UniversityService, InstituteService, ProposalTypeService, ProposalService, ApplicationService, UserService, LanguageService],
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: DefaultInterceptor,
      deps: [EnvironmentService],
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      deps: [Injector],
      multi: true
    },
    {
      provide: EventService,
      useClass: EventService,
    },
    {
      provide: UserService,
      useClass: UserService,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
