[![build status](https://gitlab.fhnw.ch/hsrmas/admintools-frontend/badges/master/build.svg)](https://gitlab.fhnw.ch/hsrmas/admintools-frontend/commits/master)
[![coverage report](https://gitlab.fhnw.ch/hsrmas/admintools-frontend/badges/master/coverage.svg)](https://hsrmas.pages.fhnw.ch/admintools-frontend/)


# AdminTools-Frontend 2.0

- Project description (tdb)

## Pre-Requisites

- Node  v8.9.1
- NPM   5.5.1
- AdminTools backend: [admintools](https://gitlab.fhnw.ch/hsrmas/admintools)

## Installation

Run `npm install`

## Development server (local)

Run `npm run start:dev` for a local dev server. Navigate to `http://localhost:4200/`

## Test server (local)

Run `npm run start:test` for a local dev with a test backend server. Navigate to `http://localhost:4200/` 

## Testing

### Coverage 
Run `npm run test:coverage` to run all karma/jasmine tests and create a coverage report. The report is available in the `public` folder of the project root.

### Unit tests

Run `npm run test` to run all karma/jasmine tests

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

### Before Commit

Before commiting changes to the repository run the following command to ensure proper linting and test verification

Run `npm run before:commit` to run, linting and all karma/jasmine tests

## Build

Run `npm run build:prod` to build the project.
